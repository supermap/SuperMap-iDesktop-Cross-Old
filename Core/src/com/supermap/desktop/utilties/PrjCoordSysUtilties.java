package com.supermap.desktop.utilties;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

import com.supermap.data.GeoCoordSysType;
import com.supermap.data.PrjCoordSys;
import com.supermap.data.PrjCoordSysType;
import com.supermap.desktop.Application;
import com.supermap.desktop.properties.CoreProperties;

public class PrjCoordSysUtilties {

	// /// <summary>
	// /// 导出投影坐标系类型
	// /// </summary>
	// public static DialogResult ExportPrjInfoToFile(PrjCoordSys
	// prjCoordSys)
	// {
	// DialogResult dialogResult = DialogResult.None;
	// try
	// {
	// if (prjCoordSys != null)
	// {
	// SaveFileDialog saveDialog = new SaveFileDialog();
	// saveDialog.Filter = CoreResources.String_PrjInfo_Filter;
	//
	// // XP 系统，文件名不支持存在 “/”，所以重命名一下文件名
	// String fileName = prjCoordSys.Name;
	// String earthLongitudeLatitude =
	// "Longitude / Latitude Coordinate System---";
	// fileName = fileName.Replace(earthLongitudeLatitude, String.Empty);
	// saveDialog.FileName = fileName;
	// if (saveDialog.ShowDialog() == DialogResult.OK)
	// {
	// String strExt = Path.getExtension(saveDialog.FileName);
	// boolean bExported = false;
	// switch (saveDialog.FilterIndex)
	// {
	// case 1:
	// {
	// bExported = prjCoordSys.ToFile(saveDialog.FileName,
	// PrjFileVersion.UGC60);
	// }
	// break;
	// case 2:
	// {
	// bExported = prjCoordSys.ToFile(saveDialog.FileName,
	// PrjFileVersion.SFC60);
	// }
	// break;
	// }
	//
	// if (bExported)
	// {
	// dialogResult = DialogResult.Yes;
	// }
	// else
	// {
	// dialogResult = DialogResult.No;
	// }
	// }
	//
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return dialogResult;
	// }
	//
	// ///// <summary>
	// /////
	// ///// </summary>
	// //public static boolean IsPrjCoordSysEqual(PrjCoordSys prjA,
	// PrjCoordSys prjB)
	// //{
	// // boolean result = false;
	// // try
	// // {
	// // if (prjA != null & prjB != null)
	// // {
	// // if (prjA.getType() == prjB.getType())
	// // {
	// // if (prjA.GeoCoordSys.getType() == prjB.GeoCoordSys.getType()
	// // && prjA.CoordUnit == prjB.CoordUnit)
	// // {
	// // boolean userDef = false;
	// // if (prjA.GeoCoordSys.GeoDatum.getType() ==
	// prjB.GeoCoordSys.GeoDatum.getType()
	// // && prjA.GeoCoordSys.GeoDatum.GeoSpheroid.getType() ==
	// prjB.GeoCoordSys.GeoDatum.GeoSpheroid.getType()
	// // && prjA.GeoCoordSys.GeoDatum.GeoSpheroid.Axis ==
	// prjB.GeoCoordSys.GeoDatum.GeoSpheroid.Axis
	// // && prjA.GeoCoordSys.GeoDatum.GeoSpheroid.Flatten ==
	// prjB.GeoCoordSys.GeoDatum.GeoSpheroid.Flatten
	// // )
	// // {
	// // userDef = true;
	// // }
	// // if (userDef)
	// // {
	// // if (prjA.Projection != null && prjB.Projection != null)
	// // {
	// // if (prjA.Projection.getType() == prjB.Projection.getType())
	// // {
	// // if (prjA.PrjParameter.Azimuth == prjB.PrjParameter.Azimuth
	// // && prjA.PrjParameter.CentralMeridian ==
	// prjB.PrjParameter.CentralMeridian
	// // && prjA.PrjParameter.CentralParallel ==
	// prjB.PrjParameter.CentralParallel
	// // && prjA.PrjParameter.FalseEasting ==
	// prjB.PrjParameter.FalseEasting
	// // && prjA.PrjParameter.FalseNorthing ==
	// prjB.PrjParameter.FalseNorthing
	// // && prjA.PrjParameter.FirstPointLongitude ==
	// prjB.PrjParameter.FirstPointLongitude
	// // && prjA.PrjParameter.ScaleFactor == prjB.PrjParameter.ScaleFactor
	// // && prjA.PrjParameter.SecondPointLongitude ==
	// prjB.PrjParameter.SecondPointLongitude
	// // && prjA.PrjParameter.StandardParallel1 ==
	// prjB.PrjParameter.StandardParallel1
	// // && prjA.PrjParameter.StandardParallel2 ==
	// prjB.PrjParameter.StandardParallel2
	// // )
	// // {}
	// // else
	// // {
	// // userDef = false;
	// // }
	// // }
	// // else
	// // {
	// // userDef = false;
	// // }
	// // }
	// // else if (prjA.Projection == null && prjB.Projection == null)
	// // {
	// // userDef = true;
	// // }
	// // else
	// // {
	// // userDef = false;
	// // }
	//
	// // }
	// // result = userDef;
	// // }
	// // }
	// // }
	// // }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// // return result;
	// //}
	//
	// /// <summary>
	// /// 从文件中获取投影坐标系类型
	// /// </summary>
	// public static PrjCoordSys getPrjCoordSysFromFile(String strFileName)
	// {
	// PrjCoordSys prjCoordSys = null;
	// try
	// {
	// String strExt = String.Empty;
	// Datasource datasource = null;
	// strExt = Path.getExtension(strFileName).ToUpper();
	// switch (strExt)
	// {
	// case ".TIF":
	// case ".TIFF":
	// {
	// ImportSettingTIF importTif = new ImportSettingTIF(strFileName,
	// datasource);
	// prjCoordSys = importTif.getSourcePrjCoordSys();
	// }
	// break;
	// case ".SIT":
	// {
	// ImportSettingSIT importSit = new ImportSettingSIT(strFileName,
	// datasource);
	// prjCoordSys = importSit.getSourcePrjCoordSys();
	// }
	// break;
	// case ".IMG":
	// {
	// ImportSettingIMG importImg = new ImportSettingIMG(strFileName,
	// datasource);
	// prjCoordSys = importImg.getSourcePrjCoordSys();
	// }
	// break;
	// case ".SHP":
	// {
	// ImportSettingSHP importShape = new ImportSettingSHP(strFileName,
	// datasource);
	// prjCoordSys = importShape.getSourcePrjCoordSys();
	// }
	// break;
	// case ".PRJ":
	// {
	// prjCoordSys = new PrjCoordSys();
	// boolean bImported = prjCoordSys.FromFile(strFileName,
	// PrjFileType.Esri);
	// if (!bImported)
	// {
	// prjCoordSys = null;
	// }
	// }
	// break;
	// case ".MIF":
	// {
	// ImportSettingMIF importMif = new ImportSettingMIF(strFileName,
	// datasource);
	// prjCoordSys = importMif.getSourcePrjCoordSys();
	// }
	// break;
	// case ".TAB":
	// {
	// ImportSettingTAB importTab = new ImportSettingTAB(strFileName,
	// datasource);
	// prjCoordSys = importTab.getSourcePrjCoordSys();
	// }
	// break;
	// case ".XML":
	// {
	// prjCoordSys = new PrjCoordSys();
	//
	// boolean bImported = prjCoordSys.FromFile(strFileName,
	// PrjFileType.SuperMap);
	// if (!bImported)
	// {
	// prjCoordSys = null;
	// }
	// }
	// break;
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return prjCoordSys;
	// }

	/**
	 * 获取所有地理坐标系类型及相应的字符串表示字典
	 * 
	 * @return
	 */
	public static HashMap<GeoCoordSysType, String> getGeoCoordSysTypeList() {
		HashMap<GeoCoordSysType, String> coordSysTypeList = new HashMap<GeoCoordSysType, String>();

		coordSysTypeList.put(GeoCoordSysType.GCS_ADINDAN, GeoCoordSysType.GCS_ADINDAN.toString());
		return coordSysTypeList;
		// HashMap<GeoCoordSysType, String> coordSysTypeList = null;
		// Enum[] types=Enum.
		// GeoCoordSysType[] types = Enum.getValues(typeof(GeoCoordSysType)) as
		// GeoCoordSysType[];
		// if (types != null)
		// {
		// coordSysTypeList = new Dictionary<GeoCoordSysType, String>();
		// for (int i = 0; i < types.Length; i++)
		// {
		// coordSysTypeList.Add(types.get(i),types.get(i).ToString());
		// }
		// }
		// return coordSysTypeList;
		// //Dictionary<GeoCoordSysType, String> coordSysTypeList = new
		// Dictionary<GeoCoordSysType, String>();
		// //coordSysTypeList.Clear();
		// //try
		// //{
		// // coordSysTypeList.Add(GeoCoordSysType.Adindan,
		// GeoCoordSysType.Adindan.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Afgooye,
		// GeoCoordSysType.Afgooye.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Agadez,
		// GeoCoordSysType.Agadez.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Agd1966,
		// GeoCoordSysType.Agd1966.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Agd1984,
		// GeoCoordSysType.Agd1984.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.AinElAbd1970,
		// GeoCoordSysType.AinElAbd1970.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Airy1830,
		// GeoCoordSysType.Airy1830.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.AiryMod,
		// GeoCoordSysType.AiryMod.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.AlaskanIslands,
		// GeoCoordSysType.AlaskanIslands.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Amersfoort,
		// GeoCoordSysType.Amersfoort.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Anna11965,
		// GeoCoordSysType.Anna11965.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.AntiguaIsland1943,
		// GeoCoordSysType.AntiguaIsland1943.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Aratu,
		// GeoCoordSysType.Aratu.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Arc1950,
		// GeoCoordSysType.Arc1950.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Arc1960,
		// GeoCoordSysType.Arc1960.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.AscensionIsland1958,
		// GeoCoordSysType.AscensionIsland1958.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Astro1952,
		// GeoCoordSysType.Astro1952.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.AtfParis,
		// GeoCoordSysType.AtfParis.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ats1977,
		// GeoCoordSysType.Ats1977.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Australian,
		// GeoCoordSysType.Australian.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ayabelle,
		// GeoCoordSysType.Ayabelle.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Barbados,
		// GeoCoordSysType.Barbados.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Batavia,
		// GeoCoordSysType.Batavia.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.BataviaJakarta,
		// GeoCoordSysType.BataviaJakarta.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.BeaconE1945,
		// GeoCoordSysType.BeaconE1945.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Beduaram,
		// GeoCoordSysType.Beduaram.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Beijing1954,
		// GeoCoordSysType.Beijing1954.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Belge1950,
		// GeoCoordSysType.Belge1950.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Belge1950Brussels,
		// GeoCoordSysType.Belge1950Brussels.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Belge1972,
		// GeoCoordSysType.Belge1972.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bellevue,
		// GeoCoordSysType.Bellevue.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bermuda1957,
		// GeoCoordSysType.Bermuda1957.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bern1898,
		// GeoCoordSysType.Bern1898.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bern1898Bern,
		// GeoCoordSysType.Bern1898Bern.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bern1938,
		// GeoCoordSysType.Bern1938.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bessel1841,
		// GeoCoordSysType.Bessel1841.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.BesselMod,
		// GeoCoordSysType.BesselMod.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.BesselNamibia,
		// GeoCoordSysType.BesselNamibia.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bissau,
		// GeoCoordSysType.Bissau.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Bogota,
		// GeoCoordSysType.Bogota.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.BogotaBogota,
		// GeoCoordSysType.BogotaBogota.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.BukitRimpah,
		// GeoCoordSysType.BukitRimpah.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Cacanaveral,
		// GeoCoordSysType.Cacanaveral.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Camacupa,
		// GeoCoordSysType.Camacupa.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.CampArea,
		// GeoCoordSysType.CampArea.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.CampoInchauspe,
		// GeoCoordSysType.CampoInchauspe.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Canton1966,
		// GeoCoordSysType.Canton1966.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Cape,
		// GeoCoordSysType.Cape.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Carthage,
		// GeoCoordSysType.Carthage.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.CarthageDegree,
		// GeoCoordSysType.CarthageDegree.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.ChathamIsland1971,
		// GeoCoordSysType.ChathamIsland1971.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.China2000,
		// GeoCoordSysType.China2000.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Chua,
		// GeoCoordSysType.Chua.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1858,
		// GeoCoordSysType.Clarke1858.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1866,
		// GeoCoordSysType.Clarke1866.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1866Mich,
		// GeoCoordSysType.Clarke1866Mich.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1880,
		// GeoCoordSysType.Clarke1880.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1880Arc,
		// GeoCoordSysType.Clarke1880Arc.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1880Benoit,
		// GeoCoordSysType.Clarke1880Benoit.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1880Ign,
		// GeoCoordSysType.Clarke1880Ign.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1880Rgs,
		// GeoCoordSysType.Clarke1880Rgs.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Clarke1880Sga,
		// GeoCoordSysType.Clarke1880Sga.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Conakry1905,
		// GeoCoordSysType.Conakry1905.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.CorregoAlegre,
		// GeoCoordSysType.CorregoAlegre.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.CoteDIvoire,
		// GeoCoordSysType.CoteDIvoire.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Dabola,
		// GeoCoordSysType.Dabola.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Datum73,
		// GeoCoordSysType.Datum73.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.DealulPiscului1933,
		// GeoCoordSysType.DealulPiscului1933.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Dealulpiscului1970,
		// GeoCoordSysType.Dealulpiscului1970.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.DeceptionIsland,
		// GeoCoordSysType.DeceptionIsland.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.DeirEzZor,
		// GeoCoordSysType.DeirEzZor.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Dhdnb,
		// GeoCoordSysType.Dhdnb.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Dos1968,
		// GeoCoordSysType.Dos1968.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Dos714,
		// GeoCoordSysType.Dos714.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Douala,
		// GeoCoordSysType.Douala.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.EasterIsland1967,
		// GeoCoordSysType.EasterIsland1967.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ed1950,
		// GeoCoordSysType.Ed1950.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ed1987,
		// GeoCoordSysType.Ed1987.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Egypt1907,
		// GeoCoordSysType.Egypt1907.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Etrs1989,
		// GeoCoordSysType.Etrs1989.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.European1979,
		// GeoCoordSysType.European1979.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Everest1830,
		// GeoCoordSysType.Everest1830.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.EverestBangladesh,
		// GeoCoordSysType.EverestBangladesh.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.EverestDef1967,
		// GeoCoordSysType.EverestDef1967.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.EverestDef1975,
		// GeoCoordSysType.EverestDef1975.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.EverestIndiaNepal,
		// GeoCoordSysType.EverestIndiaNepal.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.EverestMod,
		// GeoCoordSysType.EverestMod.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.EverestMod1969,
		// GeoCoordSysType.EverestMod1969.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Fahud,
		// GeoCoordSysType.Fahud.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Fischer1960,
		// GeoCoordSysType.Fischer1960.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Fischer1968,
		// GeoCoordSysType.Fischer1968.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.FischerMod,
		// GeoCoordSysType.FischerMod.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.FortThomas1955,
		// GeoCoordSysType.FortThomas1955.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Gan1970,
		// GeoCoordSysType.Gan1970.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Gandajika1970,
		// GeoCoordSysType.Gandajika1970.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Garoua,
		// GeoCoordSysType.Garoua.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Gda1994,
		// GeoCoordSysType.Gda1994.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Gem10c,
		// GeoCoordSysType.Gem10c.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ggrs1987,
		// GeoCoordSysType.Ggrs1987.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Graciosa1948,
		// GeoCoordSysType.Graciosa1948.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Greek,
		// GeoCoordSysType.Greek.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.GreekAthens,
		// GeoCoordSysType.GreekAthens.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Grs1967,
		// GeoCoordSysType.Grs1967.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Grs1980,
		// GeoCoordSysType.Grs1980.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Guam1963,
		// GeoCoordSysType.Guam1963.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.GunungSegara,
		// GeoCoordSysType.GunungSegara.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Gux1,
		// GeoCoordSysType.Gux1.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.GuyaneFrancaise,
		// GeoCoordSysType.GuyaneFrancaise.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Helmert1906,
		// GeoCoordSysType.Helmert1906.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.HeratNorth,
		// GeoCoordSysType.HeratNorth.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.HitoXviii1963,
		// GeoCoordSysType.HitoXviii1963.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Hjorsey1955,
		// GeoCoordSysType.Hjorsey1955.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.HongKong1963,
		// GeoCoordSysType.HongKong1963.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Hough1960,
		// GeoCoordSysType.Hough1960.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Hungarian1972,
		// GeoCoordSysType.Hungarian1972.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.HuTzuShan,
		// GeoCoordSysType.HuTzuShan.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Indian1954,
		// GeoCoordSysType.Indian1954.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Indian1960,
		// GeoCoordSysType.Indian1960.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Indian1975,
		// GeoCoordSysType.Indian1975.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Indonesian,
		// GeoCoordSysType.Indonesian.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Indonesian1974,
		// GeoCoordSysType.Indonesian1974.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.International1924,
		// GeoCoordSysType.International1924.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.International1967,
		// GeoCoordSysType.International1967.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ists0611968,
		// GeoCoordSysType.Ists0611968.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ists0731969,
		// GeoCoordSysType.Ists0731969.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Jamaica1875,
		// GeoCoordSysType.Jamaica1875.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Jamaica1969,
		// GeoCoordSysType.Jamaica1969.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Japan2000,
		// GeoCoordSysType.Japan2000.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.JohnstonIsland1961,
		// GeoCoordSysType.JohnstonIsland1961.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Kalianpur,
		// GeoCoordSysType.Kalianpur.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Kandawala,
		// GeoCoordSysType.Kandawala.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.KerguelenIsland1949,
		// GeoCoordSysType.KerguelenIsland1949.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Kertau,
		// GeoCoordSysType.Kertau.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Kkj,
		// GeoCoordSysType.Kkj.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Koc,
		// GeoCoordSysType.Koc.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Krasovsky1940,
		// GeoCoordSysType.Krasovsky1940.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Kudams,
		// GeoCoordSysType.Kudams.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Kusaie1951,
		// GeoCoordSysType.Kusaie1951.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.LaCanoa,
		// GeoCoordSysType.LaCanoa.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Lake,
		// GeoCoordSysType.Lake.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Lc51961,
		// GeoCoordSysType.Lc51961.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Leigon,
		// GeoCoordSysType.Leigon.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Liberia1964,
		// GeoCoordSysType.Liberia1964.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Lisbon,
		// GeoCoordSysType.Lisbon.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Lisbonlisbon,
		// GeoCoordSysType.Lisbonlisbon.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.LomaQuintana,
		// GeoCoordSysType.LomaQuintana.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Lome,
		// GeoCoordSysType.Lome.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Luzon1911,
		// GeoCoordSysType.Luzon1911.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Mahe1971,
		// GeoCoordSysType.Mahe1971.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Makassar,
		// GeoCoordSysType.Makassar.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.MakassarJakarta,
		// GeoCoordSysType.MakassarJakarta.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Malongo1987,
		// GeoCoordSysType.Malongo1987.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Manoca,
		// GeoCoordSysType.Manoca.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Massawa,
		// GeoCoordSysType.Massawa.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Merchich,
		// GeoCoordSysType.Merchich.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Mgi,
		// GeoCoordSysType.Mgi.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.MgiFerro,
		// GeoCoordSysType.MgiFerro.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Mhast,
		// GeoCoordSysType.Mhast.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Midway1961,
		// GeoCoordSysType.Midway1961.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Minna,
		// GeoCoordSysType.Minna.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.MonteMario,
		// GeoCoordSysType.MonteMario.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.MonteMarioRome,
		// GeoCoordSysType.MonteMarioRome.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.MontserratIsland1958,
		// GeoCoordSysType.MontserratIsland1958.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Mporaloko,
		// GeoCoordSysType.Mporaloko.ToString());
		//
		// // coordSysTypeList.Add(GeoCoordSysType.Nad1927,
		// GeoCoordSysType.Nad1927.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Nad1983,
		// GeoCoordSysType.Nad1983.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.NadMich,
		// GeoCoordSysType.NadMich.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Nahrwan1967,
		// GeoCoordSysType.Nahrwan1967.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Naparima1972,
		// GeoCoordSysType.Naparima1972.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.NdgParis,
		// GeoCoordSysType.NdgParis.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ngn,
		// GeoCoordSysType.Ngn.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ngo1948,
		// GeoCoordSysType.Ngo1948.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.NordSahara1959,
		// GeoCoordSysType.NordSahara1959.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Nswc9z2,
		// GeoCoordSysType.Nswc9z2.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Ntf,
		// GeoCoordSysType.Ntf.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.NtfParis,
		// GeoCoordSysType.NtfParis.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Nwl9d,
		// GeoCoordSysType.Nwl9d.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Nzgd1949,
		// GeoCoordSysType.Nzgd1949.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.ObservMeteor1939,
		// GeoCoordSysType.ObservMeteor1939.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.OldHawaiian,
		// GeoCoordSysType.OldHawaiian.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Oman,
		// GeoCoordSysType.Oman.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Osgb1936,
		// GeoCoordSysType.Osgb1936.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Osgb1970Sn,
		// GeoCoordSysType.Osgb1970Sn.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.OsSn1980,
		// GeoCoordSysType.OsSn1980.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Osu86f,
		// GeoCoordSysType.Osu86f.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Osu91a,
		// GeoCoordSysType.Osu91a.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Padang1884,
		// GeoCoordSysType.Padang1884.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Padang1884Jakarta,
		// GeoCoordSysType.Padang1884Jakarta.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Palestine1923,
		// GeoCoordSysType.Palestine1923.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.PicoDeLasNieves,
		// GeoCoordSysType.PicoDeLasNieves.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Pitcairn1967,
		// GeoCoordSysType.Pitcairn1967.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Plessis1817,
		// GeoCoordSysType.Plessis1817.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Point58,
		// GeoCoordSysType.Point58.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.PointeNoire,
		// GeoCoordSysType.PointeNoire.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.PortoSanto1936,
		// GeoCoordSysType.PortoSanto1936.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Psad1956,
		// GeoCoordSysType.Psad1956.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.PuertoRico,
		// GeoCoordSysType.PuertoRico.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Pulkovo1942,
		// GeoCoordSysType.Pulkovo1942.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Pulkovo1995,
		// GeoCoordSysType.Pulkovo1995.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Qatar,
		// GeoCoordSysType.Qatar.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Qatar1948,
		// GeoCoordSysType.Qatar1948.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Qornoq,
		// GeoCoordSysType.Qornoq.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Reunion,
		// GeoCoordSysType.Reunion.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Rt38,
		// GeoCoordSysType.Rt38.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Rt38Stockholm,
		// GeoCoordSysType.Rt38Stockholm.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.S42Hungary,
		// GeoCoordSysType.S42Hungary.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Sad1969,
		// GeoCoordSysType.Sad1969.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Samoa1962,
		// GeoCoordSysType.Samoa1962.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SantoDos1965,
		// GeoCoordSysType.SantoDos1965.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SaoBraz,
		// GeoCoordSysType.SaoBraz.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SapperHill1943,
		// GeoCoordSysType.SapperHill1943.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SAsiaSingapore,
		// GeoCoordSysType.SAsiaSingapore.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Schwarzeck,
		// GeoCoordSysType.Schwarzeck.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Segora,
		// GeoCoordSysType.Segora.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SelvagemGrande1938,
		// GeoCoordSysType.SelvagemGrande1938.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Serindung,
		// GeoCoordSysType.Serindung.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SJtsk,
		// GeoCoordSysType.SJtsk.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Sphere,
		// GeoCoordSysType.Sphere.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.SphereAi,
		// GeoCoordSysType.SphereAi.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Struve1860,
		// GeoCoordSysType.Struve1860.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Sudan,
		// GeoCoordSysType.Sudan.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Tananarive1925,
		// GeoCoordSysType.Tananarive1925.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Tananarive1925Paris,
		// GeoCoordSysType.Tananarive1925Paris.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.TernIsland1961,
		// GeoCoordSysType.TernIsland1961.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Timbalai1948,
		// GeoCoordSysType.Timbalai1948.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Tm65,
		// GeoCoordSysType.Tm65.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Tm75,
		// GeoCoordSysType.Tm75.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Tokyo,
		// GeoCoordSysType.Tokyo.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Trinidad1903,
		// GeoCoordSysType.Trinidad1903.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Tristan1968,
		// GeoCoordSysType.Tristan1968.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.TrucialCoast1948,
		// GeoCoordSysType.TrucialCoast1948.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.VitiLevu1916,
		// GeoCoordSysType.VitiLevu1916.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Voirol1875,
		// GeoCoordSysType.Voirol1875.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Voirol1875Paris,
		// GeoCoordSysType.Voirol1875Paris.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.VoirolUnifie1960,
		// GeoCoordSysType.VoirolUnifie1960.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.VoirolUnifie1960Paris,
		// GeoCoordSysType.VoirolUnifie1960Paris.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.WakeEniwetok1960,
		// GeoCoordSysType.WakeEniwetok1960.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.WakeIsland1952,
		// GeoCoordSysType.WakeIsland1952.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Walbeck,
		// GeoCoordSysType.Walbeck.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.WarOffice,
		// GeoCoordSysType.WarOffice.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Wgs1966,
		// GeoCoordSysType.Wgs1966.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Wgs1972,
		// GeoCoordSysType.Wgs1972.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Wgs1972Be,
		// GeoCoordSysType.Wgs1972Be.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Wgs1984,
		// GeoCoordSysType.Wgs1984.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Xian1980,
		// GeoCoordSysType.Xian1980.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Yacare,
		// GeoCoordSysType.Yacare.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Yoff,
		// GeoCoordSysType.Yoff.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.Zanderij,
		// GeoCoordSysType.Zanderij.ToString());
		// // coordSysTypeList.Add(GeoCoordSysType.UserDefined,
		// GeoCoordSysType.UserDefined.ToString());
		// //}catch (Exception ex) {
		// Application.getActiveApplication().getOutput().output(ex);
		// }
		// return coordSysTypeList;
	}

	// /// <summary>
	// /// 获取所有的大地参考系类型数组
	// /// </summary>
	// public static GeoDatumType[] getGeoDatumTypeList()
	// {
	// return Enum.getValues(typeof(GeoDatumType)) as GeoDatumType[];
	// //List<GeoDatumType> geoDatumTypeList = new List<GeoDatumType>();
	// //geoDatumTypeList.Clear();
	// //try
	// //{
	// // geoDatumTypeList.Add(GeoDatumType.Adindan);
	// // geoDatumTypeList.Add(GeoDatumType.Afgooye);
	// // geoDatumTypeList.Add(GeoDatumType.Agadez);
	// // geoDatumTypeList.Add(GeoDatumType.Agd1966);
	// // geoDatumTypeList.Add(GeoDatumType.Agd1984);
	// // geoDatumTypeList.Add(GeoDatumType.AinElAbd1970);
	// // geoDatumTypeList.Add(GeoDatumType.Airy1830);
	// // geoDatumTypeList.Add(GeoDatumType.AiryMod);
	// // geoDatumTypeList.Add(GeoDatumType.AlaskanIslands);
	// // geoDatumTypeList.Add(GeoDatumType.Amersfoort);
	// // geoDatumTypeList.Add(GeoDatumType.Anna11965);
	// // geoDatumTypeList.Add(GeoDatumType.AntiguaIsland1943);
	// // geoDatumTypeList.Add(GeoDatumType.Aratu);
	// // geoDatumTypeList.Add(GeoDatumType.Arc1950);
	// // geoDatumTypeList.Add(GeoDatumType.Arc1960);
	// // geoDatumTypeList.Add(GeoDatumType.AscensionIsland1958);
	// // geoDatumTypeList.Add(GeoDatumType.Astro1952);
	// // geoDatumTypeList.Add(GeoDatumType.Atf);
	// // geoDatumTypeList.Add(GeoDatumType.Ats1977);
	// // geoDatumTypeList.Add(GeoDatumType.Australian);
	// // geoDatumTypeList.Add(GeoDatumType.Ayabelle);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Barbados);
	// // geoDatumTypeList.Add(GeoDatumType.Batavia);
	// // geoDatumTypeList.Add(GeoDatumType.BeaconE1945);
	// // geoDatumTypeList.Add(GeoDatumType.Beduaram);
	// // geoDatumTypeList.Add(GeoDatumType.Beijing1954);
	// // geoDatumTypeList.Add(GeoDatumType.Belge1950);
	// // geoDatumTypeList.Add(GeoDatumType.Belge1972);
	// // geoDatumTypeList.Add(GeoDatumType.Bellevue);
	// // geoDatumTypeList.Add(GeoDatumType.Bermuda1957);
	// // geoDatumTypeList.Add(GeoDatumType.Bern1898);
	// // geoDatumTypeList.Add(GeoDatumType.Bern1938);
	// // geoDatumTypeList.Add(GeoDatumType.Bessel1841);
	// // geoDatumTypeList.Add(GeoDatumType.BesselMod);
	// // geoDatumTypeList.Add(GeoDatumType.BesselNamibia);
	// // geoDatumTypeList.Add(GeoDatumType.Bissau);
	// // geoDatumTypeList.Add(GeoDatumType.Bogota);
	// // geoDatumTypeList.Add(GeoDatumType.BukitRimpah);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Cacanaveral);
	// // geoDatumTypeList.Add(GeoDatumType.Camacupa);
	// // geoDatumTypeList.Add(GeoDatumType.CampArea);
	// // geoDatumTypeList.Add(GeoDatumType.CampoInchauspe);
	// // geoDatumTypeList.Add(GeoDatumType.Canton1966);
	// // geoDatumTypeList.Add(GeoDatumType.Cape);
	// // geoDatumTypeList.Add(GeoDatumType.Carthage);
	// // geoDatumTypeList.Add(GeoDatumType.ChathamIsland1971);
	// // geoDatumTypeList.Add(GeoDatumType.China2000);
	// // geoDatumTypeList.Add(GeoDatumType.Chua);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1858);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1866);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1866Mich);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1880);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1880Arc);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1880Benoit);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1880Ign);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1880Rgs);
	// // geoDatumTypeList.Add(GeoDatumType.Clarke1880Sga);
	// // geoDatumTypeList.Add(GeoDatumType.Conakry1905);
	// // geoDatumTypeList.Add(GeoDatumType.CorregoAlegre);
	// // geoDatumTypeList.Add(GeoDatumType.CoteDIvoire);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Dabola);
	// // geoDatumTypeList.Add(GeoDatumType.Datum73);
	// // geoDatumTypeList.Add(GeoDatumType.DealulPiscului1933);
	// // geoDatumTypeList.Add(GeoDatumType.DealulPiscului1970);
	// // geoDatumTypeList.Add(GeoDatumType.DeceptionIsland);
	// // geoDatumTypeList.Add(GeoDatumType.DeirEzZor);
	// // geoDatumTypeList.Add(GeoDatumType.Dhdn);
	// // geoDatumTypeList.Add(GeoDatumType.Dos1968);
	// // geoDatumTypeList.Add(GeoDatumType.Dos714);
	// // geoDatumTypeList.Add(GeoDatumType.Douala);
	//
	// // geoDatumTypeList.Add(GeoDatumType.EasterIsland1967);
	// // geoDatumTypeList.Add(GeoDatumType.Ed1950);
	// // geoDatumTypeList.Add(GeoDatumType.Ed1987);
	// // geoDatumTypeList.Add(GeoDatumType.Egypt1907);
	// // geoDatumTypeList.Add(GeoDatumType.Etrs1989);
	// // geoDatumTypeList.Add(GeoDatumType.European1979);
	// // geoDatumTypeList.Add(GeoDatumType.Everest1830);
	// // geoDatumTypeList.Add(GeoDatumType.EverestBangladesh);
	// // geoDatumTypeList.Add(GeoDatumType.EverestDef1967);
	// // geoDatumTypeList.Add(GeoDatumType.EverestDef1975);
	// // geoDatumTypeList.Add(GeoDatumType.EverestIndiaNepal);
	// // geoDatumTypeList.Add(GeoDatumType.EverestMod);
	// // geoDatumTypeList.Add(GeoDatumType.EverestMod1969);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Fahud);
	// // geoDatumTypeList.Add(GeoDatumType.Fischer1960);
	// // geoDatumTypeList.Add(GeoDatumType.Fischer1968);
	// // geoDatumTypeList.Add(GeoDatumType.FischerMod);
	// // geoDatumTypeList.Add(GeoDatumType.FortThomas1955);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Gan1970);
	// // geoDatumTypeList.Add(GeoDatumType.Gandajika1970);
	// // geoDatumTypeList.Add(GeoDatumType.Garoua);
	// // geoDatumTypeList.Add(GeoDatumType.Gda1994);
	// // geoDatumTypeList.Add(GeoDatumType.Gem10c);
	// // geoDatumTypeList.Add(GeoDatumType.Ggrs1987);
	// // geoDatumTypeList.Add(GeoDatumType.Graciosa1948);
	// // geoDatumTypeList.Add(GeoDatumType.Greek);
	// // geoDatumTypeList.Add(GeoDatumType.Grs1967);
	// // geoDatumTypeList.Add(GeoDatumType.Grs1980);
	// // geoDatumTypeList.Add(GeoDatumType.Guam1963);
	// // geoDatumTypeList.Add(GeoDatumType.GunungSegara);
	// // geoDatumTypeList.Add(GeoDatumType.Gux1);
	// // geoDatumTypeList.Add(GeoDatumType.GuyaneFrancaise);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Helmert1906);
	// // geoDatumTypeList.Add(GeoDatumType.HeratNorth);
	// // geoDatumTypeList.Add(GeoDatumType.HitoXviii1963);
	// // geoDatumTypeList.Add(GeoDatumType.Hjorsey1955);
	// // geoDatumTypeList.Add(GeoDatumType.Hong_kong1963);
	// // geoDatumTypeList.Add(GeoDatumType.Hough1960);
	// // geoDatumTypeList.Add(GeoDatumType.Hungarian1972);
	// // geoDatumTypeList.Add(GeoDatumType.HuTzuShan);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Indian1954);
	// // geoDatumTypeList.Add(GeoDatumType.Indian1960);
	// // geoDatumTypeList.Add(GeoDatumType.Indian1975);
	// // geoDatumTypeList.Add(GeoDatumType.Indonesian);
	// // geoDatumTypeList.Add(GeoDatumType.Indonesian1974);
	// // geoDatumTypeList.Add(GeoDatumType.International1924);
	// // geoDatumTypeList.Add(GeoDatumType.International1967);
	// // geoDatumTypeList.Add(GeoDatumType.Ists0611968);
	// // geoDatumTypeList.Add(GeoDatumType.Ists0731969);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Jamaica1875);
	// // geoDatumTypeList.Add(GeoDatumType.Jamaica1969);
	// // geoDatumTypeList.Add(GeoDatumType.Japan2000);
	// // geoDatumTypeList.Add(GeoDatumType.JohnstonIsland1961);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Kalianpur);
	// // geoDatumTypeList.Add(GeoDatumType.Kandawala);
	// // geoDatumTypeList.Add(GeoDatumType.KerguelenIsland1949);
	// // geoDatumTypeList.Add(GeoDatumType.Kertau);
	// // geoDatumTypeList.Add(GeoDatumType.Kkj);
	// // geoDatumTypeList.Add(GeoDatumType.Koc);
	// // geoDatumTypeList.Add(GeoDatumType.Krasovsky1940);
	// // geoDatumTypeList.Add(GeoDatumType.Kudams);
	// // geoDatumTypeList.Add(GeoDatumType.Kusaie1951);
	//
	// // geoDatumTypeList.Add(GeoDatumType.LaCanoa);
	// // geoDatumTypeList.Add(GeoDatumType.Lake);
	// // geoDatumTypeList.Add(GeoDatumType.Lc51961);
	// // geoDatumTypeList.Add(GeoDatumType.Leigon);
	// // geoDatumTypeList.Add(GeoDatumType.Liberia1964);
	// // geoDatumTypeList.Add(GeoDatumType.Lisbon);
	// // geoDatumTypeList.Add(GeoDatumType.LomaQuintana);
	// // geoDatumTypeList.Add(GeoDatumType.Lome);
	// // geoDatumTypeList.Add(GeoDatumType.Luzon1911);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Mahe1971);
	// // geoDatumTypeList.Add(GeoDatumType.Makassar);
	// // geoDatumTypeList.Add(GeoDatumType.Malongo1987);
	// // geoDatumTypeList.Add(GeoDatumType.Manoca);
	// // geoDatumTypeList.Add(GeoDatumType.Massawa);
	// // geoDatumTypeList.Add(GeoDatumType.Merchich);
	// // geoDatumTypeList.Add(GeoDatumType.Mgi);
	// // geoDatumTypeList.Add(GeoDatumType.Mhast);
	// // geoDatumTypeList.Add(GeoDatumType.Midway1961);
	// // geoDatumTypeList.Add(GeoDatumType.Minna);
	// // geoDatumTypeList.Add(GeoDatumType.Montemario);
	// // geoDatumTypeList.Add(GeoDatumType.MontserratIsland1958);
	// // geoDatumTypeList.Add(GeoDatumType.Mporaloko);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Nad1927);
	// // geoDatumTypeList.Add(GeoDatumType.Nad1983);
	// // geoDatumTypeList.Add(GeoDatumType.Nadmich);
	// // geoDatumTypeList.Add(GeoDatumType.Nahrwan1967);
	// // geoDatumTypeList.Add(GeoDatumType.Naparima1972);
	// // geoDatumTypeList.Add(GeoDatumType.Ndg);
	// // geoDatumTypeList.Add(GeoDatumType.Ngn);
	// // geoDatumTypeList.Add(GeoDatumType.Ngo1948);
	// // geoDatumTypeList.Add(GeoDatumType.Nord_sahara1959);
	// // geoDatumTypeList.Add(GeoDatumType.Nswc9z2);
	// // geoDatumTypeList.Add(GeoDatumType.Ntf);
	// // geoDatumTypeList.Add(GeoDatumType.Nwl9d);
	// // geoDatumTypeList.Add(GeoDatumType.Nzgd1949);
	//
	// // geoDatumTypeList.Add(GeoDatumType.ObservMeteor1939);
	// // geoDatumTypeList.Add(GeoDatumType.OldHawaiian);
	// // geoDatumTypeList.Add(GeoDatumType.Oman);
	// // geoDatumTypeList.Add(GeoDatumType.Os_sn1980);
	// // geoDatumTypeList.Add(GeoDatumType.Osgb1936);
	// // geoDatumTypeList.Add(GeoDatumType.Osgb1970Sn);
	// // geoDatumTypeList.Add(GeoDatumType.Osu86f);
	// // geoDatumTypeList.Add(GeoDatumType.Osu91a);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Padang1884);
	// // geoDatumTypeList.Add(GeoDatumType.Palestine1923);
	// // geoDatumTypeList.Add(GeoDatumType.PicoDeLasNieves);
	// // geoDatumTypeList.Add(GeoDatumType.Pitcairn1967);
	// // geoDatumTypeList.Add(GeoDatumType.Plessis1817);
	// // geoDatumTypeList.Add(GeoDatumType.Point58);
	// // geoDatumTypeList.Add(GeoDatumType.PointeNoire);
	// // geoDatumTypeList.Add(GeoDatumType.PortoSanto1936);
	// // geoDatumTypeList.Add(GeoDatumType.Psad1956);
	// // geoDatumTypeList.Add(GeoDatumType.PuertoRico);
	// // geoDatumTypeList.Add(GeoDatumType.Pulkovo1942);
	// // geoDatumTypeList.Add(GeoDatumType.Pulkovo1995);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Qatar);
	// // geoDatumTypeList.Add(GeoDatumType.Qatar1948);
	// // geoDatumTypeList.Add(GeoDatumType.Qornoq);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Reunion);
	//
	// // geoDatumTypeList.Add(GeoDatumType.S42Hungary);
	// // geoDatumTypeList.Add(GeoDatumType.Sad1969);
	// // geoDatumTypeList.Add(GeoDatumType.Samoa1962);
	// // geoDatumTypeList.Add(GeoDatumType.SantoDos1965);
	// // geoDatumTypeList.Add(GeoDatumType.SaoBraz);
	// // geoDatumTypeList.Add(GeoDatumType.SapperHill1943);
	// // geoDatumTypeList.Add(GeoDatumType.SAsiaSingapore);
	// // geoDatumTypeList.Add(GeoDatumType.Schwarzeck);
	// // geoDatumTypeList.Add(GeoDatumType.Segora);
	// // geoDatumTypeList.Add(GeoDatumType.SelvagemGrande1938);
	// // geoDatumTypeList.Add(GeoDatumType.Serindung);
	// // geoDatumTypeList.Add(GeoDatumType.SJtsk);
	// // geoDatumTypeList.Add(GeoDatumType.Sphere);
	// // geoDatumTypeList.Add(GeoDatumType.SphereAi);
	// // geoDatumTypeList.Add(GeoDatumType.Stockholm1938);
	// // geoDatumTypeList.Add(GeoDatumType.Struve1860);
	// // geoDatumTypeList.Add(GeoDatumType.Sudan);
	//
	// // geoDatumTypeList.Add(GeoDatumType.Tananarive1925);
	// // geoDatumTypeList.Add(GeoDatumType.TernIsland1961);
	// // geoDatumTypeList.Add(GeoDatumType.Timbalai1948);
	// // geoDatumTypeList.Add(GeoDatumType.Tm65);
	// // geoDatumTypeList.Add(GeoDatumType.Tm75);
	// // geoDatumTypeList.Add(GeoDatumType.Tokyo);
	// // geoDatumTypeList.Add(GeoDatumType.Trinidad1903);
	// // geoDatumTypeList.Add(GeoDatumType.Tristan1968);
	// // geoDatumTypeList.Add(GeoDatumType.TrucialCoast1948);
	//
	// // geoDatumTypeList.Add(GeoDatumType.VitiLevu1916);
	// // geoDatumTypeList.Add(GeoDatumType.Voirol1875);
	// // geoDatumTypeList.Add(GeoDatumType.VoirolUnifie1960);
	// // geoDatumTypeList.Add(GeoDatumType.WakeEniwetok1960);
	// // geoDatumTypeList.Add(GeoDatumType.WakeIsland1952);
	// // geoDatumTypeList.Add(GeoDatumType.Walbeck);
	// // geoDatumTypeList.Add(GeoDatumType.Waroffice);
	// // geoDatumTypeList.Add(GeoDatumType.Wgs1966);
	// // geoDatumTypeList.Add(GeoDatumType.Wgs1972);
	// // geoDatumTypeList.Add(GeoDatumType.Wgs1972Be);
	// // geoDatumTypeList.Add(GeoDatumType.BeaconE1945);
	// // geoDatumTypeList.Add(GeoDatumType.Wgs1984);
	// // geoDatumTypeList.Add(GeoDatumType.Xian1980);
	// // geoDatumTypeList.Add(GeoDatumType.Yacare);
	// // geoDatumTypeList.Add(GeoDatumType.Yoff);
	// // geoDatumTypeList.Add(GeoDatumType.Zanderij);
	// // geoDatumTypeList.Add(GeoDatumType.UserDefined);
	// //}catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// //return geoDatumTypeList.ToArray();
	// }
	//
	// /// <summary>
	// /// 获取所有的中央经线类型数组
	// /// </summary>
	// /// <returns></returns>
	// public static GeoPrimeMeridianType[] getGeoPrimeMeridianTypeList()
	// {
	// return Enum.getValues(typeof(GeoPrimeMeridianType)) as
	// GeoPrimeMeridianType[];
	// //List<GeoPrimeMeridianType> geoPrimeMeridianTypeList = new
	// List<GeoPrimeMeridianType>();
	// //geoPrimeMeridianTypeList.Clear();
	// //try
	// //{
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Athens);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Bern);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Bogota);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Brussels);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Ferro);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Greenwich);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Jakarta);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Lisbon);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Madrid);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Paris);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Rome);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.Stockholm);
	// // geoPrimeMeridianTypeList.Add(GeoPrimeMeridianType.UserDefined);
	// //}catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// //return geoPrimeMeridianTypeList.ToArray();
	// }
	//
	// /// <summary>
	// /// 获取所有的地球椭球体类型数组
	// /// </summary>
	// public static GeoSpheroidType[] getGeoSpheroidTypeList()
	// {
	// return Enum.getValues(typeof(GeoSpheroidType)) as GeoSpheroidType[];
	// //try
	// //{
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Airy1830);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.AiryMod);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Ats1977);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Australian);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Bessel1841);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.BesselMod);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.BesselNamibia);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.China2000);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1858);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1866);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1866Mich);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1880);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1880Arc);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1880Benoit);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1880Ign);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1880Rgs);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Clarke1880Sga);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Everest1830);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.EverestDef1967);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.EverestDef1975);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.EverestMod);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.EverestMod1969);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Fischer1960);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Fischer1968);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.FischerMod);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Gem10c);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Grs1967);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Grs1980);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Helmert1906);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Hough1960);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Indonesian);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.International1924);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.International1967);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.International1975);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Krasovsky1940);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Nwl10d);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Nwl9d);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Osu86f);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Osu91a);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Plessis1817);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Sphere);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.SphereAi);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Struve1860);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Walbeck);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Waroffice);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Wgs1966);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Wgs1972);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.Wgs1984);
	// // geoSpheroidTypeList.Add(GeoSpheroidType.UserDefined);
	// //}catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// //return geoSpheroidTypeList.ToArray();
	// }
	//
	// /// <summary>
	// /// 获取所有投影方式类型数组
	// /// </summary>
	// public static ProjectionType[] getProjectionTypeList()
	// {
	// return Enum.getValues(typeof(ProjectionType)) as ProjectionType[];
	// //List<ProjectionType> ProjectionTypeList = new
	// List<ProjectionType>();
	// //ProjectionTypeList.Clear();
	// //try
	// //{
	// // ProjectionTypeList.Add(ProjectionType.NoneProjection);
	// // ProjectionTypeList.Add(ProjectionType.Albers);
	// // ProjectionTypeList.Add(ProjectionType.Behrmann);
	// // ProjectionTypeList.Add(ProjectionType.Bonne);
	// // ProjectionTypeList.Add(ProjectionType.Cassini);
	// // ProjectionTypeList.Add(ProjectionType.ChinaAzimuthal);
	// // ProjectionTypeList.Add(ProjectionType.ConformalAzimuthal);
	// // ProjectionTypeList.Add(ProjectionType.EckertI);
	// // ProjectionTypeList.Add(ProjectionType.EckertII);
	// // ProjectionTypeList.Add(ProjectionType.EckertIII);
	// // ProjectionTypeList.Add(ProjectionType.EckertIV);
	// // ProjectionTypeList.Add(ProjectionType.EckertV);
	// // ProjectionTypeList.Add(ProjectionType.EckertVI);
	// // ProjectionTypeList.Add(ProjectionType.EqualareaCylindrical);
	// // ProjectionTypeList.Add(ProjectionType.EquidistantAzimuthal);
	// // ProjectionTypeList.Add(ProjectionType.EquidistantConic);
	// // ProjectionTypeList.Add(ProjectionType.EquidistantCylindrical);
	// // ProjectionTypeList.Add(ProjectionType.GallStereographic);
	// // ProjectionTypeList.Add(ProjectionType.GaussKruger);
	// // ProjectionTypeList.Add(ProjectionType.Gnomonic);
	// // ProjectionTypeList.Add(ProjectionType.Hotine);
	// // ProjectionTypeList.Add(ProjectionType.HotineAzimuthNatorigin);
	// // ProjectionTypeList.Add(ProjectionType.HotineObliqueMercator);
	// // ProjectionTypeList.Add(ProjectionType.LambertAzimuthalEqualArea);
	// // ProjectionTypeList.Add(ProjectionType.LambertConformalConic);
	// // ProjectionTypeList.Add(ProjectionType.Loximuthal);
	// // ProjectionTypeList.Add(ProjectionType.Mercator);
	// // ProjectionTypeList.Add(ProjectionType.MillerCylindrical);
	// // ProjectionTypeList.Add(ProjectionType.Mollweide);
	// // ProjectionTypeList.Add(ProjectionType.ObliqueMercator);
	// // ProjectionTypeList.Add(ProjectionType.ObliqueStereographic);
	// // ProjectionTypeList.Add(ProjectionType.OrthoGraphic);
	// // ProjectionTypeList.Add(ProjectionType.PlateCarree);
	// // ProjectionTypeList.Add(ProjectionType.Polyconic);
	// // ProjectionTypeList.Add(ProjectionType.QuarticAuthalic);
	// // ProjectionTypeList.Add(ProjectionType.Robinson);
	// // ProjectionTypeList.Add(ProjectionType.Sanson);
	// // ProjectionTypeList.Add(ProjectionType.Sinusoidal);
	// // ProjectionTypeList.Add(ProjectionType.SphereMercator);
	// // ProjectionTypeList.Add(ProjectionType.Stereographic);
	// // ProjectionTypeList.Add(ProjectionType.TransverseMercator);
	// // ProjectionTypeList.Add(ProjectionType.TwoPointEquidistant);
	// // ProjectionTypeList.Add(ProjectionType.VanDerGrintenI);
	// // ProjectionTypeList.Add(ProjectionType.WinkelI);
	// // ProjectionTypeList.Add(ProjectionType.WinkelII);
	// //}catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// //return ProjectionTypeList.ToArray();
	// }
	//
	public static String getDescription(PrjCoordSys prjCoordSys) {
		String result = "";
		try {
			if (prjCoordSys.getType() == PrjCoordSysType.PCS_NON_EARTH) {
				result = CoreProperties.getString("String_NoProjectionParameter") + "----" + prjCoordSys.getCoordUnit().toString();
			} else {
				String[] earthFrameOfReferenceinfos = new String[] { CoreProperties.getString("String_GeoCoordSys_GeodeticCoordinateSystem"),
						CoreProperties.getString("String_GeoCoordSys_ReferenceSpheroid"), CoreProperties.getString("String_GeoSpheroid_Axis"),
						CoreProperties.getString("String_GeoSpheroid_Flatten") };
				ArrayList<String> infoLabels = new ArrayList<String>();
				if (prjCoordSys.getType() == PrjCoordSysType.PCS_EARTH_LONGITUDE_LATITUDE) {
					for (String string : earthFrameOfReferenceinfos) {
						infoLabels.add(string);
					}
				} else {
					String[] prjInfo = new String[] { CoreProperties.getString("String_Projection_ProjectionType"),
							CoreProperties.getString("String_PrjParameter_CenterMeridian"), CoreProperties.getString("String_PrjParameter_CentralParallel"),
							CoreProperties.getString("String_PrjParameter_StandardParallel1"),
							CoreProperties.getString("String_PrjParameter_StandardParallel2"), CoreProperties.getString("String_PrjParameter_FalseEasting"),
							CoreProperties.getString("String_PrjParameter_FalseNorthing"), CoreProperties.getString("String_PrjParameter_ScaleFactor"),
							CoreProperties.getString("String_PrjParameter_Azimuth"), CoreProperties.getString("String_PrjParameter_FirstPointLongitude"),
							CoreProperties.getString("String_PrjParameter_SecondPointLongitude"), CoreProperties.getString("String_GeoCoordSys_Name") };

					for (String string : prjInfo) {
						infoLabels.add(string);
					}

					for (String string : earthFrameOfReferenceinfos) {
						infoLabels.add(string);
					}
				}
				result = setInformation(infoLabels, prjCoordSys);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}

	//
	// public static void SendPrjCoordSysChanged(PrjCoordSys prjCoordSys,
	// DataType type)
	// {
	// if (PrjCoordSysChangedEvent != null)
	// {
	// PrjCoordSysChangedEvent(null, new
	// PrjCoordSysChangedEventArgs(prjCoordSys, type));
	// }
	// }
	// #endregion
	//
	// #region Function_Event
	//
	// #endregion
	//
	// #region Function_Private
	private static String setInformation(ArrayList<String> infos, PrjCoordSys prj) {
		String text = "";
		try {
			if (infos != null && prj != null) {
				for (String info : infos) {
					text += MessageFormat.format("{0}\t", info); // 设置固定长度，对齐第二列
					if (info.equals(CoreProperties.getString("String_GeoCoordSys_GeodeticCoordinateSystem"))) {
						text += prj.getGeoCoordSys().getGeoDatum().getName();
					} else if (info.equals(CoreProperties.getString("String_GeoCoordSys_ReferenceSpheroid"))) {
						text += prj.getGeoCoordSys().getGeoDatum().getGeoSpheroid().getName();
					} else if (info.equals(CoreProperties.getString("String_GeoSpheroid_Axis"))) {
						text += Double.toString(prj.getGeoCoordSys().getGeoDatum().getGeoSpheroid().getAxis());
					} else if (info.equals(CoreProperties.getString("String_GeoSpheroid_Flatten"))) {
						text += prj.getGeoCoordSys().getGeoDatum().getGeoSpheroid().getFlatten();
					} else if (info.equals(CoreProperties.getString("String_Projection_ProjectionType"))) {
						text += prj.getProjection().getType().toString();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_CenterMeridian"))) {
						text += prj.getPrjParameter().getCentralMeridian();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_CentralParallel"))) {
						text += prj.getPrjParameter().getCentralParallel();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_StandardParallel1"))) {
						text += prj.getPrjParameter().getStandardParallel1();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_StandardParallel2"))) {
						text += prj.getPrjParameter().getStandardParallel2();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_FalseEasting"))) {
						text += prj.getPrjParameter().getFalseEasting();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_FalseNorthing"))) {
						text += prj.getPrjParameter().getFalseNorthing();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_ScaleFactor"))) {
						text += prj.getPrjParameter().getScaleFactor();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_Azimuth"))) {
						text += prj.getPrjParameter().getAzimuth();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_FirstPointLongitude"))) {
						text += prj.getPrjParameter().getFirstPointLongitude();
					} else if (info.equals(CoreProperties.getString("String_PrjParameter_SecondPointLongitude"))) {
						text += prj.getPrjParameter().getSecondPointLongitude();
					} else if (info.equals(CoreProperties.getString("String_GeoCoordSys_Name"))) {
						text += prj.getGeoCoordSys().getName();
					}
					text += System.lineSeparator();
				}
				text = text.substring(0, text.length() - 2);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return text;
	}
	// #endregion
	//
	// #region Event
	// public static event PrjCoordSysChangedEventHandler
	// PrjCoordSysChangedEvent;
	// public delegate void PrjCoordSysChangedEventHandler(object sender,
	// PrjCoordSysChangedEventArgs e);
	// public class PrjCoordSysChangedEventArgs : EventArgs
	// {
	// public PrjCoordSysChangedEventArgs(PrjCoordSys prjCoordSys, DataType
	// type)
	// {
	// prjCoordSys = prjCoordSys;
	// type = type;
	// }
	//
	// private PrjCoordSys prjCoordSys;
	// public PrjCoordSys PrjCoordSys
	// {
	// get
	// {
	// return prjCoordSys;
	// }
	// }
	//
	// private DataType type;
	// public DataType DataType
	// {
	// get
	// {
	// return type;
	// }
	// }
	// }
	// #endregion
	//
	// #region InterfaceMembers
	//
	// #endregion
	//
	// #region NestedTypes
	//
	// #endregion
	// }
	//
	//
	// }
	// //{{ [11/24/2011 huchenpu]
	// public enum DataType
	// {
	// PRJ_DATA = 0,
	// PRJ_MAP = 1,
	// };
}
