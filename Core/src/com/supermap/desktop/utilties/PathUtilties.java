package com.supermap.desktop.utilties;

import com.supermap.desktop.Application;

public class PathUtilties {
	/**
	 * 获取指定相对路径的绝对路径
	 * 
	 * @param pathName
	 * @return
	 */
	public static String getRootPathName() {
		String rootPath = Application.getActiveApplication().getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		rootPath = getParentPath(rootPath);
		rootPath += "Bin/";
		return rootPath;
	}

	/**
	 * 获取指定相对路径的绝对路径
	 * 
	 * @param pathName
	 * @return
	 */
	public static String getFullPathName(String pathName, boolean isFolder) {

		String result = getRootPathName();
		try {
			String[] pathPrams = new String[] { result, pathName };
			result = combinePath(pathPrams, isFolder);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}

	/**
	 * 获取指定路径的上级路径
	 * 
	 * @param pathName
	 * @return
	 */
	public static String getParentPath(String pathName) {

		String result = "";
		try {
			if (pathName != "") {
				pathName = pathName.replace("\\", "/");
				String[] splits = pathName.split("/");

				for (int i = 0; i < splits.length - 1; i++) {
					result += splits[i];
					result += "/";
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}

	/**
	 * 合并多个路径字符串
	 * 
	 * @param paths
	 *            路径数组
	 * @param isFolder
	 *            是否是文件夹路径
	 * @return 合并后的路径
	 */
	public static String combinePath(String[] paths, boolean isFolder) {
		String result = "";
		try {
			if (paths.length > 0) {
				result = paths[0];
			}

			if (result.endsWith("/") || result.endsWith("\\")) {
				// result = result.substring(0, result.length() - 1);
			} else {
				result += "/";
			}

			for (int i = 1; i < paths.length; i++) {
				if (paths[i] != null && paths[i] != "") {
					if (paths[i].startsWith("/") || paths[i].startsWith("\\")) {
						paths[i] = paths[i].substring(1, paths[i].length());
					}

					else if (paths[i].startsWith("../") || paths[i].startsWith("..\\")) {
						result = getParentPath(result);
						paths[i] = paths[i].substring(3, paths[i].length());
					}
					result += paths[i];

					if (result.endsWith("/") || result.endsWith("\\")) {
						// result = result.substring(0, result.length() -
						// 1);
					} else {
						result += "/";
					}
				}
			}

			if (!isFolder) {
				if (result.endsWith("/") || result.endsWith("\\")) {
					result = result.substring(0, result.length() - 1);
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}
	// /// <summary>
	// /// 获取指定相对路径相对于指定根目录的绝对路径
	// /// </summary>
	// public static String getFullPathName(String rootPath, String
	// pathName)
	// {
	// String newPathName = pathName;
	// try {
	// if (pathName != "")
	// {
	//
	// String oldCurrentDirectory = System .Environment.CurrentDirectory;
	// System.Environment.CurrentDirectory = rootPath;
	// newPathName = Path.getFullPath(pathName);
	//
	// //为了友好起见，恢复一下以前的当前路径
	// System.Environment.CurrentDirectory = oldCurrentDirectory;
	// }
	// } catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return newPathName;
	// }
	//
	// /// <summary>
	// /// 获取指定绝对路径相对AppDomain.CurrentDomain.BaseDirectory的相对路径
	// /// </summary>
	// public static String getRelativePath(String pathName)
	// {
	// return getRelativePath(AppDomain.CurrentDomain.BaseDirectory,
	// pathName);
	// }
	//
	// /// <summary>
	// /// 获取指定绝对路径相对指定根目录的相对路径
	// /// </summary>
	// public static string getRelativePath(String rootPath, String
	// pathName)
	// {
	// String result = pathName;
	//
	// try
	// {
	// Char[] chars = new char[] { Path.DirectorySeparatorChar,
	// Path.AltDirectorySeparatorChar };
	// String[] baseDirectory = rootPath.Split(chars);
	// String[] pathStrings = pathName.Split(chars);
	// int count = Math.Min(baseDirectory.Length, pathStrings.Length);
	// int index = -1;
	// for (int i = 0; i < count; i++)
	// {
	// if (!baseDirectory.get(i).Equals(pathStrings.get(i)))
	// {
	// break;
	// }
	// index = i;
	// }
	// if (index > -1)
	// {
	// String before = "";
	// for (int i = index + 1; i < baseDirectory.Length; i++)
	// {
	// if (!baseDirectory.get(i).Equals(""))
	// {
	// String temp = ".." + Path.DirectorySeparatorChar;
	// before += temp;
	// }
	// }
	// String after = "";
	// if (index + 1 < pathStrings.Length)
	// {
	// after = pathStrings[index + 1];
	// for (int i = index + 2; i < pathStrings.Length; i++)
	// {
	// after = after + Path.DirectorySeparatorChar + pathStrings.get(i);
	// }
	// }
	// result = before + after;
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return result;
	// }

	// /// <summary>
	// /// 获取某个目录下可用的文件名
	// /// </summary>
	// /// <param name="root">目录</param>
	// /// <param name="fileName">文件名称前缀</param>
	// /// <param name="postfix">文件的扩展名</param>
	// /// <param name="index">文件名的搜寻开始序号</param>
	// /// <returns></returns>
	// public static String getAvailableFileName(String root, String
	// fileName, String postfix, int index)
	// {
	// String name = "";
	// try
	// {
	// String fullName = "";
	// if (index == 0)
	// {
	// fullName = fileName + postfix;
	// }
	// else
	// {
	// fullName = fileName + index + postfix;
	// }
	// String path = root + "//" + fullName;
	// if (File.Exists(path))
	// {
	// index++;
	// name = getAvailableFileName(root, fileName, postfix, index);
	// }
	// else
	// {
	// name = Path.getFileNameWithoutExtension(path);
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return name;
	// }
	//
	// /// <summary>
	// /// 获取指定类型的几何风格模板文件路径
	// /// </summary>
	// public static String getTemplateFilePathRoot(GeometryType
	// geometryType)
	// {
	// String filePathReturn = "";
	//
	// String rootPath =
	// CommonToolkit.PathWrap.getFullPathName(@"..\Templates\Style\");
	// try
	// {
	// switch (geometryType)
	// {
	// case GeometryType.GeoLine:
	// {
	// filePathReturn = rootPath + "Line";
	// }
	// break;
	// case GeometryType.GeoPoint:
	// {
	// filePathReturn = rootPath + "Marker";
	// }
	// break;
	// case GeometryType.GeoRegion:
	// {
	// filePathReturn = rootPath + "Fill";
	// }
	// break;
	// case GeometryType.GeoText:
	// {
	// filePathReturn =
	// CommonToolkit.PathWrap.getFullPathName(@"..\Templates\TextStyle\CommonStyle");
	// }
	// break;
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// return filePathReturn;
	// }
	//
	// /// <summary>
	// /// 比较两个路径是否相等，比较相对路径之前得
	// /// 先调用getFullPath()转为绝对路径
	// /// </summary>
	// /// <param name="path1"></param>
	// /// <param name="path2"></param>
	// /// <returns></returns>
	// public static boolean IsPathEquals(String path1, String path2)
	// {
	// boolean result = false;
	// try
	// {
	// path1 = path1.Replace(Path.AltDirectorySeparatorChar,
	// Path.DirectorySeparatorChar);
	// if (path1.EndsWith(Path.DirectorySeparatorChar.ToString()))
	// path1 = path1.Remove(path1.Length - 1);
	// path2 = path2.Replace(Path.AltDirectorySeparatorChar,
	// Path.DirectorySeparatorChar);
	// if (path2.EndsWith(Path.DirectorySeparatorChar.ToString()))
	// path2 = path2.Remove(path2.Length - 1);
	// result = (path1.Equals(path2, StringComparison.OrdinalIgnoreCase));
	// }
	// catch
	// {
	// }
	// return result;
	// }
}
