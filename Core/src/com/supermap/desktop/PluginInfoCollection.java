package com.supermap.desktop;

import java.awt.List;
import java.util.ArrayList;

import com.supermap.desktop.enums.XMLCommandType;
import com.supermap.desktop.ui.*;

public class PluginInfoCollection extends ArrayList<PluginInfo> {
	public PluginInfoCollection() {
		// this.pluginInfos = new ArrayList<PluginInfo>();
	}

	private Boolean isDesigning;

	public Boolean getIsDesigning() {
		return this.isDesigning;
	}

	public void setIsDesigning(Boolean isDesigning) {
		this.isDesigning = isDesigning;
	}

	public Boolean getIsLoadded() {
		return this.xmlFrameMenus != null && this.xmlContextMenus != null
				&& this.xmlStatusbars != null && this.xmlToolbars != null
				&& this.xmlDockbars != null;
	}

	// private ArrayList<PluginInfo> this.pluginInfos;
	// public PluginInfo[] getPluginInfos() {
	// PluginInfo[] pluginInfos = new PluginInfo[this.pluginInfos.size()];
	// for (int i = 0; i < this.pluginInfos.size(); i++) {
	// pluginInfos[i] = this.pluginInfos.get(i);
	// }
	// return (PluginInfo[])(this.pluginInfos.toArray());
	// }

	// public int getCount() {
	// return this.pluginInfos.size();
	// return this.size();
	// }

	public Boolean isReadOnly() {
		return false;
	}

	private XMLMenus xmlFrameMenus = null;

	public XMLMenus getFrameMenus() {
		return this.xmlFrameMenus;
	}

	public void setFrameMenus(XMLMenus xmlFrameMenus) {
		this.xmlFrameMenus = xmlFrameMenus;
	}

	private XMLToolbars xmlToolbars = null;

	public XMLToolbars getToolbars() {
		return this.xmlToolbars;
	}

	public void setXMLToolbars(XMLToolbars xmlToolBars) {
		this.xmlToolbars = xmlToolBars;
	}

	public XMLStatusbars xmlStatusbars = null;

	public XMLStatusbars getStatusbars() {
		return this.xmlStatusbars;
	}

	public void setXMLStatusbars(XMLStatusbars xmlStatusbars) {
		this.xmlStatusbars = xmlStatusbars;
	}

	public XMLDockbars xmlDockbars = null;

	public XMLDockbars getDockbars() {
		return this.xmlDockbars;
	}

	public void setDockBars(XMLDockbars xmlDockbars) {
		this.xmlDockbars = xmlDockbars;
	}

	private XMLMenus xmlContextMenus = null;

	public XMLMenus getContextMenus() {
		return this.xmlContextMenus;
	}

	public void setXMLContextMenus(XMLMenus xmlContextMenus) {
		this.xmlContextMenus = xmlContextMenus;
	}

	// @Override
	// public boolean add(PluginInfo item)
	// {
	// boolean result = this.pluginInfos.add(item);
	// return result;
	// }
	//
	// public void clear()
	// {
	// this.pluginInfos.clear();;
	// }
	//
	// public Boolean contains(PluginInfo item)
	// {
	// return this.pluginInfos.contains(item);
	// }
	//
	// public void copyTo(PluginInfo[] array, int arrayIndex)
	// {
	//
	// }
	//
	// public Boolean remove(PluginInfo item)
	// {
	// return this.pluginInfos.remove(item);
	// }

	public void mergeUIElements() {
		this.mergeFrameMenus();
		this.mergeContextMenus();
		this.mergeStatusbars();
		this.mergeToolBars();
		this.mergeDockBars();
	}

	public Boolean mergeFrameMenus() {
		this.xmlFrameMenus = new XMLMenus(XMLCommandType.FRAMEMENUS);

		for (int i = 0; i < this.size(); i++) {
			PluginInfo info = this.get(i);
			this.xmlFrameMenus.merge(info.getFrameMenus());
		}

		return true;
	}

	private Boolean mergeContextMenus() {
		this.xmlContextMenus = new XMLMenus(XMLCommandType.CONTEXTMENUS);

		for (int i = 0; i < this.size(); i++) {

			PluginInfo info = this.get(i);
			this.xmlContextMenus.merge(info.getContextMenus());
		}

		return true;
	}

	private Boolean mergeStatusbars() {
		this.xmlStatusbars = new XMLStatusbars();

		for (int i = 0; i < this.size(); i++) {
			PluginInfo info = this.get(i);
			this.xmlStatusbars.merge(info.getStatusbars());
		}
		return true;
	}

	private Boolean mergeToolBars() {
		this.xmlToolbars = new XMLToolbars();

		for (int i = 0; i < this.size(); i++) {

			PluginInfo info = this.get(i);
			this.xmlToolbars.merge(info.getToolbars());
		}

		return true;
	}

	private Boolean mergeDockBars() {
		this.xmlDockbars = new XMLDockbars();

		for (int i = 0; i < this.size(); i++) {
			PluginInfo pluginInfo = this.get(i);
			if (pluginInfo.getDockbars() != null) {
				this.xmlDockbars.merge(pluginInfo.getDockbars());
			}
		}

		this.xmlDockbars.sort();
		return true;
	}
	//
	// public int indexOf(PluginInfo item)
	// {
	// return this.pluginInfos.indexOf(item);
	// }
	//
	// public void insert(int index, PluginInfo item)
	// {
	// this.pluginInfos.add(index, item);
	// }
	//
	// public void removeAt(int index)
	// {
	// this.pluginInfos.remove(index);
	// }
	//
	// public PluginInfo set(int index) {
	// return this.pluginInfos.get(index);
	// }
	//
	// public void get(int index, PluginInfo info) {
	// this.pluginInfos.set(index, info);
	// }
}
