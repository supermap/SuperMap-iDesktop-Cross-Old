package com.supermap.desktop.ui;

import org.w3c.dom.Element;

import com.supermap.desktop.Application;
import com.supermap.desktop.PluginInfo;
import com.supermap.desktop.enums.XMLCommandType;

public class XMLMenuButton extends XMLMenuCommand {

	public XMLMenuButton(PluginInfo pluginInfo, XMLMenuGroup group) {
		super(pluginInfo, group);
		this.commandType = XMLCommandType.MENUBUTTON;
	}

	// public XMLMenuButton(XMLMenuGroup group) {
	// super(group);
	// this.commandType = XMLCommandType.MENUBUTTON;
	// }

	@Override
	public boolean getIsContainer() {
		return false;
	}

	boolean isSelected = false;

	public boolean getSelected() {
		return this.isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	@Override
	public boolean initialize(Element xmlNodeCommand) {
		super.initialize(xmlNodeCommand);

		try {
			if (xmlNodeCommand.getAttribute(g_AttributionCheckState).equalsIgnoreCase(g_ValueTrue)) {
				this.setSelected(true);
			} else {
				this.setSelected(false);
			}
		} catch (Exception ex) {
		}

		return true;
	}

	@Override
	public XMLCommandBase clone(XMLCommandBase parent) {
		XMLMenuButton result = null;
		try {
			result = (XMLMenuButton) super.clone(parent);
			result.setSelected(this.getSelected());
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	@Override
	public XMLCommandBase saveToPluginInfo(PluginInfo pluginInfo, XMLCommandBase parent) {
		XMLMenuButton result = null;
		try {
			result = (XMLMenuButton) super.saveToPluginInfo(pluginInfo, parent);
			if (result != null) {
				result.setSelected(this.getSelected());
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	// public override XmlElement ToXML(XmlDocument document)
	// {
	// XmlElement contextMenuButtonNode = null;
	// try
	// {
	// if (document != null)
	// {
	// contextMenuButtonNode = document.CreateElement(_XMLTag.g_ControlButton);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionIndex,
	// this.Index.ToString());
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionLabel,
	// this.Label);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionVisible,
	// this.Visible.ToString().ToLower());
	// Boolean isChecked = (this.CheckState ==
	// System.Windows.Forms.CheckState.Checked);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionCheckState,
	// isChecked.ToString().ToLower());
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionAssemblyName,
	// this.AssemblyFileName);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_OnAction,
	// this.CtrlActionClass);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionImage,
	// this.ImageFile);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionShortcutKey,
	// this.ShortCutKeys);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionScreenTip,
	// this.Tooltip);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionScreenTipImage,
	// this.TooltipImageFile);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionHelpURL,
	// this.HelpURL);
	// contextMenuButtonNode.SetAttribute(_XMLTag.g_AttributionCustomProperty,
	// this.CustomProperty);
	// SetAttributeCodeType(contextMenuButtonNode);
	// XmlCDataSection dynamicCodeNode = CreateDynamicCodeNode(document);
	// if (dynamicCodeNode != null)
	// {
	// contextMenuButtonNode.AppendChild(dynamicCodeNode);
	// }
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	//
	// return contextMenuButtonNode;
	// }
}
