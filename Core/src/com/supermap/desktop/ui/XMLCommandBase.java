package com.supermap.desktop.ui;

import java.lang.reflect.Type;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.supermap.desktop.Interface.IXMLCreator;
import com.supermap.desktop.enums.XMLCommandType;
import com.supermap.desktop.Application;
import com.supermap.desktop.Plugin;
import com.supermap.desktop.PluginInfo;
import com.supermap.desktop._XMLTag;

public abstract class XMLCommandBase extends _XMLTag {

	private PluginInfo pluginInfo = null;
	private boolean isDesigning = false;
	private int index = 0;
	private String id = "";
	private boolean visible = true;
	private String label = "";
	private String customProperty = "";

	protected XMLCommandType commandType = XMLCommandType.BUTTON;

	public XMLCommandBase() {
	}

	public XMLCommandBase(PluginInfo pluginInfo) {
		this.pluginInfo = pluginInfo;
	}

	public XMLCommandBase(XMLCommandBase parent) {
		this.parent = parent;
	}

	public XMLCommandBase(PluginInfo pluginInfo, XMLCommandBase parent) {
		this.pluginInfo = pluginInfo;
		this.parent = parent;
	}

	public boolean initialize(Element element) {
		// id
		try {
			this.setID(element.getAttribute(g_AttributionID));
		} catch (Exception ex) {
		}

		// label
		try {
			this.label = element.getAttribute(g_AttributionLabel);
		} catch (Exception ex) {
		}
		
		// index
		try {
			String attribute = element.getAttribute(g_AttributionIndex);
			this.setIndex(Integer.valueOf(attribute));
		} catch (Exception ex) {
		}

		// visible
		try {
			String attribute = element.getAttribute(g_AttributionVisible);
			this.visible = !attribute.equalsIgnoreCase(g_ValueFalse);
		} catch (Exception ex) {
		}

		// CustomProperty
		try {
			this.customProperty = element
					.getAttribute(g_AttributionCustomProperty);
		} catch (Exception ex) {
		}

		return true;
	}

	public PluginInfo getPluginInfo() {
		return this.pluginInfo;
	}

	public void setPluginInfo(PluginInfo pluginInfo) {
		this.pluginInfo = pluginInfo;
	}

	public boolean getIsDesigning() {
		return this.isDesigning;
	}

	public void setIsDesigning(boolean isDesigning) {
		this.isDesigning = isDesigning;
	}

	public int getIndex() {
		return this.index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getID() {
		return this.id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public boolean getVisible() {
		return this.visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean getIsContainer() {
		return true;
	}

	public String getCustomProperty() {
		return this.customProperty;
	}

	public void setCustomProperty(String customProperty) {
		this.customProperty = customProperty;
	}

	protected XMLCommandBase parent = null;

	public XMLCommandBase getParent() {
		return this.parent;
	}

	public XMLCommandType getCommandType() {
		return this.commandType;
	}

	public IXMLCreator getXMLCreator() {
		return null;
	}

	public void remove() {

		try {
			// if (DoRemove())
			// {
			// if (this.parent != null)
			// {
			// this.parent.RemovePluginInfos(this.pluginInfoList);
			// this.parent = null;
			// }
			// }
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	public XMLCommandBase createElement(XMLCommandType commandType) {
		return null;
	}

	/**
	 * 拷贝一份自己，包括自己的子节点也执行拷贝, 并添加为指定元素的子项
	 * 
	 * @param parent
	 *            指定父节点，拷贝之后可能会改变父节点
	 * @return
	 */
	public XMLCommandBase copyTo(XMLCommandBase parent) {
		XMLCommandBase result = null;

		try {
			result = clone(parent);
			if (result != null) {
				parent.addSubItem(result);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	/**
	 * 改变自己的父节点, 插入到指定父元素的指定子项之后, 由于XMLCommand集合的Index、XMLCommand本身的Index不同,
	 * 而XMLCommand本身的Index还可以相同，因此不使用Index来作为,
	 * 插入位置，插入过程需要自动对集合中的XMLCommand本身的Index做处理
	 * 
	 * @param preCommand
	 * @param subCommand
	 * @return
	 */
	public XMLCommandBase Insert(XMLCommandBase preCommand,
			XMLCommandBase subCommand) {
		XMLCommandBase result = null;
		try {
			// if (subCommand != null) {
			// // 首先将自己从原父节点移除
			// // 这一步在方法调用之前处理
			// // 然后改变父节点为当前元素
			// subCommand.changeParent(this);
			// // 最后插入到当前元素对应的位置
			// this.insertSubItem(preCommand, subCommand);
			// }
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	/**
	 * 查找承载此元素的根容器
	 * 
	 * @return
	 */
	public XMLCommandBase findBaseContainer() {
		XMLCommandBase result = null;
		try {
			// if (this.Parent is XMLRibbonTab
			// || this.Parent is XMLStartMenu
			// || this.Parent is XMLStatusBar
			// || this.Parent is XMLContextMenus)
			// {
			// result = this.Parent;
			// }
			// else if (this.Parent != null)
			// {
			// result = this.Parent.FindBaseContainer();
			// }
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}

	// public XMLRibbonTab findRibbonTab() {
	// XMLRibbonTab tab = null;
	// try {
	// if (this.Parent is XMLRibbonTab)
	// {
	// tab = this.Parent as XMLRibbonTab;
	// }
	// else if (this.Parent != null)
	// {
	// tab = this.Parent.FindRibbonTab();
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	//
	// return tab;
	// }
	//
	// public XMLStatusBar findStatusBar() {
	// XMLStatusBar statusBar = null;
	// try {
	// if (this.Parent is XMLStatusBar)
	// {
	// statusBar = this.Parent as XMLStatusBar;
	// }
	// else if (this.Parent != null)
	// {
	// statusBar = this.Parent.FindStatusBar();
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	//
	// return statusBar;
	// }

	public XMLCommandBase clone(XMLCommandBase parent) {
		return null;
	}

	public void insertSubItem(XMLCommandBase preItem, XMLCommandBase insertItem) {

	}

	/**
	 * 添加子项, 内部仅根据Command的IIndex加顺序进行处理, 不处理PluginInfoList
	 * 
	 * @param subItem
	 */
	public void addSubItem(XMLCommandBase subItem) {

	}

	/**
	 * 将自己从所父节点中移除
	 * 
	 * @return
	 */
	protected boolean doRemove() {
		return false;
	}

	// / <summary>
	// / 获取指定PluginInfo的内容
	// / 如果不包含指定PluginInfo的内容，则返回null
	// / </summary>
	// / <param name="pluginInfo">指定的PluginInfo</param>
	// / <param name="parent">指定的父节点</param>
	// / <returns></returns>
	public XMLCommandBase saveToPluginInfo(PluginInfo pluginInfo,
			XMLCommandBase parent) {
		return null;
	}

	/**
	 * 清理资源（图片等）,在进行拖拽操作的时候，比如拖拽一个Tab, 实现过程中，XMLRibbonTab并没有拷贝,
	 * 而是更改了父节点，插入到了新的父节点子项中, 于是拖拽之后XMLRibbonTab不再使用了, 但是由于有些独占的资源（比如图片等）已经构造,
	 * 于是在拖拽操作之后，保存修改再次打开界面设计器, 会有各种奇怪的现象，比如无法调试跟踪的异常, 所以需要在一些特殊情况下，手动清理独占资源
	 */
	public void clearResources() {

	}

	public Element toXML(Document document) {
		return null;
	}

	public String getEnabledKey(String key) {
		String enabledKey = "";
		try {
			if (this.getXMLCreator() != null
					&& this.getXMLCreator().getDefaultValueCreator() != null) {
				enabledKey = this.getXMLCreator().getDefaultValueCreator()
						.getDefaultID(key);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return enabledKey;
	}

	/**
	 * 为RibbonManager服务, 现在RibbonTab在使用到的时候才加载, 因此在使用RibbonManager查找Item的时候,
	 * 需要使用这个方法先判断对应的Tab下是否, 存在指定Type CtrlAction的XMLCommand, 有则Build一下并继续后面的操作,
	 * 后续可以有计划的从结构上优化RibbonManger的内容。
	 * 
	 * @param ctrlActionType
	 * @return
	 */
	public boolean containsAction(Type ctrlActionType) {
		return false;
	}

	public <T extends XMLCommand> void insertCommand(T command,
			ArrayList<T> commands) {
		if (command != null && commands != null) {
			int insertPos = commands.size();
			for (int i = commands.size() - 1; i >= 0; i--) {
				if (commands.get(i).getIndex() > command.getIndex()) {
					insertPos--;
				} else {
					break;
				}
			}

			if (insertPos < 0 || insertPos == commands.size()) {
				commands.add(command);
			} else {
				commands.add(insertPos, command);
			}
		}
	}
}
