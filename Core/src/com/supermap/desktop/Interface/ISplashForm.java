package com.supermap.desktop.Interface;

/**
* 定义窗体所具有的基本功能的接口
*/
public interface ISplashForm {
	
	/** classVar1 documentation comment */
	
	/**
	* Set the counters
	*/
	
	/**
	* Get the counters
	*/
	
	/**
	* 显示启动界面。
	*/
	void Show();

	/**
	* 隐藏启动界面。
	*/
    void Hide();

    /**
	* 关闭启动界面。
	*/
    void Close();
    
    
//    /**
//	* 获取启动界面的背景图片。
//	*/
//	public Bitmap getBackgroundImage();
//
//	/**
//	* 设置启动界面的背景图片。
//	*/
//	public void setBackgroundImage(Bitmap backgroundImage);
//    
//    /// <summary>
//    /// 获取启动界面所使用的所有控件的集合。 
//    /// </summary>
//    Control.ControlCollection Controls
//    {
//        get;
//    }

}