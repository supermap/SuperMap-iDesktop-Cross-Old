package com.supermap.desktop.Interface;

import com.supermap.desktop.event.ActiveFormChangedListener;

public interface IFormManager {

	/**
	 * 获取指定索引的子窗体。
	 */
	IForm get(int index);

	void add(IForm form);

	void add(int index, IForm form);

	// IForm this[Int32 index]

	/**
	 * 获取应用程序内的子窗体的总数。
	 */
	int getCount();

	/**
	 * 获取或者设置当前被激活的子窗体。
	 */
	IForm getActiveForm();

	void setActiveForm(IForm form);

	void showChildForm(IForm childForm);

	void addActiveFormChangedListener(ActiveFormChangedListener listener);

	void removeActiveFormChangedListener(ActiveFormChangedListener listener);

	// /// <summary>
	// /// 当创建窗体时将触发该事件。
	// /// </summary>
	// event FormCreatedEventHandler FormCreated;

	/**
	 * 关闭指定的子窗体。
	 * 
	 * @param form
	 *            指定的要保存的子窗口。
	 * @returns 关闭成功返回 true；否则返回 false
	 */
	boolean close(IForm form);

	/**
	 * 关闭所有的子窗体。
	 * 
	 * @returns 关闭成功返回 true；否则返回 false
	 */
	boolean closeAll();

	/**
	 * 关闭所有的子窗体。
	 * 
	 * @param isSave
	 *            是否保存窗口内容。
	 * @returns 关闭成功返回 true；否则返回 false
	 */
	boolean closeAll(boolean isSave);

	/**
	 * 保存所有的子窗体内容。
	 * 
	 * @param notify
	 *            是否弹出提示对话框，true 表示弹出对话框进行提示，否则不会提示。
	 * @returns 保存成功返回 true；否则返回 false
	 */
	boolean saveAll(boolean notify);

	// /// <summary>
	// /// 根据指定的窗口集合进行关联浏览。
	// /// </summary>
	// /// <param name="forms">待关联的窗口集合，支持的类型包括：IFormMap，IFormScene和IFormTabular。</param>
	// /// <param name="autoLayoutWindows">是否自动进行窗口布局，true 表示自动进行布局，否则不会自动布局。</param>
	// void bindForms(IForm[] forms, Boolean autoLayoutWindows);
	//
	// /// <summary>
	// /// 指定一个应用程序中的子窗体的布局样式。
	// /// </summary>
	// /// <param name="mdiLayout">指定的布局样式对象，即指定应用程序主窗口中的所有子窗体的布局。</param>
	// void layoutMdi(MdiLayout mdiLayout);
}
