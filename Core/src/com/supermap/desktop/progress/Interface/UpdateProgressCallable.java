package com.supermap.desktop.progress.Interface;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;

/**
 * 提供进度更新信息的 Callable
 * 
 * @author highsad
 *
 */
public abstract class UpdateProgressCallable implements Callable<Boolean> {

	private IUpdateProgress update;

	public IUpdateProgress getUpdate() {
		return update;
	}

	public void setUpdate(IUpdateProgress update) {
		this.update = update;
	}

	public void updateProgress(int percent, String remainTime, String message) throws CancellationException {
		this.update.updateProgress(percent, remainTime, message);
	}

	public void updateProgressTotal(int percent, int totalPercent, String remainTime, String message) throws CancellationException {
		this.update.updateProgress(percent, totalPercent, remainTime, message);
	}
}
