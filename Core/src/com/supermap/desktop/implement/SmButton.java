package com.supermap.desktop.implement;

import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.ICtrlAction;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.ui.XMLCommand;
import com.supermap.desktop.utilties.PathUtilties;

public class SmButton extends JButton implements IBaseItem {

	// private JComponent this.parent = null;
	private IForm formClass = null;
	private XMLCommand xmlCommand = null;

	public SmButton(IForm formClass, XMLCommand xmlCommand, JComponent parent) {
		// super(xmlCommand.getLabel());
		super.setToolTipText(xmlCommand.getTooltip());
		String[] pathPrams = new String[] { PathUtilties.getRootPathName(), xmlCommand.getImageFile() };
		String path = PathUtilties.combinePath(pathPrams, false);
		File file = new File(path);
		if (file.exists() && file.isFile()) {
			this.setIcon(new ImageIcon(path));
		} else {
			this.setText(xmlCommand.getLabel());
		}

		this.formClass = formClass;
		// this.parent = parent;
		this.xmlCommand = xmlCommand;

		try {
			// 这里临时做重复判断，以后再统一优化
			// 同时建议，框架菜单、弹出菜单和工具条的CtrlAction统一管理
			ICtrlAction ctrlAction = Application.getActiveApplication().getCtrlAction(
					xmlCommand.getPluginInfo().getBundleName(),  xmlCommand.getCtrlActionClass());
			if (ctrlAction == null) {
				ctrlAction = CommonToolkit.CtrlActionWrap.getCtrlAction(xmlCommand, this, this.formClass);
			}

			if (ctrlAction != null) {
				ctrlAction.setFormClass(this.formClass);
				setCtrlAction(ctrlAction);
				Application.getActiveApplication().setCtrlAction(
						xmlCommand.getPluginInfo().getBundleName(), xmlCommand.getCtrlActionClass(), ctrlAction);

				this.addPropertyChangeListener(new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						Button_PropertyChange(evt);
					}
				});
			} else {
				this.setToolTipText(this.getToolTipText() + CommonProperties.getString("String_UnDo"));
			}

			this.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					item_ActionPerformed(evt);
				}
			});
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}
	
	private void item_ActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			if (this.getCtrlAction() != null) {
				this.getCtrlAction().setCaller(this);
				this.getCtrlAction().run();
			} else {
				Application.getActiveApplication().getOutput().output("CtrlAction Unimplemented!");
				JOptionPane.showMessageDialog(null, this.xmlCommand.getCtrlActionClass() + " Unimplemented!");
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	public void Button_PropertyChange(PropertyChangeEvent evt) {
		try {
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

//	@Override
//	protected void paintComponent(Graphics g) {
//		try {
//			super.paintComponent(g);
//			if (this.getCtrlAction() != null) {
//				boolean rePaint = false;
//				boolean enable = this.getCtrlAction().enable();
//				if (this.isEnabled() != enable) {
//					this.setEnabled(enable);
//					rePaint = true;
//				}
//
//				boolean check = this.getCtrlAction().check();
//				if (this.isSelected() != check) {
//					this.setSelected(check);
//					rePaint = true;
//				}
//				
//				if (rePaint) {
//					this.updateUI();
//				}
//			}
//		} catch (Exception ex) {
//
//		}
//	}

	@Override
	public boolean isEnabled() {
		return super.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}

	@Override
	public boolean isChecked() {
		return super.isSelected();
	}

	@Override
	public void setChecked(boolean checked) {
		super.setSelected(checked);
	}

	@Override
	public boolean isVisible() {
		return super.isVisible();
	}

	@Override
	public void setVisible(boolean enabled) {
		super.setVisible(enabled);
	}

	@Override
	public int getIndex() {
		return this.xmlCommand.getIndex();
	}

	@Override
	public void setIndex(int index) {
		this.xmlCommand.setIndex(index);
	}

	@Override
	public String getID() {
		return this.xmlCommand.getID();
	}

	@Override
	public String getText() {
		return super.getText();
	}

	@Override
	public void setText(String text) {
		super.setText(text);
	}

	@Override
	public ICtrlAction getCtrlAction() {
		return this.xmlCommand.getCtrlAction();
	}

	@Override
	public void setCtrlAction(ICtrlAction ctrlAction) {
		this.xmlCommand.setCtrlAction(ctrlAction);
	}
}
