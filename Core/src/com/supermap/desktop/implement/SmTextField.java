package com.supermap.desktop.implement;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.ICtrlAction;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.ui.XMLCommand;

public class SmTextField extends JTextField implements IBaseItem {

	// private JComponent this.parent = null;
	private IForm formClass = null;
	private XMLCommand xmlCommand = null;

	public SmTextField(IForm formClass, XMLCommand xmlCommand, JComponent parent) {
		super(xmlCommand.getLabel());
		super.setToolTipText(xmlCommand.getTooltip());

		this.formClass = formClass;
		// this.parent = parent;
		this.xmlCommand = xmlCommand;

		this.addFocusListener(new java.awt.event.FocusAdapter() {

			public void focusLost(FocusEvent e) {
				runCtrlAction();
			}

			public void focusGained(FocusEvent arg0) {
				runCtrlAction();
			}
		});

		this.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent evt) {
				runCtrlAction();
			}
		});

		try {
			// 这里临时做重复判断，以后再统一优化
			// 同时建议，框架菜单、弹出菜单和工具条的CtrlAction统一管理
			ICtrlAction ctrlAction = Application.getActiveApplication().getCtrlAction(xmlCommand.getPluginInfo().getBundleName(), 
					xmlCommand.getCtrlActionClass());
			if (ctrlAction == null) {
				ctrlAction = CommonToolkit.CtrlActionWrap.getCtrlAction(xmlCommand, this, this.formClass);
			}

			if (ctrlAction != null) {
				ctrlAction.setFormClass(this.formClass);
				setCtrlAction(ctrlAction);
				Application.getActiveApplication().setCtrlAction(
						xmlCommand.getPluginInfo().getBundleName(), xmlCommand.getCtrlActionClass(), ctrlAction);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}
	
	public void runCtrlAction() {
		try {
			if (this.getCtrlAction() != null) {
				this.getCtrlAction().setCaller(this);
				this.getCtrlAction().run();
			} else {
				// add by huchenpu 20150827
				// 地图的状态栏是自己处理的消息，这里就暂时不处理了
//				Application.getActiveApplication().getOutput().output("CtrlAction Unimplemented!");
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		try {
			super.paintComponent(g);
			if (this.getCtrlAction() != null) {
				boolean enable = this.getCtrlAction().enable();
				this.setEnabled(enable);
				this.updateUI();
			}
		} catch (Exception ex) {

		}
	}

	@Override
	public boolean isEnabled() {
		return super.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}
	
	@Override
	public boolean isChecked() {
		return false;
	}

	@Override
	public void setChecked(boolean checked) {
	}

	@Override
	public boolean isVisible() {
		return super.isVisible();
	}

	@Override
	public void setVisible(boolean enabled) {
		super.setVisible(enabled);
	}

	@Override
	public int getIndex() {
		return this.xmlCommand.getIndex();
	}

	@Override
	public void setIndex(int index) {
		this.xmlCommand.setIndex(index);
	}

	@Override
	public String getID() {
		return this.xmlCommand.getID();
	}

	@Override
	public String getText() {
		return super.getText();
	}

	@Override
	public void setText(String text) {
		super.setText(text);
	}

	@Override
	public ICtrlAction getCtrlAction() {
		return this.xmlCommand.getCtrlAction();
	}

	@Override
	public void setCtrlAction(ICtrlAction ctrlAction) {
		this.xmlCommand.setCtrlAction(ctrlAction);
	}
}
