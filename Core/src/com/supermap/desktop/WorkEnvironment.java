package com.supermap.desktop;

import java.io.File;

import org.w3c.dom.Element;

import com.supermap.desktop.ui.XMLMenu;
import com.supermap.desktop.ui.XMLMenus;
import com.supermap.desktop.utilties.PathUtilties;
import com.supermap.desktop.utilties.XmlUtilties;

public class WorkEnvironment {

	public final static String g_FileConfigExt = "*.config";

	/**
	 * 获取或设置工作环境的名称。
	 */
	private String name = "";

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取工作环境管理器。
	 */
	private WorkEnvironmentManager manager = null;

	public WorkEnvironmentManager getWorkEnvironmentManager() {
		return this.manager;
	}

	/**
	 * 获取工作环境对象是否是当前程序使用的活动工作环境。
	 */
	private Boolean actived;

	public Boolean getActived() {
		return this.actived;
	}

	/**
	 * 当前工作场景是否被加载了
	 */
	private Boolean isLoadded = false;

	public Boolean getIsLoadded() {
		return this.isLoadded;
	}

	private PluginInfoCollection pluginInfos = null;

	public PluginInfoCollection getPluginInfos() {
		return this.pluginInfos;
	}

	// /**
	// * 获取或设置是否是设计状态。
	// */
	// public Boolean getIsDesigning() {
	// return this.pluginInfos.IsDesigning;
	// }
	//
	// public void setIsDesigning(Boolean isDesigning) {
	// this.pluginInfos.IsDesigning = isDesigning;
	// }

	public WorkEnvironment(String name) {
		this.name = name;
		this.pluginInfos = new PluginInfoCollection();
		this.isLoadded = false;
	}

	public WorkEnvironment(WorkEnvironment workEnvironment) {
		this.actived = workEnvironment.getActived();
		this.isLoadded = workEnvironment.getIsLoadded();
		this.name = workEnvironment.getName();
		this.manager = workEnvironment.getWorkEnvironmentManager();
		this.pluginInfos = new PluginInfoCollection();
		for (int i = 0; i < workEnvironment.getPluginInfos().size(); i++) {
			PluginInfo pluginInfo = workEnvironment.getPluginInfos().get(i);
			this.pluginInfos.add(pluginInfo);
		}
		this.pluginInfos.mergeUIElements();
	}

	public WorkEnvironment(WorkEnvironmentManager manager, String name) {
		this.manager = manager;
		if (!this.manager.isWorkEnvironmentNameValid(name)) {
			// throw new Exception("No Valid.");
		}

		this.pluginInfos = new PluginInfoCollection();

		this.name = name;
		this.isLoadded = false;
	}

	// public void initializeXMLCommands()
	// {
	// if (!this.pluginInfos.getIsLoadded())
	// {
	// initializePluginInfos();
	// }
	// }
	//
	// public void loadXMLCommands()
	// {
	// loadWorkEnvironment();
	// }

	/**
	 * 加载工作环境
	 */
	public Boolean load() {
		try {
			_XMLTag.g_strWorkEnvironment = getName();

			if (!this.pluginInfos.getIsLoadded()) {
				initializePluginInfos();
			}

			loadWorkEnvironment();

			this.isLoadded = true;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return this.pluginInfos.size() > 0;
	}

	public void initializePluginInfos() {
		this.pluginInfos.clear();
		// 查找所有默认工作场景下的插件配置文件
		String workEnvironmentPath = PathUtilties.getFullPathName(_XMLTag.g_FolderWorkEnvironment, true);
		String[] pathPrams = new String[] { workEnvironmentPath, getName() };
		workEnvironmentPath = PathUtilties.combinePath(pathPrams, true);

		File file = new File(workEnvironmentPath);
		File[] childFiles = file.listFiles();
		for (int j = 0; j < childFiles.length; j++) {
			// 读取配置文件字符串也用XML的接口去读，同时也要有我们的命名空间
			pathPrams = new String[] { workEnvironmentPath, childFiles[j].getName() };
			String configFile = PathUtilties.combinePath(pathPrams, false);
			Element pluginElement = XmlUtilties.getRootNode(configFile);

			if (pluginElement != null) {
				try {
					PluginInfo pluginInfo = new PluginInfo(pluginElement);
					if (pluginInfo.IsValid()) {
						pluginInfo.setConfigLocation(configFile);
						pluginInfo.parseUI();

						this.pluginInfos.add(pluginInfo);
					}
				} catch (Exception ex) {
					Application.getActiveApplication().getOutput().output(ex);
				}
			}
		}

		this.pluginInfos.mergeUIElements();
	}

	public void mergeUIElements() {
		this.pluginInfos.mergeUIElements();
	}

	/**
	 * 卸载当前工作环境中的插件
	 * 
	 * @param isSaveConfig
	 *            是否保存工作环境的配置文件
	 */
	public void unLoad(Boolean isSaveConfig) {
		this.actived = false;
		ProcessPlugin.unloadPlugins(isSaveConfig, true);
		this.isLoadded = false;
	}

	/**
	 * 工作环境类输出字符串，目前输出的是工作环境对象的名称。
	 */
	public String toString() {
		return this.getName();
	}

	public void toXML() {
		for (int i = 0; i < this.pluginInfos.size(); i++) {
			PluginInfo pluginInfo = this.pluginInfos.get(i);
			if (pluginInfo != null) {
				pluginInfo.toXML();
			}
		}
	}

	// / <summary>
	// / 以ApplicationRibbon举例说明一个问题
	// / 首先，Application内部维护了一个PluginInfo
	// / 的集合，这个集合是 PluginInfos 的子集
	// / 在工作环境设计的时候，可能会有删除操作
	// / 只要有删除操作就有可能导致ApplicationRibbon
	// / 内部维护的 PluginInfo 集合删除某些项
	// / （当 ApplicationRibbon 以及它的子元素都不再有
	// / 某个 PluginInfo 的内容的时候，就会删除这个PluginInfo）;
	// / 然后在做 ApplicationRibbon.Save 的时候，只会根据
	// / ApplicationRibbon 内部维护的PluginInfo 集合来进行分发
	// / 这就会导致，之前做删除操作的时候从这个集合内部删掉的
	// / PluginInfo的状态没有改变，而这个PluginInfo是存在于
	// / PluginInfos里的，就回导致下一次打开设计器做融合的时候
	// / 出现意料之外的内容，因此需要先做一次清理
	// / </summary>
	public void saveChange() {
		// for (Int32 i = 0; i < this.PluginInfos.Count; i++)
		// {
		// this.PluginInfos[i].RibbonDefine = null;
		// this.PluginInfos[i].ContextMenuDefines = null;
		// this.PluginInfos[i].DockBarDefines = null;
		// this.PluginInfos[i].StartMenuDefines = null;
		// this.PluginInfos[i].StatusBarDefines = null;
		// }
		// this.PluginInfos.ApplicationRibbon.Save();
		// this.PluginInfos.ApplicationContextMenus.Save();
		// this.PluginInfos.ApplicationDockBars.Save();
		// this.PluginInfos.ApplicationStartMenu.Save();
		// this.PluginInfos.ApplicationStatusBars.Save();
		// for (Int32 i = this.PluginInfos.Count - 1; i >= 0; i--)
		// {
		// if (this.PluginInfos[i].RibbonDefine == null
		// && this.PluginInfos[i].ContextMenuDefines == null
		// && this.PluginInfos[i].DockBarDefines == null
		// && this.PluginInfos[i].StartMenuDefines == null
		// && this.PluginInfos[i].StatusBarDefines == null)
		// {
		// this.PluginInfos.RemoveAt(i);
		// }
		// }
	}

	/**
	 * 根据当前的活动工作场景，重新装载界面
	 */
	private void loadWorkEnvironment() {
		try {
			// 如果在 Application.Initialize() 之前设置当前工作环境，会导致插件被加载两次，这里判断写再卸载掉
			if (Application.getActiveApplication().getPluginManager().getCount() > 0) {
				unLoad(false);
			}

			for (int i = 0; i < this.pluginInfos.size(); i++) {
				PluginInfo info = this.pluginInfos.get(i);
				if (info.getEnable()) {
					Plugin plugin = CreatePluginFromInfo(info);
					if (plugin != null) {
						Application.getActiveApplication().getPluginManager().load(plugin, -1);
						if (Application.getActiveApplication().getOutput() != null) {
							File file = new File(info.getName());
							Application.getActiveApplication().getOutput().output(file.getName());
						}
					}
				}
			}

			if (Application.getActiveApplication().getMainFrame() != null) {
				Application.getActiveApplication().getMainFrame().getFormManager().closeAll();
				// Application.ActiveApplication.WorkEnvironmentManager.SendReloadUIEvent();
				// (Application.ActiveApplication.MainForm as
				// System.Windows.Forms.Form).Refresh();
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private Plugin CreatePluginFromInfo(PluginInfo pluginInfo) {
		Plugin plugin = null;

		// try
		// {
		// String path =
		// CommonToolkit.PathWrap.GetFullPathName(pluginInfo.AssemblyName);
		//
		// System.Reflection.Assembly assembly = null;
		//
		// //查找一下是否已经加载进来了
		// Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
		// foreach (Assembly a in assemblys)
		// {
		// if (path.Length != 0 &&
		// a.FullName.ToLower().Equals(path.ToLower()))
		// {
		// assembly = a;
		// break;
		// }
		// }
		// if (assembly == null)
		// {
		// assembly = System.Reflection.Assembly.LoadFrom(path);
		// }
		//
		// plugin = assembly.CreateInstance(pluginInfo.ClassName,
		// false,
		// BindingFlags.Default,
		// null,
		// new object[] { pluginInfo },
		// CultureInfo.CurrentCulture,
		// null) as Plugin;
		// }
		// catch (Exception ex)
		// {
		// SuperMap.Desktop.Application.ActiveApplication.Output.Output(ex.StackTrace,
		// InfoType.Exception);
		// plugin = null;
		// }

		return plugin;
	}

	private Boolean copyFrom(WorkEnvironment data) {
		return false;
	}
}
