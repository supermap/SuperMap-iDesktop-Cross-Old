package com.supermap.desktop.CtrlAction.Dataset;

import java.text.MessageFormat;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import com.supermap.data.Dataset;
import com.supermap.data.Datasource;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.dataeditor.DataEditorProperties;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.ui.UICommonToolkit;

public class CtrlActionDeleteDataset extends CtrlAction {

	public CtrlActionDeleteDataset(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		try {
			boolean isDataset = false;
			Dataset[] datasets = Application.getActiveApplication().getActiveDatasets();
			if (datasets != null && datasets.length > 0) {
				isDataset = true;
			}

			if (isDataset) {
				if (Application.getActiveApplication().getActiveDatasources()[0].isReadOnly()) {
					UICommonToolkit.showConfirmDialog(DataEditorProperties.getString("String_DelectReadonlyDataset"));
					return;
				}
				String message = "";
				if (datasets.length == 1) {
					message = MessageFormat.format(DataEditorProperties.getString("String_DelectOneDataset"), Application.getActiveApplication()
							.getActiveDatasources()[0].getAlias(), datasets[0].getName());
				} else {
					message = MessageFormat.format(DataEditorProperties.getString("String_DelectManyDataset"), Application.getActiveApplication()
							.getActiveDatasources()[0].getAlias(), datasets.length);
				}
				if (JOptionPane.OK_OPTION == UICommonToolkit.showConfirmDialog(message)) {
					CommonToolkit.DatasetWrap.CloseDataset(datasets);
					for (int i = 0; i < datasets.length; i++) {
						Application.getActiveApplication().getActiveDatasources()[0].getDatasets().delete(datasets[i].getName());
					}
					Application.getActiveApplication().setActiveDatasets(null);
					String resultInfo = "";
					resultInfo = DataEditorProperties.getString("String_DelectDatasetSuccessful");
					Application.getActiveApplication().getOutput().output(resultInfo);
				}
			} else {
				Datasource datasource = Application.getActiveApplication().getActiveDatasources()[0];
				JFrame frame = (JFrame) Application.getActiveApplication().getMainFrame();
				DatasetChooserDataEditor dataSetChooser = new DatasetChooserDataEditor(frame, datasource, true);
				dataSetChooser.setVisible(true);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	@Override
	public boolean enable() {
		boolean enable = false;
		Datasource[] datasources = Application.getActiveApplication().getActiveDatasources();
		if (datasources != null && datasources.length > 0) {
			for (Datasource datasource : datasources) {
				if (!datasource.isReadOnly()) {
					enable = true;
					break;
				}
			}
		}
		return enable;
	}
}