package com.supermap.desktop.CtrlAction.Dataset;

/**
 * @author Administrator 复制和删除数据集界面
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.supermap.data.Datasource;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.dataeditor.DataEditorProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.properties.CoreProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.DataCell;
import com.supermap.desktop.ui.controls.DatasetChooser;
import com.supermap.desktop.ui.controls.mutiTable.component.MutiTable;
import com.supermap.desktop.ui.controls.mutiTable.component.MutiTableModel;

public class DatasetChooserDataEditor extends DatasetChooser {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean DIALOG_TYPE_COPY = false;
	private boolean DIALOG_TYPE_DELECT = false;
	private int COLUMN_INDEX_SOURCEDATASET = 0;
	private int COLUMN_INDEX_SOURCEDATASOURCE = 1;
	private int COLUMN_INDEX_TARGETDATASOURCE = 2;
	private int COLUMN_INDEX_TARGETDATASET = 3;
	private int COLUMN_INDEX_CODINGTYPE = 4;
	private int COLUMN_INDEX_CHARSET = 5;
	private MutiTable datasetCopyTable;

	public DatasetChooserDataEditor(JDialog owner, Datasource datasource, MutiTable datasetCopyTable, boolean DIALOG_TYPE_COPY) {
		super(owner, true, datasource);
		this.datasetCopyTable = datasetCopyTable;
		this.DIALOG_TYPE_COPY = DIALOG_TYPE_COPY;
		setTitle(CoreProperties.getString("String_FormDatasetBrowse_FormText"));
		getDataset();
	}

	public DatasetChooserDataEditor(JFrame owner, Datasource datasource, boolean DIALOG_TYPE_DELECT) {
		super(owner, true, datasource);
		this.DIALOG_TYPE_DELECT = DIALOG_TYPE_DELECT;
		setTitle(CoreProperties.getString("String_FormDatasetBrowse_FormText"));
		getDataset();
	}

	private void getDataset() {
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (DIALOG_TYPE_COPY) {
					// 添加数据集到复制数据集窗口
					addInfoToMainTable();
				}
				if (DIALOG_TYPE_DELECT) {
					// 删除数据集
					DeleteThread thread = new DeleteThread();
					thread.start();
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (2 == e.getClickCount() && DIALOG_TYPE_COPY) {
					addInfoToMainTable();
				} else if (2 == e.getClickCount() && DIALOG_TYPE_DELECT) {
					DeleteThread thread = new DeleteThread();
					thread.start();
				}
			}
		});
		table.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER && DIALOG_TYPE_COPY) {
					addInfoToMainTable();
				} else if (e.getKeyChar() == KeyEvent.VK_ENTER && DIALOG_TYPE_DELECT) {
					DeleteThread thread = new DeleteThread();
					thread.start();
				}
			}
		});
	}

	class DeleteThread extends Thread {

		@Override
		public void run() {
			deleteFromDatasource();
		}
	}

	private void deleteFromDatasource() {
		MutiTableModel model = (MutiTableModel) table.getModel();
		int[] selectRows = table.getSelectedRows();
		if (0 < selectRows.length) {
			Datasource datasource = null;
			int count = selectRows.length;
			String datasourceName = model.getTagValue(selectRows[0]).get(1).toString();
			datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(datasourceName);
			// 只选择一条数据删除
			dispose();
			workspaceTree.removeMouseListener(mouseAdapter);
			if (1 == count) {
				String datasetName = model.getTagValue(selectRows[0]).get(0).toString();
				if (JOptionPane.OK_OPTION == UICommonToolkit.showConfirmDialog(DataEditorProperties.getString("String_DelectOneDataset")
						.replace("{0}", datasourceName).replace("{1}", datasetName))) {
					boolean result = datasource.getDatasets().delete(datasetName);
					workspaceTree.dispose();
					if (result) {
						String successInfo = String.format(DataEditorProperties.getString("String_Message_DelGroupSuccess"), datasourceName, datasetName);
						Application.getActiveApplication().getOutput().output(successInfo);
					} else {
						String failedInfo = String.format(DataEditorProperties.getString("String_Message_DelGroupFailed"), datasourceName, datasetName);
						Application.getActiveApplication().getOutput().output(failedInfo);
					}
				}
			} else if (JOptionPane.OK_OPTION == UICommonToolkit.showConfirmDialog(DataEditorProperties.getString("String_DelectMoreDataset").replace("{0}",
					Integer.toString(count)))) {
				// 删除选中的多条数据
				for (int i = 0; i < selectRows.length; i++) {
					String datasetName = model.getTagValue(selectRows[i]).get(0).toString();
					boolean result = datasource.getDatasets().delete(datasetName);
					if (result) {
						String successInfo = String.format(DataEditorProperties.getString("String_Message_DelGroupSuccess"), datasourceName, datasetName);
						Application.getActiveApplication().getOutput().output(successInfo);
					} else {
						String failedInfo = String.format(DataEditorProperties.getString("String_Message_DelGroupFailed"), datasourceName, datasetName);
						Application.getActiveApplication().getOutput().output(failedInfo);
					}
				}
			}
		}

	}

	private void addInfoToMainTable() {
		try {
			int[] selectIndex = table.getSelectedRows();
			MutiTableModel model = (MutiTableModel) table.getModel();
			int rowCount = datasetCopyTable.getRowCount();
			for (int i = 0; i < selectIndex.length; i++) {
				Object[] temp = new Object[6];
				Vector<Object> selectVector = model.getTagValue(selectIndex[i]);
				temp[COLUMN_INDEX_SOURCEDATASET] = selectVector.get(0);
				Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(selectVector.get(1).toString());
				String imagePath = CommonToolkit.DatasourceImageWrap.getImageIconPath(datasource.getEngineType());
				DataCell datasoureCell = new DataCell(imagePath, datasource.getAlias());
				temp[COLUMN_INDEX_SOURCEDATASOURCE] = datasoureCell;
				temp[COLUMN_INDEX_TARGETDATASOURCE] = datasoureCell;
				temp[COLUMN_INDEX_TARGETDATASET] = datasource.getDatasets().getAvailableDatasetName(selectVector.get(0).toString());
				temp[COLUMN_INDEX_CODINGTYPE] = CommonProperties.getString("String_EncodeType_None");
				temp[COLUMN_INDEX_CHARSET] = "UTF-8";
				datasetCopyTable.addRow(temp);
			}
			if (0 < datasetCopyTable.getRowCount()) {
				datasetCopyTable.setRowSelectionInterval(rowCount, datasetCopyTable.getRowCount() - 1);
			}
			dispose();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

}
