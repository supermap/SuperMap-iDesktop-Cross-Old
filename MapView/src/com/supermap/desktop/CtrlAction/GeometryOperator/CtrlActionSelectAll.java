package com.supermap.desktop.CtrlAction.GeometryOperator;

import java.util.ArrayList;

import com.supermap.data.CursorType;
import com.supermap.data.DatasetVector;
import com.supermap.data.Recordset;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.FormMap;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.Interface.IFormMain;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.implement.SmLabel;
import com.supermap.desktop.implement.SmStatusbar;
import com.supermap.desktop.ui.FormBaseChild;
import com.supermap.desktop.ui.StatusbarManager;
import com.supermap.mapping.Layer;
import com.supermap.ui.GeometrySelectedEvent;
import com.supermap.ui.GeometrySelectedListener;

public class CtrlActionSelectAll extends CtrlAction {
	private final int SELECT_NUMBER = 1;

	public CtrlActionSelectAll(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		try {
			IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
			ArrayList<Layer> layers = CommonToolkit.MapWrap.getLayers(formMap.getMapControl().getMap());
			// 记录选择集
			int count = 0;
			for (Layer layer : layers) {
				if (layer.isVisible() && layer.isSelectable()) {
					DatasetVector dataset = (DatasetVector) layer.getDataset();
					Recordset recordset = dataset.getRecordset(false, CursorType.STATIC);
					layer.getSelection().fromRecordset(recordset);
					count += dataset.getRecordCount();
					recordset.dispose();
				}
			}
			// 更新选择个数
			FormMap activeFormMap = (FormMap) Application.getActiveApplication().getActiveForm();
			SmStatusbar statusbar = null;
			statusbar = activeFormMap.getStatusbar();
			if (null!=statusbar) {
				((SmLabel) statusbar.getComponent(SELECT_NUMBER)).setText(String.valueOf(count));
				formMap.getMapControl().getMap().refresh();
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	@Override
	public boolean enable() {
		boolean result = false;
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		ArrayList<Layer> layers = CommonToolkit.MapWrap.getLayers(formMap.getMapControl().getMap());
		for (Layer layer : layers) {
			if (layer.isVisible() && layer.isSelectable()) {
				result = true;
			}
		}
		return result;
	}
}
