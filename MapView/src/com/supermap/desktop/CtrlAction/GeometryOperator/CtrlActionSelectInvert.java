package com.supermap.desktop.CtrlAction.GeometryOperator;

import java.util.ArrayList;

import com.supermap.data.CursorType;
import com.supermap.data.DatasetVector;
import com.supermap.data.Recordset;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.FormMap;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.implement.SmLabel;
import com.supermap.desktop.implement.SmStatusbar;
import com.supermap.desktop.ui.XMLStatusbar;
import com.supermap.mapping.Layer;
import com.supermap.mapping.Selection;

public class CtrlActionSelectInvert extends CtrlAction {

	private final int SELECT_NUMBER = 1;

	public CtrlActionSelectInvert(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		try {
			IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
			ArrayList<Layer> layers = CommonToolkit.MapWrap.getLayers(formMap.getMapControl().getMap());
			for (Layer layer : layers) {
				if (layer.isVisible() && layer.getSelection() != null) {
					Recordset preRecordset = layer.getSelection().toRecordset();
					DatasetVector dataset = (DatasetVector) layer.getDataset();
					Recordset recordset = dataset.getRecordset(false, CursorType.STATIC);
					layer.getSelection().fromRecordset(recordset);
					while (!preRecordset.isEOF()) {
						layer.getSelection().remove(preRecordset.getID());
						preRecordset.moveNext();
					}
					preRecordset.dispose();
					recordset.dispose();
				}
			}
			// 更新选择个数
			FormMap activeFormMap = (FormMap) Application.getActiveApplication().getActiveForm();
			SmStatusbar statusbar = null;
			statusbar = activeFormMap.getStatusbar();
			if (null != statusbar) {
				((SmLabel) statusbar.getComponent(SELECT_NUMBER)).setText(String.valueOf(0));
				formMap.getMapControl().getMap().refresh();
			}
			formMap.getMapControl().getMap().refresh();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

}
