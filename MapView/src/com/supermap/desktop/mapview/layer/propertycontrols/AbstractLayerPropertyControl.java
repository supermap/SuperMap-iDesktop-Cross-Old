package com.supermap.desktop.mapview.layer.propertycontrols;

import java.awt.event.ActionListener;
import java.security.PrivilegedActionException;

import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import com.supermap.desktop.Application;
import com.supermap.desktop.event.ActiveDatasetsChangeListener;
import com.supermap.desktop.mapview.layer.propertymodel.LayerPropertyModel;
import com.supermap.desktop.mapview.layer.propertymodel.PropertyEnabledChangeEvent;
import com.supermap.desktop.mapview.layer.propertymodel.PropertyEnabledChangeListener;

// @formatter:off
/**
 * 1. checkbox 没有三态，需要处理。
 * 2. 三态对应控件取值设置值的问题需要处理。
 * 3. 控件关联可用不可用状态需要处理。
 * 
 * @author highsad
 *
 */
// @formatter:on
public abstract class AbstractLayerPropertyControl extends JPanel implements ILayerProperty {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean isChanged = false;
	private boolean isAutoApply = true;

	private transient LayerPropertyModel layerPropertyModel; // 原属性
	private transient LayerPropertyModel modifiedLayerPropertyModel; // 过程中修改的属性
	private transient PropertyEnabledChangeListener enabledChangeListener = new PropertyEnabledChangeListener() {

		@Override
		public void propertyEnabeldChange(PropertyEnabledChangeEvent e) {
			setControlEnabled(e.getPropertyName(), e.getEnabled());
		}
	};

	public AbstractLayerPropertyControl() {
		initializeComponents();
		initializeResources();
	}

	public LayerPropertyModel getLayerPropertyModel() {
		return this.layerPropertyModel;
	}

	public final void setLayerPropertyModel(LayerPropertyModel layerPropertyModel) {
		if (this.modifiedLayerPropertyModel != null) {
			this.modifiedLayerPropertyModel.removePropertyEnabledChangeListener(enabledChangeListener);
		}

		this.layerPropertyModel = layerPropertyModel;
		if (layerPropertyModel != null) {
			this.modifiedLayerPropertyModel = layerPropertyModel.clone();
			this.modifiedLayerPropertyModel.addPropertyEnabledChangeListener(enabledChangeListener);
		}
		unregisterEvents();
		setControlEnabled();
		fillComponents();
		registerEvents();
	}

	@Override
	public final boolean isChanged() {
		return this.isChanged;
	}

	@Override
	public final boolean isAutoApply() {
		return this.isAutoApply;
	}

	@Override
	public final void setAutoApply(boolean autoApply) {
		this.isAutoApply = autoApply;
		if (this.isAutoApply) {
			apply();
		}
	}

	@Override
	public final void addLayerPropertyChangedListener(ChangedListener listener) {
		listenerList.add(ChangedListener.class, listener);
	}

	@Override
	public final void removeLayerPropertyChangedListener(ChangedListener listener) {
		listenerList.remove(ChangedListener.class, listener);
	}

	@Override
	public final boolean apply() {
		boolean result = true;

		try {
			getLayerPropertyModel().setProperties(getModifiedLayerPropertyModel());
			getLayerPropertyModel().apply();
			getLayerPropertyModel().refresh();
			this.isChanged = false;
		} catch (Exception e) {
			result = false;
			Application.getActiveApplication().getOutput().output(e);
		}
		return result;
	}

	protected void fireLayerPropertyChanged(ChangedEvent e) {
		Object[] listeners = listenerList.getListenerList();

		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ChangedListener.class) {
				((ChangedListener) listeners[i + 1]).changed(e);
			}
		}
	}

	protected LayerPropertyModel getModifiedLayerPropertyModel() {
		return this.modifiedLayerPropertyModel;
	}

	protected final void checkChanged() {
		if (this.isAutoApply()) {
			apply();
		} else {
			int state = getLayerPropertyModel().equals(getModifiedLayerPropertyModel()) ? ChangedEvent.UNCHANGED : ChangedEvent.CHANGED;
			fireLayerPropertyChanged(new ChangedEvent(this, state));
		}
	}

	/**
	 * 初始化控件，该构造的构造，该加载的加载，该布局的布局，子类必须重写这个方法
	 */
	protected abstract void initializeComponents();

	/**
	 * 资源化，子类必须重写这个方法
	 */
	protected abstract void initializeResources();

	/**
	 * 往控件里填充数据，子类必须重写这个方法
	 */
	protected abstract void fillComponents();

	/**
	 * 注册事件，子类必须重写这个方法 由于在基类的构造函数调用这个方法， 为了避免基类调用的时候子类的 Listener 还没有构造的情况，需要使用单例模式 在子类的方法实现里构造对应的 Listener
	 */
	protected abstract void registerEvents();

	/**
	 * 注销事件，子类必须重写这个方法
	 */
	protected abstract void unregisterEvents();

	protected abstract void setControlEnabled(String propertyName, boolean enabled);

	private void setControlEnabled() {
		for (String propertyName : this.modifiedLayerPropertyModel.getPropertyNames()) {
			setControlEnabled(propertyName, this.modifiedLayerPropertyModel.isPropertyEnabled(propertyName));
		}
	}
}
