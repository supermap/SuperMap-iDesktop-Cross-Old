package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.naming.InsufficientResourcesException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.supermap.data.ColorGradientType;
import com.supermap.data.Colors;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.data.FieldInfos;
import com.supermap.data.FillGradientMode;
import com.supermap.data.GeoStyle;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.ColorsComboBox;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.ui.controls.SymbolPreViewPanel;
import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeUnique;

/**
 * 单值专题图中央面板
 * @author XiaJT
 *
 */
public class UniqueValueAttributePanel extends ThemeChangePanel{
	
	/**
	 * 属性面板
	 */
	private JPanel jPaneleAttribute;
	/**
	 * 高级面板
	 */
	private JPanel jPanelAdvanced;
	
	/**
	 * 表达式
	 */
	private JLabel jLabelExpression;
	/**
	 * 颜色方案
	 */
	private JLabel jLabelColorChoose;
	/**
	 * 偏移量单位选择
	 */
	private JLabel jLabelGetoffUnit;
	/**
	 * 水平偏移量
	 */
	private JLabel jLabelGetoffX;
	/**
	 * 垂直偏移量
	 */
	private JLabel jLabelGetoffy;
	/**
	 * 单位为mm显示的Label
	 */
	private JLabel jLabelUnitMmX;
	/**
	 * 单位为°时显示的Label
	 */
	private JLabel jLabelUnitAngleX;
	/**
	 * 单位为mm显示的Label
	 */
	private JLabel jLabelUnitMmY;
	/**
	 * 单位为°时显示的Label
	 */
	private JLabel jLabelUnitAngleY;
	
	/**
	 * 表达式选择框
	 */
	private JComboBox jComboBoxExpression;
	/**
	 * 偏移量单位选择框
	 */
	private JComboBox jComboBoxGetoffUnit;
	/**
	 * 水平方向偏移量选择框
	 */
	private JComboBox jComboBoxGetoffX;
	/**
	 * 垂直方向偏移量选择框
	 */
	private JComboBox jComboBoxGetoffY;
	
	/**
	 * 颜色方案选择框
	 */
	private ColorsComboBox jColorsComboBoxChoose;

	private ThemeUnique themeUnique;
	private SymbolPreViewPanel preViewPanel;
	private DatasetVector dataset;
	
	public UniqueValueAttributePanel(SymbolPreViewPanel previewPanel, Theme themeUnique){
		super();
		
		this.preViewPanel = previewPanel;
		//this.setBounds(5, 40, 545, 325);
		this.themeUnique = (ThemeUnique)themeUnique;
		this.dataset = (DatasetVector)previewPanel.getDataset();
		
		initComponent();
		initjPanelAttribute();
		initjPanelAdvanced();
		initThisPanel();
		initResources();
	}
    public static String getPanelName(){
    	return MapViewProperties.getString("String_PanelTitle_AttributeSet");
    }

	@Override
	protected void setTheme(Theme theme) {
		themeUnique = (ThemeUnique)theme;
	}
	
	/**
	 * 初始化组件
	 */
	private void initComponent(){
		jPanelAdvanced = new JPanel();
		jPaneleAttribute = new JPanel();
		
		jLabelExpression= new JLabel("Expression:");
		jLabelColorChoose= new JLabel("colorChoose:");
		jLabelGetoffUnit= new JLabel("GetoffUnit:");
		jLabelGetoffX= new JLabel("getoffX:");
		jLabelGetoffy= new JLabel("getoffY");
		jLabelUnitAngleX= new JLabel("°");
		jLabelUnitMmX= new JLabel("mm");
		jLabelUnitAngleY= new JLabel("°");
		jLabelUnitMmY= new JLabel("mm");
		
		jComboBoxGetoffX = new JComboBox();
		jComboBoxGetoffX.setEditable(true);
		jComboBoxGetoffY = new JComboBox();
		jComboBoxGetoffY.setEditable(true);
		jComboBoxGetoffUnit = new JComboBox();
		jComboBoxExpression = new JComboBox();
		
		
		jColorsComboBoxChoose = new ColorsComboBox();
		
		initComponentData();
		addListners();
		
	}
	
	/**
	 * 添加初始化数据
	 */
	private void initComponentData(){
		FieldInfos fieldInfos = dataset.getFieldInfos();
		
		jPanelAdvanced.setBorder(BorderFactory.createTitledBorder(MapViewProperties.getString("String_PanelTitle_AdvancedSet")));
		jPaneleAttribute.setBorder(BorderFactory.createTitledBorder(MapViewProperties.getString("String_PanelTitle_AttributeSet")));
		
		for(int i = 0; i < fieldInfos.getCount(); i++){
			// 太长 以后再改
			//String item =datasetName + "." + fieldInfos.get(i).getCaption();
			String item = fieldInfos.get(i).getCaption();
			jComboBoxExpression.addItem(item);
			jComboBoxGetoffX.addItem(item);
			jComboBoxGetoffY.addItem(item);
		}
		jComboBoxGetoffUnit.addItem(MapViewProperties.getString("String_Combobox_Coordinate"));
		jComboBoxGetoffUnit.addItem(MapViewProperties.getString("String_Combobox_MM"));
		
		jComboBoxGetoffX.getEditor().setItem("0");
		jComboBoxGetoffY.getEditor().setItem("0");
		
		jLabelUnitMmX.setVisible(false);
		jLabelUnitMmY.setVisible(false);
		
		jLabelUnitMmX.setPreferredSize(new Dimension(30, 10));
		jLabelUnitMmY.setPreferredSize(new Dimension(30, 10));
		jLabelUnitAngleX.setPreferredSize(new Dimension(30, 10));
		jLabelUnitAngleY.setPreferredSize(new Dimension(30, 10));
		
		jLabelUnitMmX.setMinimumSize(new Dimension(30, 10));
		jLabelUnitMmY.setMinimumSize(new Dimension(30, 10));
		jLabelUnitAngleX.setMinimumSize(new Dimension(30, 10));
		jLabelUnitAngleY.setMinimumSize(new Dimension(30, 10));
		
		jLabelUnitMmX.setMaximumSize(new Dimension(30, 10));
		jLabelUnitMmY.setMaximumSize(new Dimension(30, 10));
		jLabelUnitAngleX.setMaximumSize(new Dimension(30, 10));
		jLabelUnitAngleY.setMaximumSize(new Dimension(30, 10));
	}
	
	/**
	 * 初始化属性面板
	 */
	private void initjPanelAttribute(){
		jPaneleAttribute.setLayout(new GridBagLayout());
		
		jPaneleAttribute.add(jLabelExpression, 	  new GridBagConstraintsHelper(0, 0, 1, 1).setInsets(10, 10, 5, 5).setFill(GridBagConstraints.BOTH).setWeight(1, 1).setIpad(30, 0));
		jPaneleAttribute.add(jComboBoxExpression, new GridBagConstraintsHelper(1, 0, 1, 1).setInsets(10,  5, 5, 10).setFill(GridBagConstraints.BOTH).setWeight(1, 1));
		
		jPaneleAttribute.add(jLabelColorChoose,     new GridBagConstraintsHelper(0, 1, 1, 1).setInsets(5, 10, 10, 5).setFill(GridBagConstraints.BOTH).setWeight(1, 1).setIpad(30, 0));
		jPaneleAttribute.add(jColorsComboBoxChoose, new GridBagConstraintsHelper(1, 1, 1, 1).setInsets(5, 5, 10, 10).setFill(GridBagConstraints.BOTH).setWeight(1, 1));
	}
	
	/**
	 * 初始化高级面板
	 */
	private void initjPanelAdvanced(){
		jPanelAdvanced.setLayout(new GridBagLayout());
		
		jPanelAdvanced.add(jLabelGetoffUnit,    new GridBagConstraintsHelper(0, 0, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(10, 10, 5, 5).setWeight(1, 1));
		jPanelAdvanced.add(jComboBoxGetoffUnit, new GridBagConstraintsHelper(1, 0, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(10, 5, 5, 10).setWeight(1, 1));

		jPanelAdvanced.add(jLabelGetoffX,     new GridBagConstraintsHelper(0, 1, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5, 10, 5, 5 ).setWeight(1, 1));
		jPanelAdvanced.add(jComboBoxGetoffX,  new GridBagConstraintsHelper(1, 1, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5,  5, 5, 5 ).setWeight(0.5, 1));
		jPanelAdvanced.add(jLabelUnitAngleX,  new GridBagConstraintsHelper(2, 1, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5,  5, 5, 5 ).setWeight(1, 1));
		jPanelAdvanced.add(jLabelUnitMmX,     new GridBagConstraintsHelper(3, 1, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5,  5, 5, 10).setWeight(1, 1));
		
		jPanelAdvanced.add(jLabelGetoffy, 	  new GridBagConstraintsHelper(0, 2, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5, 10, 10, 5 ).setWeight(1, 1));
		jPanelAdvanced.add(jComboBoxGetoffY,  new GridBagConstraintsHelper(1, 2, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5,  5, 10, 5 ).setWeight(0.5, 1));
		jPanelAdvanced.add(jLabelUnitAngleY,  new GridBagConstraintsHelper(2, 2, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5,  5, 10, 5 ).setWeight(1, 1));
		jPanelAdvanced.add(jLabelUnitMmY,     new GridBagConstraintsHelper(3, 2, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(5,  5, 10, 10).setWeight(1, 1));
	}
	
	/**
	 * 初始化面板
	 */
	private void initThisPanel(){
		this.setLayout(new GridBagLayout());
		
		this.add(jPanelAdvanced,   new GridBagConstraintsHelper(0, 0, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(10, 10, 5, 5).setWeight(1, 1).setIpad(80, 0));
		this.add(jPaneleAttribute, new GridBagConstraintsHelper(1, 0, 1, 1).setFill(GridBagConstraints.BOTH).setInsets(10, 5, 5, 10).setWeight(1, 1));
		this.add(preViewPanel,     new GridBagConstraintsHelper(0, 1, 2, 1).setFill(GridBagConstraints.BOTH).setInsets(5, 10, 10, 5).setWeight(1, 1).setIpad(0, 120));
	}
	
	/**
	 * 返回当前的专题
	 */
	public Theme getTheme(){
		return this.themeUnique;
	}
	
	/**
	 * 添加监听器
	 */
	private void addListners(){
		
		jComboBoxGetoffUnit.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				// 设置是否需要更改
				if(!(String.valueOf(jComboBoxGetoffUnit.getSelectedItem()).equals(MapViewProperties.getString("String_Combobox_MM")) == themeUnique.isOffsetFixed())){
					themeUnique.setOffsetFixed(!themeUnique.isOffsetFixed());
					preViewPanel.refreshPreViewMapControl(themeUnique);
					
					jLabelUnitAngleX.setVisible(!jLabelUnitAngleX.isVisible());
					jLabelUnitMmX.setVisible(!jLabelUnitMmX.isVisible());
					jLabelUnitAngleY.setVisible(!jLabelUnitAngleY.isVisible());
					jLabelUnitMmY.setVisible(!jLabelUnitMmY.isVisible());
				}
			}
		});
		
		jComboBoxGetoffX.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				themeUnique.setOffsetX((String)jComboBoxGetoffX.getSelectedItem());
				preViewPanel.refreshPreViewMapControl(themeUnique);
			}
		});
		
		jComboBoxGetoffY.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				themeUnique.setOffsetY((String)jComboBoxGetoffY.getSelectedItem());
				preViewPanel.refreshPreViewMapControl(themeUnique);
			}
		});
		
		jComboBoxExpression.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(!String.valueOf(jComboBoxExpression.getSelectedItem()).equals(themeUnique.getUniqueExpression())){
					themeUnique.setUniqueExpression(String.valueOf(jComboBoxExpression.getSelectedItem()));
					preViewPanel.refreshPreViewMapControl(themeUnique);
				}
			}
		});
		
		jColorsComboBoxChoose.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				refReshColor();
				preViewPanel.refreshPreViewMapControl(themeUnique);
				// TODO 刷新表格
			}
		});
	}
	
	/**
	 * 颜色方案改变时刷新颜色
	 */
	private void refReshColor() {
		if ( jColorsComboBoxChoose != null) {
			int colorCount = ((Colors) jColorsComboBoxChoose.getSelectedItem())
					.getCount();
			Colors colors = (Colors) jColorsComboBoxChoose.getSelectedItem();
			int rangeCount = themeUnique.getCount();
			if (rangeCount > 0) {
				float ratio = (1f * colorCount) / (1f * rangeCount);
				setGeoStyleColor(themeUnique.getItem(0).getStyle(), colors.get(0));
				setGeoStyleColor(themeUnique.getItem(rangeCount - 1).getStyle(), colors.get(colorCount - 1));
				for (int i = 1; i < rangeCount - 1; i++) {
					int colorIndex = Math.round(i * ratio);
					if( colorIndex == colorCount){
						colorIndex --;
					}
					setGeoStyleColor(themeUnique.getItem(i).getStyle(), colors.get(colorIndex));
				}
			}
		}
	}
	
	/**
	 * 根据当前数据集类型设置颜色方案
	 * @param geoStyle 需要设置的风格
	 * @param color 设置的颜色
	 */
	private void setGeoStyleColor(GeoStyle geoStyle, Color color){
		DatasetType datasetType = dataset.getType();
		if(datasetType.equals(DatasetType.POINT) || datasetType.equals(DatasetType.LINE)){
			geoStyle.setLineColor(color);
		}else if(datasetType.equals(DatasetType.REGION)){
			geoStyle.setFillForeColor(color);
		}
	}
	
	/**
	 * 初始化资源化
	 */
	private void initResources(){
		jLabelColorChoose.setText(MapViewProperties.getString("String_Label_GradualColor"));
		jLabelExpression.setText(MapViewProperties.getString("String_label_Expression"));
		jLabelGetoffUnit.setText(MapViewProperties.getString("String_Label_GetoffUnit"));
		jLabelGetoffX.setText(MapViewProperties.getString("String_Label_GetoffX"));
		jLabelGetoffy.setText(MapViewProperties.getString("String_Label_GetoffY"));
		jLabelUnitAngleX.setText(MapViewProperties.getString("String_Label_Angle"));
		jLabelUnitMmX.setText(MapViewProperties.getString("String_Label_Mm"));
		jLabelUnitAngleY.setText(MapViewProperties.getString("String_Label_Angle"));
		jLabelUnitMmY.setText(MapViewProperties.getString("String_Label_Mm"));
	}
}
