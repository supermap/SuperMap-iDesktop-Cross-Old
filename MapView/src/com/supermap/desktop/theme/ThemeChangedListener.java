package com.supermap.desktop.theme;

import java.util.EventListener;
import java.util.Vector;

/**
 * 专题图改变监听器
 * @author zhaosy
 *
 */
 interface ThemeChangedListener extends EventListener {
    public void themeHandleChanged(ThemeChangedEvent event);
}
