package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JSpinner.NumberEditor;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetVector;
import com.supermap.data.Enum;
import com.supermap.data.FieldInfo;
import com.supermap.data.Point2D;
import com.supermap.data.TextAlignment;
import com.supermap.data.TextStyle;
import com.supermap.desktop.Application;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.ColorSwatch;
import com.supermap.desktop.ui.controls.ControlButton;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.DropDownColor;
import com.supermap.desktop.ui.controls.FontComboBox;
import com.supermap.desktop.ui.controls.SQLExpressionDialog;
import com.supermap.desktop.ui.controls.SymbolPreViewPanel;
import com.supermap.layout.MapLayout;
import com.supermap.mapping.Map;
import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeLabel;

class ThemeLabelUniformStylePanel extends ThemeChangePanel {
	private JComboBox<String> jComboBoxLabelExpression;
	private JSpinner jSpinnerItalicAngle;
	private JSpinner jSpinnerRotation;

	private JComboBox jComboBoxTextAlignment;
	private JComboBox jComboBoxFontName;

	private JPanel jPanelUp;
	private SymbolPreViewPanel jPanelLayyerView;
	private JPanel jPanelFontEffect;

	private JButton jButtonBackgroundColor;
	private DropDownColor backgroundColorDropDown;
	private JButton jButtonTextColor;

	private ColorSwatch colorSwatch;
	private ColorSwatch textColorSwatch;

	private JTextField jTextFieldFontSize;
	private DropDownColor textColorDropDown;

	private JSpinner jSpinnerFontWidth;
	private JSpinner jSpinnerFontHeight;

	private JCheckBox jCheckBoxBold;
	private JCheckBox jCheckBoxStrikeout;
	private JCheckBox jCheckBoxItalic;
	private JCheckBox jCheckBoxShadow;
	private JCheckBox jCheckBoxOutline;
	private JCheckBox jCheckBoxUnderline;
	private JCheckBox jCheckBoxSizeFixed;
	private JCheckBox jCheckBoxBackOpaque;
	private SQLExpressionDialog sqlDialog;

	private ThemeLabel currentTheme;
	private TextStyle textStyle;

	// 地图或者是布局
	private Object mapObject;
	// 经验值
	private final static double EXPERIENCE = 0.283;
	// 单位转换
	private final static int UNIT_CONVERSION = 10;

	// 对齐方式名称和对齐方式值构成的HashMap
	private static HashMap<String, Integer> hashMapTextAlignment = new HashMap<String, Integer>();
	// 对齐方式名称
	private static final String[] TEXTALIGNMENT_NAMES = { ControlsProperties.getString("String_TextAlignment_LeftTop"),
			ControlsProperties.getString("String_TextAlignment_MidTop"),
			ControlsProperties.getString("String_TextAlignment_RightTop"),
			ControlsProperties.getString("String_TextAlignment_LeftBaseline"),
			ControlsProperties.getString("String_TextAlignment_MidBaseline"),
			ControlsProperties.getString("String_TextAlignment_RightBaseline"),
			ControlsProperties.getString("String_TextAlignment_LeftBottom"),
			ControlsProperties.getString("String_TextAlignment_MidBottom"),
			ControlsProperties.getString("String_TextAlignment_RightBottom"),
			ControlsProperties.getString("String_TextAlignment_LeftMid"),
			ControlsProperties.getString("String_TextAlignment_Mid"),
			ControlsProperties.getString("String_TextAlignment_RightMid"),
	};
	// // 默认的字号
	// private final static String[] FONT_WEIGHT = { "1", "2", "3", "4", "5",
	// "5.5", "6.5", "7.5", "8", "9", "10", "11", "12", "14", "16", "18",
	// "20", "22", "24", "26", "28", "36", "48", "72" };
	// // 字号框中只能填入的串
	// private final static String VALID_FONTSIZE_STRING = ".0123456789";

	// 最大字高和字宽
	private final static double MAX_FONT_HEIGHT_WIDTH = Double.MAX_VALUE;
	// 是否是初始化的情况，该字段用于字号的全面处理，from .net
	private boolean isInInitialState = false;

	private double fontHeight;
	private double fontWidth;

	/**
	 * 构造方法
	 * 
	 * @param previewPanel
	 * @param themeLabel
	 */
	public ThemeLabelUniformStylePanel(SymbolPreViewPanel previewPanel,
			Theme themeLabel) {
		super();
		setLayout(null);
		this.setBounds(5, 35, 545, 325);

		init();
		this.jPanelLayyerView = previewPanel;
		this.currentTheme = (ThemeLabel) themeLabel;

		// TextStyle
		textStyle = currentTheme.getUniformStyle();

		// upPanel
		getUpPanel();
		getLayyerViewPanel();
		getFontStylePanel();
		setTheme(themeLabel);
	}

	/**
	 * 获取Panel名称
	 * 
	 * @return Panel名称
	 */
	public static String getPanelName() {
		return MapViewProperties.getString("String_PanelTitle_BasicSet");
	}

	// 文本风格中地图单位的字高、字宽与字号、逻辑字高、字宽之间的转换
	static double fontWidthToMapWidth(double fontWidth, Object mapObject,
			boolean isSizeFixed) {
		double mapWidth = 0;
		try {
			mapWidth = fontWidth;
			if (!isSizeFixed) {
				Point2D logicalPntEnd = new Point2D(fontWidth, fontWidth);
				Point2D logicalPntStart = new Point2D(0, 0);
				Point2D pointEnd = new Point2D();
				Point2D pointStart = new Point2D();
				if (mapObject instanceof Map) {
					Map map = (Map) mapObject;
					if (map != null) {
						pointEnd = map.logicalToMap(logicalPntEnd);
						pointStart = map.logicalToMap(logicalPntStart);
					}
				} else if (mapObject instanceof MapLayout) {
					MapLayout mapLayout = (MapLayout) mapObject;
					if (mapLayout != null) {
						pointEnd = mapLayout.logicalToLayout(logicalPntEnd);
						pointStart = mapLayout.logicalToLayout(logicalPntStart);
					}
				}
				mapWidth = Math.abs(pointEnd.getX() - pointStart.getX());
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return mapWidth;
	}

	static double fontSizeToMapHeight(double fontSize, Object mapObject,
			boolean isSizeFixed) {
		double mapHeight = 0;
		try {
			mapHeight = fontSize / EXPERIENCE;
			if (isSizeFixed) {
				mapHeight = fontSize / EXPERIENCE;
			} else {
				Double fontHeight = fontSize / EXPERIENCE;
				Point2D logicalPntEnd = new Point2D(fontHeight, fontHeight);
				Point2D logicalPntStart = new Point2D(0, 0);
				Point2D pointEnd = new Point2D();
				Point2D pointStart = new Point2D();
				if (mapObject instanceof Map) {
					Map map = (Map) mapObject;
					if (map != null) {
						pointEnd = map.logicalToMap(logicalPntEnd);
						pointStart = map.logicalToMap(logicalPntStart);
					}
				} else if (mapObject instanceof MapLayout) {
					MapLayout mapLayout = (MapLayout) mapObject;
					if (mapLayout != null) {
						pointEnd = mapLayout.logicalToLayout(logicalPntEnd);
						pointStart = mapLayout.logicalToLayout(logicalPntStart);
					}
				}
				mapHeight = Math.abs(pointEnd.getY() - pointStart.getY());
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return mapHeight;
	}

	static double mapWidthToFontWidth(double mapWidth, Object mapObject,
			boolean isSizeFixed) {
		double fontWidth = 0;
		try {
			if (!isSizeFixed) {
				fontWidth = mapWidth;
				Point2D pointEnd = new Point2D(mapWidth, mapWidth);
				Point2D pointStart = new Point2D(0, 0);
				Point2D logicalPntEnd = new Point2D();
				Point2D logicalPntStart = new Point2D();
				if (mapObject instanceof Map) {
					Map map = (Map) mapObject;
					if (map != null) {
						logicalPntEnd = map.mapToLogical(pointEnd);
						logicalPntStart = map.mapToLogical(pointStart);
					}
				} else if (mapObject instanceof MapLayout) {
					MapLayout mapLayout = (MapLayout) mapObject;
					if (mapLayout != null) {
						logicalPntEnd = mapLayout.layoutToLogical(pointEnd);
						logicalPntStart = mapLayout.layoutToLogical(pointStart);
					}
				}
				fontWidth = Math.abs(logicalPntEnd.getY()
						- logicalPntStart.getY());
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return fontWidth;
	}

	private JComboBox getComboBoxLabelExpression() {
		if (jComboBoxLabelExpression == null) {
			jComboBoxLabelExpression = new JComboBox<String>();
			jComboBoxLabelExpression.setBounds(85, 10, 94, 18);
			// 初始化标签表达式
			Dataset m_dataset = jPanelLayyerView.getDataset();
			int count = ((DatasetVector) m_dataset).getFieldInfos().getCount();
			for (int i = 0; i < count; i++) {
				FieldInfo fieldInfo = ((DatasetVector) m_dataset)
						.getFieldInfos().get(i);
				jComboBoxLabelExpression.addItem(fieldInfo.getName());
			}
			jComboBoxLabelExpression.addItem(MapViewProperties.getString("String_Expression"));
			getUpPanel().add(jComboBoxLabelExpression);
			jComboBoxLabelExpression.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						// 判断是否为最后一项“表达式”
						if (jComboBoxLabelExpression.getSelectedIndex() == jComboBoxLabelExpression
								.getItemCount() - 1) {
							sqlDialog = new SQLExpressionDialog();
							DialogResult dialogResult = sqlDialog.showDialog(new Dataset[] { jPanelLayyerView.getDataset() });
							if (dialogResult == DialogResult.OK) {
								String filter = sqlDialog.getQueryParameter()
										.getAttributeFilter();
								if (filter != null && !filter.trim().equals("")) {
									jComboBoxLabelExpression.insertItemAt(filter, jComboBoxLabelExpression.getItemCount() - 1);
									jComboBoxLabelExpression.setSelectedIndex(jComboBoxLabelExpression.getItemCount() - 2);
								}
							}
						}
						currentTheme.setLabelExpression(jComboBoxLabelExpression.getSelectedItem().toString());
						jPanelLayyerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
		}
		return jComboBoxLabelExpression;
	}

	private void init() {
		int[] textAlignmentValues = Enum.getValues(TextAlignment.class);
		for (int i = 0; i < textAlignmentValues.length; i++) {
			hashMapTextAlignment.put(TEXTALIGNMENT_NAMES[i],
					textAlignmentValues[i]);
		}
	}

	private JPanel getUpPanel() {
		if (jPanelUp == null) {
			jPanelUp = new JPanel();
			jPanelUp.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, false));
			jPanelUp.setLayout(null);
			jPanelUp.setBounds(2, 8, 523, 119);

			getAllLabels();
			jPanelUp.add(getComboBoxFontName());
			jPanelUp.add(getComboBoxTextAlignment());

			jPanelUp.add(getSpinnerFontHeight());
			jPanelUp.add(getSpinnerFontWidth());
			jPanelUp.add(getButtonBackgroundColor());

			jPanelUp.add(getSpinnerRotation());
			jPanelUp.add(getSpinnerItalicAngle());
			jPanelUp.add(getButtonTextColor());
			add(jPanelUp);

			final JLabel fontName = new JLabel();
			fontName.setBounds(10, 10, 74, 20);
			fontName.setText(MapViewProperties.getString("String_LabelExpression"));
			jPanelUp.add(fontName);
			jPanelUp.add(getComboBoxLabelExpression());

		}
		return jPanelUp;
	}

	private DropDownColor getButtonTextColor() {
		if (jButtonTextColor == null) {
			jButtonTextColor = new ControlButton();
			jButtonTextColor.setPreferredSize(new Dimension(78, 15));
			textColorSwatch = new ColorSwatch(textStyle.getForeColor(), 9,
					70);
			jButtonTextColor.setIcon(textColorSwatch);

			textColorDropDown = new DropDownColor(jButtonTextColor);
			// 这里把期望宽度设置的略大一些，保证在网格中显示正确
			// m_textColorDropDown.setPreferredSize(new Dimension(150, 33));
			textColorDropDown.setBounds(245, 5, 94, 25);
			textColorDropDown.addPropertyChangeListener("m_selectionColors",
					new PropertyChangeListener() {
						public void propertyChange(PropertyChangeEvent evt) {
							if (textStyle != null && !isInInitialState) {
								Color color = textColorDropDown.getColor();
								if (color != null) {
									textStyle.setForeColor(color);
									textColorSwatch.setColor(color);
									jButtonTextColor.repaint();
									refreshPreViewMapControl();
								}
							}
						}
					});
		}
		return textColorDropDown;
	}

	protected JSpinner getSpinnerItalicAngle() {
		if (jSpinnerItalicAngle == null) {
			SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(
					textStyle.getItalicAngle(), -60.0, 60.0, 1.0);
			jSpinnerItalicAngle = new JSpinner(spinnerNumberModel);
			jSpinnerItalicAngle.setBounds(420, 50, 94, 18);
			final NumberEditor numberEditor = (JSpinner.NumberEditor) jSpinnerItalicAngle
					.getEditor();
			numberEditor.getTextField().setHorizontalAlignment(JTextField.LEFT);
			JTextField textField = numberEditor.getTextField();
			// 初始化该值
			textField.setText(String.valueOf(textStyle.getItalicAngle()));
			numberEditor.getTextField().addCaretListener(new CaretListener() {

				public void caretUpdate(CaretEvent e) {
					if (textStyle != null && !isInInitialState) {
						String value = numberEditor.getTextField().getText();
						if (value == null || value.equals("")) {
							return;
						}
						try {
							Double doubleValue = Double.valueOf(value);
							// 保证精度为0.1
							String temp = String.valueOf(doubleValue);
							if (temp.indexOf(".") <= temp.length() - 2) {
								temp = temp.substring(0, temp.indexOf(".") + 2);
							}
							textStyle.setItalicAngle(Double.valueOf(temp));
						} catch (Exception e2) {
							// ignore
						}
						refreshPreViewMapControl();
					}
				}
			});
			numberEditor.getTextField().addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent e) {
					try {
						Double italicAngle = Double.valueOf(numberEditor
								.getTextField().getText());
						if (Double.compare(italicAngle, 60.0) > 0) {
							numberEditor.getTextField().setText("60.0");
						} else if (Double.compare(italicAngle, -60.0) < 0) {
							numberEditor.getTextField().setText("-60.0");
						} else {
							numberEditor.getTextField().setText(
									String
											.valueOf(textStyle
													.getItalicAngle()));
						}
					} catch (Exception e2) {
						// ignore
					}
				}
			});
		}
		return jSpinnerItalicAngle;
	}

	private JSpinner getSpinnerRotation() {
		if (jSpinnerRotation == null) {
			SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(
					textStyle.getRotation(), 0.0, 360.0, 1.0);
			jSpinnerRotation = new JSpinner(spinnerNumberModel);
			jSpinnerRotation.setBounds(420, 10, 94, 18);
			final NumberEditor numberEditor = (JSpinner.NumberEditor) jSpinnerRotation
					.getEditor();
			numberEditor.getTextField().setHorizontalAlignment(JTextField.LEFT);
			// JTextField textField = numberEditor.getTextField();
			// // 初始化当前值
			// textField.setText(String.valueOf(m_textStyle.getRotation()));
			numberEditor.getTextField().addCaretListener(new CaretListener() {
				public void caretUpdate(CaretEvent e) {
					if (textStyle != null && !isInInitialState) {
						String value = numberEditor.getTextField().getText();
						if (value == null || value.equals("")
								|| value.indexOf("-") == 0) {
							if (value.indexOf("-") == 0) {
								SwingUtilities.invokeLater(new Runnable() {
									public void run() {
										numberEditor.getTextField().setText(
												String.valueOf(textStyle
														.getRotation()));
									}
								});
							}
							return;
						}
						try {
							Double doubleValue = Double.valueOf(value);

							// 保证精度为0.1
							String temp = String.valueOf(doubleValue);
							if (temp.indexOf(".") <= temp.length() - 2) {
								temp = temp.substring(0, temp.indexOf(".") + 2);
							}
							textStyle.setRotation(Double.valueOf(temp));
						} catch (Exception e2) {
							// 如果Double.value()抛异常，则忽略
						}
						refreshPreViewMapControl();
					}
				}
			});
			numberEditor.getTextField().addFocusListener(new FocusAdapter() {

				public void focusLost(FocusEvent e) {
					try {
						Double rotation = Double.valueOf(numberEditor
								.getTextField().getText());
						if (Double.compare(rotation, 360.0) > 0) {
							numberEditor.getTextField().setText("360.0");
						} else if (Double.compare(rotation, 0.0) < 0) {
							numberEditor.getTextField().setText("0");
						} else {
							numberEditor.getTextField().setText(
									String.valueOf(textStyle.getRotation()));
						}
					} catch (Exception e2) {
						// 如果Double.value()抛异常，则忽略
					}
				}

			});
		}
		return jSpinnerRotation;
	}

	private JSpinner getSpinnerFontWidth() {
		if (jSpinnerFontWidth == null) {
			SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(
					textStyle.getFontWidth(), 0.0, MAX_FONT_HEIGHT_WIDTH, 1.0);
			jSpinnerFontWidth = new JSpinner(spinnerNumberModel);
			jSpinnerFontWidth.setBounds(245, 85, 94, 18);

			final NumberEditor numberEditor = (JSpinner.NumberEditor) jSpinnerFontWidth
					.getEditor();
			JTextField textField = numberEditor.getTextField();
			textField.setHorizontalAlignment(JTextField.LEFT);
			textField.addCaretListener(new CaretListener() {
				public void caretUpdate(CaretEvent e) {
					if (textStyle != null && !isInInitialState) {
						String value = numberEditor.getTextField().getText();
						if (value == null || value.equals("")) {
							return;
						}
						try {
							fontWidth = Double.valueOf(value);
							final DecimalFormat decimalFormat = new DecimalFormat(
									"#0.0");
							fontWidth = Double.valueOf(decimalFormat
									.format(fontWidth));
						} catch (Exception e2) {
							// Ignore
						}
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jSpinnerFontWidth;
	}

	private JSpinner getSpinnerFontHeight() {
		if (jSpinnerFontHeight == null) {
			SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(
					textStyle.getFontHeight(), 0, MAX_FONT_HEIGHT_WIDTH, 1.0);
			jSpinnerFontHeight = new JSpinner(spinnerNumberModel);
			jSpinnerFontHeight.setBounds(245, 50, 94, 18);
			final NumberEditor numberEditor = (JSpinner.NumberEditor) jSpinnerFontHeight
					.getEditor();
			numberEditor.getTextField().setHorizontalAlignment(JTextField.LEFT);

			numberEditor.getTextField().addCaretListener(new CaretListener() {
				public void caretUpdate(CaretEvent e) {
					if (textStyle != null && !isInInitialState) {
						String value = numberEditor.getTextField().getText();
						if (value == null || value.equals("")) {
							return;
						}
						try {
							fontHeight = Double.valueOf(value);
							final DecimalFormat decimalFormat = new DecimalFormat(
									"#0.0");
							fontHeight = Double.valueOf(decimalFormat
									.format(fontHeight));
						} catch (Exception e2) {
							// Ignore
						}
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jSpinnerFontHeight;
	}

	private DropDownColor getButtonBackgroundColor() {
		if (jButtonBackgroundColor == null) {
			jButtonBackgroundColor = new ControlButton();
			jButtonBackgroundColor.setPreferredSize(new Dimension(78, 15));
			jButtonBackgroundColor.setEnabled(!getCheckBoxBackOpaque()
					.isSelected());
			colorSwatch = new ColorSwatch(textStyle.getBackColor(), 9, 70);
			// if (!m_textStyle.getBackOpaque()) {
			// m_buttonBackgroundColor.setIcon(null);
			// } else {
			// m_buttonBackgroundColor.setIcon(m_colorSwatch);
			// }
			backgroundColorDropDown = new DropDownColor(
					jButtonBackgroundColor);
			backgroundColorDropDown.getArrowButton().setEnabled(
					!getCheckBoxBackOpaque().isSelected());
			// 这里把期望宽度设置的略大一些，保证在网格中显示正确
			backgroundColorDropDown.setBounds(420, 80, 94, 24);
			backgroundColorDropDown.addPropertyChangeListener(
					"m_selectionColors", new PropertyChangeListener() {
						public void propertyChange(PropertyChangeEvent evt) {
							if (textStyle != null && !isInInitialState) {
								Color color = backgroundColorDropDown
										.getColor();
								if (color != null) {
									textStyle.setBackColor(color);
									colorSwatch.setColor(color);
									jButtonBackgroundColor.repaint();
									refreshPreViewMapControl();
								}
							}
						}
					});
		}
		return backgroundColorDropDown;
	}

	private JComboBox getComboBoxTextAlignment() {
		if (jComboBoxTextAlignment == null) {
			jComboBoxTextAlignment = new JComboBox(TEXTALIGNMENT_NAMES);
			jComboBoxTextAlignment.setBounds(85, 85, 94, 18);
			String name = ControlsProperties.getString("String_TextAlignment_LeftTop");
			Object[] textAlignmentValues = hashMapTextAlignment.values()
					.toArray();
			Object[] textAlignmentNames = hashMapTextAlignment.keySet()
					.toArray();
			for (int i = 0; i < textAlignmentValues.length; i++) {
				Integer temp = (Integer) textAlignmentValues[i];
				if (temp == textStyle.getAlignment().value()) {
					name = (String) textAlignmentNames[i];
				}
			}
			jComboBoxTextAlignment.setSelectedItem(name);
			jComboBoxTextAlignment.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						if (textStyle != null && !isInInitialState) {
							String textAlignmentName = jComboBoxTextAlignment
									.getSelectedItem().toString();
							int value = hashMapTextAlignment
									.get(textAlignmentName);
							TextAlignment textAlignment = (TextAlignment) Enum
									.parse(TextAlignment.class, value);
							textStyle.setAlignment(textAlignment);
							refreshPreViewMapControl();
						}
					}
				}
			});
		}
		return jComboBoxTextAlignment;
	}

	private void getAllLabels() {
		JLabel fontName = new JLabel();
		fontName.setHorizontalAlignment(SwingConstants.CENTER);
		fontName.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontName"));
		fontName.setBounds(5, 50, 69, 18);
		jPanelUp.add(fontName);

		JLabel snapLabel = new JLabel();
		snapLabel.setHorizontalAlignment(SwingConstants.CENTER);
		snapLabel.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelAlinement"));
		snapLabel.setBounds(0, 85, 74, 18);
		jPanelUp.add(snapLabel);

		JLabel backgroundColor = new JLabel();
		backgroundColor.setHorizontalAlignment(SwingConstants.CENTER);
		backgroundColor.setText(MapViewProperties.getString("String_Label_BackColor"));
		backgroundColor.setBounds(355, 85, 66, 18);
		jPanelUp.add(backgroundColor);

		JLabel fontHeight = new JLabel();
		fontHeight.setHorizontalAlignment(SwingConstants.LEFT);
		fontHeight.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontHeight"));
		fontHeight.setBounds(195, 50, 44, 18);
		jPanelUp.add(fontHeight);

		JLabel fontWidth = new JLabel();
		fontWidth.setHorizontalAlignment(SwingConstants.LEFT);
		fontWidth.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontWidth"));
		fontWidth.setBounds(195, 85, 44, 18);
		jPanelUp.add(fontWidth);

		JLabel rotation = new JLabel();
		rotation.setHorizontalAlignment(SwingConstants.CENTER);
		rotation.setText(ControlsProperties.getString("String_Label_SymbolAngle"));
		rotation.setBounds(360, 10, 55, 20);
		jPanelUp.add(rotation);

		JLabel italicAngle = new JLabel();
		italicAngle.setHorizontalAlignment(SwingConstants.CENTER);
		italicAngle.setText(ControlsProperties.getString("String_Label_ItalicAngle"));
		italicAngle.setBounds(360, 50, 55, 18);
		jPanelUp.add(italicAngle);

		JLabel ttextColor = new JLabel();
		ttextColor.setHorizontalAlignment(SwingConstants.LEFT);
		ttextColor.setText(ControlsProperties.getString("String_Label_TextStyleForeColor"));
		ttextColor.setBounds(195, 10, 44, 18);
		jPanelUp.add(ttextColor);
	}

	private JComboBox<?> getComboBoxFontName() {
		if (jComboBoxFontName == null) {
			jComboBoxFontName = new FontComboBox();
			jComboBoxFontName.setBounds(85, 50, 94, 18);
			// // 初始化当前comboBox的值为m_currentTheme内的m_textStyle的fontName
			// m_comboBoxFontName.setSelectedItem(m_textStyle.getFontName());
			jComboBoxFontName.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						if (textStyle != null && !isInInitialState) {
							textStyle.setFontName(jComboBoxFontName
									.getSelectedItem().toString());
							refreshPreViewMapControl();
						}
					}
				}
			});
		}
		return jComboBoxFontName;
	}

	private SymbolPreViewPanel getLayyerViewPanel() {
		jPanelLayyerView.setBounds(5, 127, 250, 200);
		add(jPanelLayyerView);
		return jPanelLayyerView;
	}

	private JPanel getFontStylePanel() {
		if (jPanelFontEffect == null) {
			jPanelFontEffect = new JPanel();
			jPanelFontEffect.setLayout(null);
			jPanelFontEffect.setBorder(new TitledBorder(new LineBorder(
					Color.LIGHT_GRAY, 1, false), ControlsProperties.getString("String_GeometryPropertyTextControl_GroupBoxFontEffect"),
					TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, null, null));
			jPanelFontEffect.setBounds(252, 127, 275, 200);

			jPanelFontEffect.add(getCheckBoxBold());
			jPanelFontEffect.add(getCheckBoxStrikeout());
			jPanelFontEffect.add(getCheckBoxItalic());
			jPanelFontEffect.add(getCheckBoxShadow());

			jPanelFontEffect.add(getCheckBoxOutline());
			jPanelFontEffect.add(getCheckBoxUnderline());
			jPanelFontEffect.add(getCheckBoxSizeFixed());
			jPanelFontEffect.add(getCheckBoxBackOpaque());
			add(jPanelFontEffect);
		}
		return jPanelFontEffect;
	}

	private JCheckBox getCheckBoxBold() {
		if (jCheckBoxBold == null) {
			jCheckBoxBold = new JCheckBox();
			jCheckBoxBold.setHorizontalAlignment(SwingConstants.CENTER);
			jCheckBoxBold.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_CheckBoxBold"));
			jCheckBoxBold.setBounds(44, 24, 51, 26);
			// m_textStyle.setBold(m_textStyle.getBold());
			jCheckBoxBold.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						textStyle.setBold(jCheckBoxBold.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxBold;
	}

	private JCheckBox getCheckBoxItalic() {
		if (jCheckBoxItalic == null) {
			jCheckBoxItalic = new JCheckBox();
			jCheckBoxItalic.setHorizontalAlignment(SwingConstants.CENTER);
			jCheckBoxItalic.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_CheckBoxItalic"));
			jCheckBoxItalic.setBounds(45, 65, 51, 26);
			// m_checkBoxItalic.setSelected(m_textStyle.getItalic());
			jCheckBoxItalic.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						textStyle.setItalic(jCheckBoxItalic.isSelected());
						jSpinnerItalicAngle.setEnabled(jCheckBoxItalic.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxItalic;
	}

	private JCheckBox getCheckBoxOutline() {
		if (jCheckBoxOutline == null) {
			jCheckBoxOutline = new JCheckBox();
			jCheckBoxOutline.setHorizontalAlignment(SwingConstants.CENTER);
			jCheckBoxOutline.setText(ControlsProperties.getString("String_Outline"));
			jCheckBoxOutline.setBounds(45, 145, 51, 26);
			// m_checkBoxOutline.setSelected(m_textStyle.getOutline());
			jCheckBoxOutline.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						textStyle.setOutline(jCheckBoxOutline.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxOutline;
	}

	private JCheckBox getCheckBoxShadow() {
		if (jCheckBoxShadow == null) {
			jCheckBoxShadow = new JCheckBox();
			jCheckBoxShadow.setHorizontalAlignment(SwingConstants.CENTER);
			jCheckBoxShadow.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_CheckBoxShadow"));
			jCheckBoxShadow.setBounds(45, 105, 51, 26);
			// m_checkBoxShadow.setSelected(m_textStyle.getShadow());
			jCheckBoxShadow.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						textStyle.setShadow(jCheckBoxShadow.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxShadow;
	}

	private JCheckBox getCheckBoxUnderline() {
		if (jCheckBoxUnderline == null) {
			jCheckBoxUnderline = new JCheckBox();
			jCheckBoxUnderline.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_CheckBoxUnderline"));
			jCheckBoxUnderline.setBounds(134, 65, 64, 26);
			// m_checkBoxUnderline.setSelected(m_textStyle.getUnderline());
			jCheckBoxUnderline.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						textStyle.setUnderline(jCheckBoxUnderline
								.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxUnderline;
	}

	private JCheckBox getCheckBoxStrikeout() {
		if (jCheckBoxStrikeout == null) {
			jCheckBoxStrikeout = new JCheckBox();
			jCheckBoxStrikeout.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_CheckBoxStrikeout"));
			jCheckBoxStrikeout.setBounds(134, 24, 64, 26);
			// m_checkBoxStrikeout.setSelected(m_textStyle.getStrikeout());
			jCheckBoxStrikeout.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						textStyle.setStrikeout(jCheckBoxStrikeout
								.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxStrikeout;
	}

	protected JCheckBox getCheckBoxBackOpaque() {
		if (jCheckBoxBackOpaque == null) {
			jCheckBoxBackOpaque = new JCheckBox();
			jCheckBoxBackOpaque.setText(MapViewProperties.getString("String_BackgroundNotTransparency"));
			jCheckBoxBackOpaque.setBounds(134, 145, 92, 26);
			jCheckBoxBackOpaque.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						jButtonBackgroundColor.setEnabled(jCheckBoxBackOpaque
								.isSelected());
						textStyle.setBackOpaque(jCheckBoxBackOpaque
								.isSelected());
						backgroundColorDropDown.getArrowButton().setEnabled(
								jCheckBoxBackOpaque.isSelected());
						if (!jCheckBoxBackOpaque.isSelected()) {
							jButtonBackgroundColor.setIcon(null);
						} else {
							jButtonBackgroundColor.setIcon(colorSwatch);
						}
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxBackOpaque;
	}

	private JCheckBox getCheckBoxSizeFixed() {
		if (jCheckBoxSizeFixed == null) {
			jCheckBoxSizeFixed = new JCheckBox();
			jCheckBoxSizeFixed.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_CheckBoxIsSizeFixed"));
			jCheckBoxSizeFixed.setBounds(134, 105, 77, 26);
			jCheckBoxSizeFixed.setEnabled(false);
			jCheckBoxSizeFixed.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (textStyle != null && !isInInitialState) {
						jSpinnerFontWidth.setEnabled(!jCheckBoxSizeFixed.isSelected());
						// m_textStyle.setSizeFixed(!m_checkBoxSizeFixed.isSelected());
						refreshPreViewMapControl();
					}
				}
			});
		}
		return jCheckBoxSizeFixed;
	}

	private void refreshPreViewMapControl() {
		textStyle.setFontHeight(fontHeight);
		if (jSpinnerFontWidth.isEnabled()) {
			textStyle.setFontWidth(fontWidth);
		}

		currentTheme.setUniformStyle(textStyle);
		jPanelLayyerView.refreshPreViewMapControl(currentTheme);
	}

	protected Theme getTheme() {
		return currentTheme;
	}

	protected void setTheme(Theme theme) {
		currentTheme = (ThemeLabel) theme;
		textStyle = currentTheme.getUniformStyle();

		// 判断是否为固定大小
		jSpinnerFontWidth.setEnabled(true);

		// 初始化字高,字宽
		fontHeight = textStyle.getFontHeight();
		fontWidth = textStyle.getFontWidth();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				jSpinnerFontHeight.setValue(fontHeight);
				jSpinnerFontWidth.setValue(fontWidth);
			}
		});

		// 初始化标签表达式
		if ("".equals(currentTheme.getLabelExpression().trim())) {
			currentTheme.setLabelExpression(jComboBoxLabelExpression.getItemAt(0).toString());
		} else {
			jComboBoxLabelExpression.setSelectedItem(currentTheme.getLabelExpression());
		}

		// 初始化当前comboBox的值为m_currentTheme内的m_textStyle的fontName
		jComboBoxFontName.setSelectedItem(textStyle.getFontName());
		String name = ControlsProperties.getString("String_TextAlignment_LeftTop");
		Object[] textAlignmentValues = hashMapTextAlignment.values()
				.toArray();
		Object[] textAlignmentNames = hashMapTextAlignment.keySet().toArray();
		for (int i = 0; i < textAlignmentValues.length; i++) {
			Integer temp = (Integer) textAlignmentValues[i];
			if (temp == textStyle.getAlignment().value()) {
				name = (String) textAlignmentNames[i];
			}
		}
		jComboBoxTextAlignment.setSelectedItem(name);
		jSpinnerFontWidth.setEnabled(!getCheckBoxSizeFixed().isSelected());

		// 初始化背景颜色
		if (textStyle.getBackOpaque()) {
			jButtonBackgroundColor.setEnabled(true);
			backgroundColorDropDown.getArrowButton().setEnabled(true);
			colorSwatch.setColor(textStyle.getBackColor());
			jButtonBackgroundColor.setIcon(colorSwatch);
		} else {
			jButtonBackgroundColor.setEnabled(false);
			backgroundColorDropDown.getArrowButton().setEnabled(false);
		}

		// 初始化文本色
		textColorSwatch.setColor(textStyle.getForeColor());
		jButtonTextColor.setIcon(textColorSwatch);

		// 初始化旋转角度
		final NumberEditor numberEditor = (JSpinner.NumberEditor) jSpinnerRotation
				.getEditor();
		JTextField textField = numberEditor.getTextField();
		textField.setText(String.valueOf(textStyle.getRotation()));

		// 初始化倾斜角度
		final NumberEditor numberEditorTwo = (JSpinner.NumberEditor) jSpinnerItalicAngle
				.getEditor();
		JTextField textFieldTwo = numberEditorTwo.getTextField();
		textFieldTwo.setText(String.valueOf(textStyle.getItalicAngle()));

		// 如果倾斜checkBox选中的话,m_spinnerItalicAngle才可用
		jSpinnerItalicAngle.setEnabled(jCheckBoxItalic.isSelected());

		// 斜体初始化textStyle内的属性值
		textStyle.setBold(textStyle.getBold());
		jCheckBoxStrikeout.setSelected(textStyle.getStrikeout());
		jCheckBoxItalic.setSelected(textStyle.getItalic());
		jCheckBoxUnderline.setSelected(textStyle.getUnderline());
		jCheckBoxShadow.setSelected(textStyle.getShadow());
		// m_checkBoxSizeFixed.setSelected(m_textStyle.isSizeFixed());
		jCheckBoxOutline.setSelected(textStyle.getOutline());

		// 背景透明初始化
		jCheckBoxBackOpaque.setSelected(textStyle.getBackOpaque());
	}
}
