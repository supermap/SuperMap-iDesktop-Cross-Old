package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent.EventType;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.data.FieldInfo;
import com.supermap.data.FieldType;
import com.supermap.data.GeoStyle;
import com.supermap.data.Resources;
import com.supermap.data.SymbolType;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.SymbolDialog;
import com.supermap.desktop.ui.controls.SymbolPreViewPanel;
import com.supermap.mapping.LabelBackShape;
import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeLabel;

/**
 * @author WDY
 */
class ThemeLabelAttributePanel extends ThemeChangePanel {
	private JPanel jPanelEffectSetting;
	private JPanel jPanelLabelOffset;
	private SymbolPreViewPanel jPanelLayerView;
	private JPanel jPanelBackgroundSetting;
	private JLabel jLabelNumericPrecision = new JLabel();
	private JLabel jLabelOffsetUnit = new JLabel();
	private JLabel jLabelOffsetX = new JLabel();
	private JLabel jLabelOffsetY = new JLabel();

	private JComboBox<String> jComboBoxNumericPrecision;
	private JComboBox<String> jComboBoxAutoAvoided;
	private JComboBox<String> jComboBoxOffsetY;
	private JComboBox<String> jComboBoxOffsetX;
	private JComboBox<String> jComboBoxOffsetUnit;
	private JComboBox<String> jComboBoxBackgroundShape;

	private JCheckBox jCheckBoxRepeatedLabelAvoided = new JCheckBox();
	private JCheckBox jCheckBoxFlowEnabled = new JCheckBox();
	private JCheckBox jCheckBoxShowSmallOLabel = new JCheckBox();
	private JCheckBox jCheckBoxAutoAvoided = new JCheckBox();
	private JButton jButtonBackgroundStyle = new JButton();
	private JCheckBox jCheckBoxLeaderLineDisplayed = new JCheckBox();
	private JButton jButtonLeaderLineDisplayed = new JButton();
	private JLabel jLabelBackgroundStyle = new JLabel();

	private transient Dataset dataset;
	private transient ThemeLabel currentTheme = new ThemeLabel();
	private transient Map<LabelBackShape, Integer> groundShapeMap;
	private JLabel jLabelBackgroundShape = new JLabel();

	public ThemeLabelAttributePanel(SymbolPreViewPanel previewPanel, Theme themeLabel) {
		super();
		setLayout(null);
		this.setBounds(5, 40, 545, 325);
		this.jPanelLayerView = previewPanel;
		this.dataset = previewPanel.getDataset();
		groundShapeMap = new HashMap<LabelBackShape, Integer>();
		groundShapeMap.put(LabelBackShape.NONE, 0);
		groundShapeMap.put(LabelBackShape.RECT, 1);
		groundShapeMap.put(LabelBackShape.ROUNDRECT, 2);
		groundShapeMap.put(LabelBackShape.ELLIPSE, 3);
		groundShapeMap.put(LabelBackShape.DIAMOND, 4);
		groundShapeMap.put(LabelBackShape.TRIANGLE, 5);
		groundShapeMap.put(LabelBackShape.MARKER, 6);
		initResources();
		getBackgroundSettingPanel();
		getLayyerViewPanel();
		getEffectSettingPanel();
		getLabelOffsetPanel();
		setTheme(themeLabel);
	}

	private void initResources() {
		jLabelOffsetUnit.setText(MapViewProperties.getString("String_LabelOffsetUnit"));
		jLabelOffsetX.setText(MapViewProperties.getString("String_LabelOffsetX"));
		jLabelOffsetY.setText(MapViewProperties.getString("String_LabelOffsetY"));
		jCheckBoxRepeatedLabelAvoided.setText(MapViewProperties.getString("String_RemoveRepeat"));
		jCheckBoxFlowEnabled.setText(MapViewProperties.getString("String_CheckBox_ShowFlow"));
		jCheckBoxShowSmallOLabel.setText(MapViewProperties.getString("String_SmallGeometry"));
		jCheckBoxAutoAvoided.setText(MapViewProperties.getString("String_CheckBox_AutoAvoid"));
		jCheckBoxLeaderLineDisplayed.setText(MapViewProperties.getString("String_ShowLeaderLine"));
		jButtonLeaderLineDisplayed.setText(MapViewProperties.getString("String_ThemeCustom_LineSymbolID"));
		jComboBoxOffsetUnit = new JComboBox<String>(new String[] { MapViewProperties.getString("String_MapBorderLineStyle_LabelDistanceUnit"),
				MapViewProperties.getString("String_ThemeLabelOffsetUnit_Map") });
		jComboBoxAutoAvoided = new JComboBox<String>(new String[] { MapViewProperties.getString("String_TwoDirectionsAvoided"),
				MapViewProperties.getString("String_AllDirectionsAvoided") });
		jComboBoxBackgroundShape = new JComboBox<String>(new String[] { MapViewProperties.getString("String_ColorTable_Default"),
				MapViewProperties.getString("String_ThemeLabelBackShape_Rect"), MapViewProperties.getString("String_ThemeLabelBackShape_BoundRect"),
				MapViewProperties.getString("String_ThemeLabelBackShape_Ellipse"), MapViewProperties.getString("String_ThemeLabelBackShape_Diamond"),
				MapViewProperties.getString("String_ThemeLabelBackShape_Triangle"), MapViewProperties.getString("String_ThemeLabelBackShape_Marker") });
		jLabelBackgroundShape.setText(MapViewProperties.getString("String_BackShape"));
		jLabelBackgroundStyle.setText(MapViewProperties.getString("String_BackStyle"));
		jLabelNumericPrecision.setText(MapViewProperties.getString("String_Precision"));
	}

	public static String getPanelName() {
		return MapViewProperties.getString("String_PanelTitle_AttributeSet");
	}

	private SymbolPreViewPanel getLayyerViewPanel() {
		jPanelLayerView.setBounds(0, 127, 250, 200);
		add(jPanelLayerView);
		return jPanelLayerView;
	}

	private JPanel getLabelOffsetPanel() {
		if (jPanelLabelOffset == null) {
			jPanelLabelOffset = new JPanel();
			jPanelLabelOffset.setLayout(null);
			jPanelLabelOffset.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY, 1, false), MapViewProperties.getString("String_LabelOffset"),
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
			jPanelLabelOffset.setBounds(250, 10, 275, 118);

			jLabelOffsetUnit.setHorizontalAlignment(SwingConstants.CENTER);
			jLabelOffsetUnit.setBounds(10, 26, 91, 18);
			jPanelLabelOffset.add(jLabelOffsetUnit);

			jLabelOffsetX.setHorizontalAlignment(SwingConstants.CENTER);
			jLabelOffsetX.setBounds(10, 57, 91, 18);
			jPanelLabelOffset.add(jLabelOffsetX);

			jLabelOffsetY.setHorizontalAlignment(SwingConstants.CENTER);
			jLabelOffsetY.setBounds(10, 87, 91, 18);
			jPanelLabelOffset.add(jLabelOffsetY);

			final JLabel offsetXLabel = new JLabel();
			offsetXLabel.setBounds(219, 57, 39, 18);
			jPanelLabelOffset.add(offsetXLabel);
			offsetXLabel.setHorizontalAlignment(SwingConstants.LEFT);
			offsetXLabel.setHorizontalTextPosition(SwingConstants.LEFT);
			offsetXLabel.setFont(new Font("0.1mm", Font.ITALIC, 12));
			offsetXLabel.setText("0.1mm");

			final JLabel offsetYLabel = new JLabel();
			offsetYLabel.setBounds(219, 87, 39, 18);
			jPanelLabelOffset.add(offsetYLabel);
			offsetYLabel.setHorizontalAlignment(SwingConstants.LEFT);
			offsetYLabel.setFont(new Font("0.1mm", Font.ITALIC, 12));
			offsetYLabel.setText("0.1mm");

			jComboBoxOffsetUnit.setBounds(105, 25, 108, 20);
			jComboBoxOffsetUnit.setSelectedIndex(1);
			// 初始化
			jComboBoxOffsetUnit.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					int index = jComboBoxOffsetUnit.getSelectedIndex();
					boolean isOffsetFixedValue = index == 0 ? true : false;
					currentTheme.setOffsetFixed(isOffsetFixedValue);
					if (isOffsetFixedValue == true) {
						offsetXLabel.setText("0.1mm");
						offsetYLabel.setText("0.1mm");
					} else {
						offsetXLabel.setText("m");
						offsetYLabel.setText("m");
					}
				}
			});
			jPanelLabelOffset.add(jComboBoxOffsetUnit);

			jComboBoxOffsetX = new JComboBox<String>();
			jComboBoxOffsetX.setBounds(105, 55, 108, 20);
			// 初始化
			int count = ((DatasetVector) dataset).getFieldInfos().getCount();
			for (int i = 0; i < count; i++) {
				FieldInfo fieldInfo = ((DatasetVector) dataset).getFieldInfos().get(i);
				if (fieldInfo.getType() != FieldType.TEXT) {
					jComboBoxOffsetX.addItem(fieldInfo.getName());
				}
			}

			jComboBoxOffsetX.setEditable(true);
			final JTextField textField = (JTextField) jComboBoxOffsetX.getEditor().getEditorComponent();
			textField.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				@Override
				public void changedUpdate(DocumentEvent e) {
					// nothing
				}

				@Override
				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						for (int i = 0; i < jComboBoxOffsetX.getModel().getSize(); i++) {
							if (textField.getText().equals(jComboBoxOffsetX.getItemAt(i))) {
								currentTheme.setOffsetX(jComboBoxOffsetX.getItemAt(i));
								jPanelLayerView.refreshPreViewMapControl(currentTheme);
								return;
							}
						}
						value = Integer.valueOf(textField.getText());
						isException = false;
						// 处理各种情况
						if (value < 0 || (textField.getText().indexOf("0") == 0 && textField.getText().length() > 1 || textField.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = textField.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								textField.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setOffsetX(record);
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}

				@Override
				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(textField.getText())) {
							return;
						}
						try {
							Integer.valueOf(textField.getText());
						} catch (Exception exp) {
							SwingUtilities.invokeLater(new Runnable() {

								@Override
								public void run() {
									textField.setText(record);
								}
							});
							return;
						}

						if (!textField.getText().equals(record)) {
							record = textField.getText();
						}
						currentTheme.setOffsetX(record);
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});

			textField.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					if ("".equals(textField.getText().trim()) || (textField.getText().indexOf("0") == 0 && textField.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								((JTextField) jComboBoxOffsetX.getEditor().getEditorComponent()).setText("0");
							}
						});
					}
				}
			});

			jComboBoxOffsetX.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					if (ItemEvent.DESELECTED != e.getStateChange()) {
						currentTheme.setOffsetX(jComboBoxOffsetX.getSelectedItem().toString());
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jPanelLabelOffset.add(jComboBoxOffsetX);

			jComboBoxOffsetY = new JComboBox();
			jComboBoxOffsetY.setBounds(105, 85, 108, 20);
			jComboBoxOffsetY.setEditable(true);
			// 初始化
			if ("".equals(currentTheme.getOffsetY())) {
				jComboBoxOffsetY.setSelectedItem("0");
			} else {
				jComboBoxOffsetY.setSelectedItem(currentTheme.getOffsetY());
			}
			count = ((DatasetVector) dataset).getFieldInfos().getCount();
			for (int i = 0; i < count; i++) {
				FieldInfo fieldInfo = ((DatasetVector) dataset).getFieldInfos().get(i);
				if (fieldInfo.getType() != FieldType.TEXT) {
					jComboBoxOffsetY.addItem(fieldInfo.getName());
				}
			}

			final JTextField textFieldY = (JTextField) jComboBoxOffsetY.getEditor().getEditorComponent();
			textFieldY.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				@Override
				public void changedUpdate(DocumentEvent e) {
					// nothing
				}

				@Override
				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						for (int i = 0; i < jComboBoxOffsetY.getModel().getSize(); i++) {
							if (textFieldY.getText().equals(jComboBoxOffsetY.getItemAt(i))) {
								currentTheme.setOffsetX(jComboBoxOffsetY.getItemAt(i));
								jPanelLayerView.refreshPreViewMapControl(currentTheme);
								return;
							}
						}
						value = Integer.valueOf(textFieldY.getText());
						isException = false;
						// 处理各种情况
						if (value < 0
								|| (textFieldY.getText().indexOf("0") == 0 && textFieldY.getText().length() > 1 || textFieldY.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = textFieldY.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								textFieldY.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setOffsetY(record);
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}

				@Override
				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(textFieldY.getText())) {
							return;
						}
						try {
							Integer.valueOf(textFieldY.getText());
						} catch (Exception exp) {
							SwingUtilities.invokeLater(new Runnable() {

								@Override
								public void run() {
									textFieldY.setText(record);
								}
							});
							return;
						}
						if (!textFieldY.getText().equals(record)) {
							record = textFieldY.getText();
						}
						currentTheme.setOffsetY(record);
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});

			textFieldY.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					if ("".equals(textFieldY.getText().trim()) || (textFieldY.getText().indexOf("0") == 0 && textFieldY.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								((JTextField) jComboBoxOffsetY.getEditor().getEditorComponent()).setText("0");
							}
						});
					}
				}
			});

			jComboBoxOffsetY.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					currentTheme.setOffsetY(jComboBoxOffsetY.getSelectedItem().toString());
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelLabelOffset.add(jComboBoxOffsetY);
			add(jPanelLabelOffset);
		}
		return jPanelLabelOffset;
	}

	private JPanel getEffectSettingPanel() {
		if (jPanelEffectSetting == null) {
			jPanelEffectSetting = new JPanel();
			jPanelEffectSetting.setLayout(null);
			jPanelEffectSetting.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY, 1, false), MapViewProperties.getString("String_EffectOption"),
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
			jPanelEffectSetting.setBounds(250, 127, 275, 200);

			// checkBox
			if (dataset.getType().equals(DatasetType.POINT)) {
				jCheckBoxRepeatedLabelAvoided.setEnabled(false);
			}
			jCheckBoxRepeatedLabelAvoided.setBounds(15, 25, 110, 26);
			// 初始化
			jCheckBoxRepeatedLabelAvoided.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					currentTheme.setRepeatedLabelAvoided(jCheckBoxRepeatedLabelAvoided.isSelected());
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelEffectSetting.add(jCheckBoxRepeatedLabelAvoided);

			// checkBox
			if (dataset.getType().equals(DatasetType.POINT)) {
				jCheckBoxFlowEnabled.setEnabled(false);
			}
			jCheckBoxFlowEnabled.setBounds(15, 55, 77, 26);
			// 初始化
			jCheckBoxFlowEnabled.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					currentTheme.setFlowEnabled(jCheckBoxFlowEnabled.isSelected());
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelEffectSetting.add(jCheckBoxFlowEnabled);

			// checkBox
			jCheckBoxShowSmallOLabel.setBounds(115, 55, 118, 26);
			// 初始化
			jCheckBoxShowSmallOLabel.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					currentTheme.setSmallGeometryLabeled(jCheckBoxShowSmallOLabel.isSelected());
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelEffectSetting.add(jCheckBoxShowSmallOLabel);

			// checkBox
			jCheckBoxAutoAvoided.setBounds(15, 85, 77, 26);
			// 初始化
			jCheckBoxAutoAvoided.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					jComboBoxAutoAvoided.setEnabled(jCheckBoxAutoAvoided.isSelected());
					currentTheme.setOverlapAvoided(jCheckBoxAutoAvoided.isSelected());
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelEffectSetting.add(jCheckBoxAutoAvoided);

			// checkBox
			jComboBoxAutoAvoided.setBounds(120, 90, 128, 20);
			jComboBoxAutoAvoided.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						if (MapViewProperties.getString("String_AllDirectionsAvoided").equals(jComboBoxAutoAvoided.getSelectedItem())) {
							currentTheme.setAllDirectionsOverlapedAvoided(true);
						} else {
							currentTheme.setAllDirectionsOverlapedAvoided(false);
						}
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jPanelEffectSetting.add(jComboBoxAutoAvoided);

			// checkBox
			jCheckBoxLeaderLineDisplayed.setBounds(15, 115, 91, 26);
			// 初始化
			jCheckBoxLeaderLineDisplayed.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					boolean isLeaderLineDisplayedValue = jCheckBoxLeaderLineDisplayed.isSelected();
					jButtonLeaderLineDisplayed.setEnabled(isLeaderLineDisplayedValue);
					currentTheme.setLeaderLineDisplayed(isLeaderLineDisplayedValue);
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelEffectSetting.add(jCheckBoxLeaderLineDisplayed);

			// button
			jButtonLeaderLineDisplayed.setEnabled(false);
			jButtonLeaderLineDisplayed.setBounds(120, 119, 128, 18);
			jButtonLeaderLineDisplayed.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Resources resources = dataset.getDatasource().getWorkspace().getResources();
					SymbolDialog dialog = new SymbolDialog();
					GeoStyle geoStyle = new GeoStyle();
					DialogResult dialogResult = dialog.showDialog(resources, geoStyle, SymbolType.LINE);
					if (dialogResult.equals(DialogResult.OK)) {
						currentTheme.setLeaderLineStyle(dialog.getStyle());
					}
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelEffectSetting.add(jButtonLeaderLineDisplayed);

			// checkBox
			jComboBoxNumericPrecision = new JComboBox<String>();
			jComboBoxNumericPrecision.setBounds(120, 150, 130, 18);
			// 初始化
			jComboBoxNumericPrecision.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						currentTheme.setNumericPrecision(jComboBoxNumericPrecision.getSelectedIndex() - 1);
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jComboBoxNumericPrecision.addItem("");
			jComboBoxNumericPrecision.addItem("1");
			jComboBoxNumericPrecision.addItem("0.1");
			jComboBoxNumericPrecision.addItem("0.01");
			jComboBoxNumericPrecision.addItem("0.001");
			jComboBoxNumericPrecision.addItem("0.0001");
			jComboBoxNumericPrecision.addItem("0.00001");
			jComboBoxNumericPrecision.addItem("0.000001");
			jComboBoxNumericPrecision.addItem("0.0000001");
			jComboBoxNumericPrecision.addItem("0.00000001");

			jPanelEffectSetting.add(jComboBoxNumericPrecision);

			jLabelNumericPrecision.setBounds(20, 150, 91, 18);
			jPanelEffectSetting.add(jLabelNumericPrecision);

			add(jPanelEffectSetting);
		}
		return jPanelEffectSetting;
	}

	private JPanel getBackgroundSettingPanel() {
		if (jPanelBackgroundSetting == null) {
			jPanelBackgroundSetting = new JPanel();
			jPanelBackgroundSetting.setLayout(null);
			jPanelBackgroundSetting.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY, 1, false), MapViewProperties
					.getString("String_BackShapeSetting"), TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
			jPanelBackgroundSetting.setBounds(0, 10, 250, 118);
			add(jPanelBackgroundSetting);

			jLabelBackgroundShape.setBounds(10, 40, 66, 18);
			jPanelBackgroundSetting.add(jLabelBackgroundShape);

			jLabelBackgroundStyle.setBounds(10, 80, 55, 18);
			jPanelBackgroundSetting.add(jLabelBackgroundStyle);

			jComboBoxBackgroundShape.setBounds(75, 40, 140, 20);
			// 初始化comboBox

			jComboBoxBackgroundShape.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					LabelBackShape labelBackShapeValue = LabelBackShape.NONE;
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						int index = jComboBoxBackgroundShape.getSelectedIndex();
						if (index == 0) {
							labelBackShapeValue = LabelBackShape.NONE;
						} else if (index == 1) {
							labelBackShapeValue = LabelBackShape.RECT;
						} else if (index == 2) {
							labelBackShapeValue = LabelBackShape.ROUNDRECT;
						} else if (index == 3) {
							labelBackShapeValue = LabelBackShape.ELLIPSE;
						} else if (index == 4) {
							labelBackShapeValue = LabelBackShape.DIAMOND;
						} else if (index == 5) {
							labelBackShapeValue = LabelBackShape.TRIANGLE;
						} else if (index == 6) {
							labelBackShapeValue = LabelBackShape.MARKER;
						}
						if (index == 0) {
							jButtonBackgroundStyle.setEnabled(false);
						} else {
							jButtonBackgroundStyle.setEnabled(true);
						}
					}
					currentTheme.setBackShape(labelBackShapeValue);
					jPanelLayerView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelBackgroundSetting.add(jComboBoxBackgroundShape);

			jButtonBackgroundStyle.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// 判断是否为点符号
					Resources resources = dataset.getDatasource().getWorkspace().getResources();
					SymbolDialog dialog = new SymbolDialog();
					GeoStyle geoStyle = new GeoStyle();
					SymbolType type = (MapViewProperties.getString("String_ThemeLabelBackShape_Marker").equals(
							jComboBoxBackgroundShape.getSelectedItem().toString()) == true) ? SymbolType.MARKER : SymbolType.FILL;
					DialogResult dialogResult = dialog.showDialog(resources, geoStyle, type);
					if (dialogResult.equals(DialogResult.OK)) {
						currentTheme.setBackStyle(dialog.getStyle());
						jPanelLayerView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jButtonBackgroundStyle.setBounds(75, 80, 140, 18);
			jPanelBackgroundSetting.add(jButtonBackgroundStyle);
		}
		return jPanelBackgroundSetting;
	}

	/**
	 * 返回专题图
	 */

	@Override
	protected Theme getTheme() {
		return currentTheme;
	}

	/**
	 * 重新设置专题图
	 */

	@Override
	protected void setTheme(Theme theme) {
		currentTheme = (ThemeLabel) theme;
		int index = groundShapeMap.get(currentTheme.getBackShape());
		jComboBoxBackgroundShape.setSelectedIndex(index);

		jComboBoxOffsetUnit.setSelectedIndex(currentTheme.isOffsetFixed() ? 0 : 1);

		if ("".equals(currentTheme.getOffsetX())) {
			jComboBoxOffsetX.setSelectedItem("0");
		} else {
			jComboBoxOffsetX.setSelectedItem(currentTheme.getOffsetX());
		}

		if ("".equals(currentTheme.getOffsetY())) {
			jComboBoxOffsetY.setSelectedItem("0");
		} else {
			jComboBoxOffsetY.setSelectedItem(currentTheme.getOffsetY());
		}

		if (jComboBoxBackgroundShape.getSelectedIndex() == 0) {
			jButtonBackgroundStyle.setEnabled(false);
		}

		jCheckBoxRepeatedLabelAvoided.setSelected(currentTheme.isRepeatedLabelAvoided());
		jCheckBoxFlowEnabled.setSelected(currentTheme.isFlowEnabled());
		jCheckBoxShowSmallOLabel.setSelected(currentTheme.isSmallGeometryLabeled());
		jCheckBoxAutoAvoided.setSelected(currentTheme.isOverlapAvoided());

		jComboBoxAutoAvoided.setEnabled(jCheckBoxAutoAvoided.isSelected());
		if (currentTheme.isAllDirectionsOverlapedAvoided()) {
			jComboBoxAutoAvoided.setSelectedIndex(0);
		} else {
			jComboBoxAutoAvoided.setSelectedIndex(1);
		}
		jCheckBoxLeaderLineDisplayed.setSelected(currentTheme.isLeaderLineDisplayed());

		// 初始化数值文本精度
		jComboBoxNumericPrecision.setSelectedIndex(currentTheme.getNumericPrecision() + 1);
		if (isTextTypeOfLabelExpression(currentTheme.getLabelExpression())) {
			jComboBoxNumericPrecision.setEnabled(false);
			jLabelNumericPrecision.setEnabled(false);
		} else {
			jComboBoxNumericPrecision.setEnabled(true);
			jLabelNumericPrecision.setEnabled(true);
		}
	}

	/**
	 * 判断标签表达式的字段信息类型是否是文本的，如果是返回true
	 * 
	 * @param labelExpression
	 *            theme中的标签表达式
	 * @return
	 */
	private boolean isTextTypeOfLabelExpression(String labelExpression) {
		boolean isTextType = false;
		int count = ((DatasetVector) dataset).getFieldInfos().getCount();
		for (int i = 0; i < count; i++) {
			FieldInfo fieldInfo = ((DatasetVector) dataset).getFieldInfos().get(i);
			String labelExpressionValue = fieldInfo.getName();
			if (labelExpression.equals(labelExpressionValue) && FieldType.TEXT.equals(fieldInfo.getType())) {
				isTextType = true;
			}
		}
		return isTextType;
	}

}
