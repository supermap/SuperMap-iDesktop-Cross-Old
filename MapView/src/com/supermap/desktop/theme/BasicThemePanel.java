package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.omg.CORBA.PRIVATE_MEMBER;

import com.supermap.data.ColorGradientType;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetVector;
import com.supermap.data.FieldInfo;
import com.supermap.data.FieldType;
import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.desktop.ui.controls.SymbolPreViewPanel;
import com.supermap.mapping.Layer;
import com.supermap.mapping.RangeMode;
import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeLabel;
import com.supermap.mapping.ThemeRange;
import com.supermap.mapping.ThemeType;
import com.supermap.mapping.ThemeUnique;
import com.supermap.ui.MapControl;

/**
 * @author WDY
 */
class BasicThemePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	// kinds of buttons
	private JButton buttonExput;
	private JButton buttonInput;
	private JButton buttonOk;
	private JButton buttonCancel;
	private JButton buttonApply;

	// three panels
	private JPanel panelCenter;
	private JPanel panelSail;
	private JPanel panelDown;

	private transient ThemeType themeType;
	private int labelStyle;
	private transient ThemeInfo themeInfo;

	// 导航条相关信息
	private String[] panelNames;

	// 导航条内的四个label
	private JLabel labelStartGuidePage;
	private JLabel labelBasicPanel;
	private JLabel labelAttributePanel;
	private JLabel labelAdvancedPanel;

	// 当前专题图面板数
	private List<JPanel> panelsList;
	private transient Map<ThemeType, Class<?>> map;
	private transient Map<Integer, Class<?>> subMap;
	private static Theme CURRENT_THEME;
	private SymbolPreViewPanel panelPreview;
	private int indexOfPanels;
	private ThemeMainPanel panelMain;
	private transient DatasetVector dataset;

	// ------------导航面板所需变量
	private ThemeGuide themeGuider;
	private ArrayList<JLabel> themeGuideLabels = new ArrayList<JLabel>();
	private transient Layer layer;
	private boolean mapHasChanged = false;
	// 导航条上未被点击时显示的图片
	ImageIcon[] image;

	// 导航条上被点击后显示的图片
	ImageIcon[] dimImage;
	private CreateThemeContainer container;

	static void setCurrentTheme(Theme currentTheme) {
		CURRENT_THEME = currentTheme;
	}

	/**
	 * 初始化面板用以修改专题图
	 * 
	 * @param layer
	 * @param themeGuide
	 */
	BasicThemePanel(Layer layer, ThemeGuide themeGuide, CreateThemeContainer container) {
		super();
		this.container = container;
		CURRENT_THEME = layer.getTheme();
		this.layer = layer;
		CURRENT_THEME.fromXML(layer.getTheme().toXML());
		this.themeType = CURRENT_THEME.getType();
		this.themeGuider = themeGuide;
		this.dataset = (DatasetVector) layer.getDataset();
		initional();
	}

	/**
	 * 初始化面板用以制作专题图
	 * 
	 * @param themeType
	 * @param labelStyle
	 * @param dataset
	 * @param mainPanel
	 */
	public BasicThemePanel(ThemeType themeType, int labelStyle, DatasetVector dataset, ThemeMainPanel mainPanel, CreateThemeContainer container) {
		super();
		setPreferredSize(new Dimension(530, 410));
		this.setOpaque(false);
		setLayout(null);

		// 保存传进来的专题图类型
		this.themeType = themeType;
		this.labelStyle = labelStyle;
		this.panelMain = mainPanel;
		this.themeGuider = mainPanel.getGuide();
		this.dataset = dataset;
		this.container = container;

		// map
		map = new HashMap<ThemeType, Class<?>>();
		subMap = new HashMap<Integer, Class<?>>();
		// 添加各种专题图Class
		map.put(ThemeType.RANGE, ThemeRangeThemeInfo.class);
		map.put(ThemeType.UNIQUE, ThemeUniqueThemeInfo.class);
		subMap.put(new Integer(0), ThemeLabelUniformStyleThemeInfo.class);
		subMap.put(new Integer(1), ThemeLabelRangeStyleThemeInfo.class);
		panelsList = new ArrayList<JPanel>();
		initOrRefresh(themeType, labelStyle);

		CURRENT_THEME = getTheme();
		panelPreview = new SymbolPreViewPanel(dataset);
		panelPreview.refreshPreViewMapControl(CURRENT_THEME);

		// add upPanel to the specified panel
		add(getUpPanel());

		// add centerPanel to the specified panel
		add(getCenterPanel());

		// add downPanel to the specified panel
		add(getDownPanel());
	}

	/**
	 * 加载模板
	 * 
	 * @param layer
	 * @param theme
	 * @param mainPanel
	 */
	public BasicThemePanel(Layer layer, Theme theme, ThemeMainPanel mainPanel) {
		super();
		setPreferredSize(new Dimension(530, 410));
		this.setOpaque(false);
		setLayout(null);

		// 保存传进来的专题图类型
		this.themeType = theme.getType();
		this.labelStyle = getLableStyle((ThemeLabel) theme);
		this.panelMain = mainPanel;
		this.themeGuider = mainPanel.getGuide();
		this.dataset = (DatasetVector) layer.getDataset();

		// map
		map = new HashMap<ThemeType, Class<?>>();
		subMap = new HashMap<Integer, Class<?>>();

		// 添加各种专题图Class
		map.put(ThemeType.RANGE, ThemeRangeThemeInfo.class);
		map.put(ThemeType.UNIQUE, ThemeUniqueThemeInfo.class);

		subMap.put(new Integer(0), ThemeLabelUniformStyleThemeInfo.class);
		subMap.put(new Integer(1), ThemeLabelRangeStyleThemeInfo.class);
		panelsList = new ArrayList<JPanel>();
		initOrRefresh(themeType, labelStyle);
		CURRENT_THEME = theme;
		panelPreview = new SymbolPreViewPanel(dataset);
		panelPreview.refreshPreViewMapControl(CURRENT_THEME);

		// add upPanel to the specified panel
		add(getUpPanel());

		// add centerPanel to the specified panel
		add(getCenterPanel());

		// add downPanel to the specified panel
		add(getDownPanel());
	}

	/**
	 * 修改专题图的初始化方法
	 */
	private void initional() {
		setPreferredSize(new Dimension(530, 410));
		this.setOpaque(false);
		setLayout(null);
		if (themeType.equals(ThemeType.LABEL)) {
			ThemeLabel themeLabel = (ThemeLabel) CURRENT_THEME;
			getLableStyle(themeLabel);
		}
		map = new HashMap<ThemeType, Class<?>>();
		subMap = new HashMap<Integer, Class<?>>();

		// 添加各种专题图Class
		map.put(ThemeType.RANGE, ThemeRangeThemeInfo.class);
		map.put(ThemeType.UNIQUE, ThemeUniqueThemeInfo.class);

		subMap.put(new Integer(0), ThemeLabelUniformStyleThemeInfo.class);
		subMap.put(new Integer(1), ThemeLabelRangeStyleThemeInfo.class);
		panelsList = new ArrayList<JPanel>();

		panelPreview = new SymbolPreViewPanel(dataset);
		panelPreview.refreshPreViewMapControl(CURRENT_THEME);

		initOrRefresh(themeType, labelStyle);
		// add upPanel to the specified panel
		add(getUpPanel());

		// add centerPanel to the specified panel
		add(getCenterPanel());

		// add downPanel to the specified panel
		add(getDownPanel());

	}

	private int getLableStyle(ThemeLabel themeLabel) {
		if (themeLabel.getLabels() != null) {
			labelStyle = 3;
		} else if (themeLabel.getUniformMixedSytle() != null) {
			labelStyle = 2;
		} else if (themeLabel.getCount() > 0) {
			labelStyle = 1;
			this.themeGuider.setTitle(ControlsProperties.getString("String_ParagraphMap"));
		} else {
			labelStyle = 0;
			this.themeGuider.setTitle(ControlsProperties.getString("String_UnityThemeMap"));
		}
		return labelStyle;
	}

	void disposeAll() {
		if (panelPreview != null) {
			panelPreview.getPreViewMapControl().dispose();
		}
	}

	private Theme getTheme() {
		if (themeType == ThemeType.LABEL) {
			String rangeExpression = "SmID";
			if (labelStyle == 0) {
				setCurrentTheme(new ThemeLabel());
			} else if (labelStyle == 1) {
				setCurrentTheme(ThemeLabel.makeDefault(dataset, rangeExpression, RangeMode.EQUALINTERVAL, 5, ColorGradientType.GREENRED));
			}
			int count = dataset.getFieldCount();
			for (int j = 0; j < count; j++) {
				FieldInfo fieldInfo = dataset.getFieldInfos().get(j);
				if (fieldInfo.getType() == FieldType.TEXT) {
					String labelExpression = fieldInfo.getName();
					((ThemeLabel) CURRENT_THEME).setLabelExpression(labelExpression);
				}
			}
		} else if (themeType == ThemeType.RANGE) {
			String rangeExpression = "SmID";
			setCurrentTheme(ThemeRange.makeDefault(dataset, rangeExpression, RangeMode.EQUALINTERVAL, 5, ColorGradientType.GREENRED));
		} else if (themeType == ThemeType.UNIQUE) {
			setCurrentTheme(ThemeUnique.makeDefault(dataset, "SmID", ColorGradientType.YELLOWGREEN));
		}
		return CURRENT_THEME;
	}

	private void initOrRefresh(ThemeType themeType, int labelStyle) {
		Class<?> type;
		try {
			if (ThemeType.LABEL.equals(themeType)) {
				type = subMap.get(labelStyle);
			} else {
				type = map.get(themeType);
			}
			if (type == null) {
				throw new IllegalStateException("Not Support This Theme Now!");
			} else {
				themeInfo = (ThemeInfo) type.newInstance();
			}
		} catch (InstantiationException e) {
			Application.getActiveApplication().getOutput().output(e);
		} catch (IllegalAccessException e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		// 初始导航条信息
		panelNames = themeInfo.getPanelNames();

		// reset m_themeType
		this.themeType = themeType;
		this.labelStyle = labelStyle;
	}

	private JPanel getUpPanel() {
		if (panelSail == null) {
			panelSail = new JPanel();
			panelSail.setLayout(new GridLayout(0, 4));
			panelSail.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, false));
			panelSail.setBounds(5, 0, 523, 34);
			initGuidePanel();
		}
		return panelSail;
	}

	/**
	 * 初始化导航条上的图片
	 */
	private void initImage() {
		image = new ImageIcon[3];
		image[0] = InternalImageIconFactory.THEMEGUIDE_BASICSETTING;
		image[1] = InternalImageIconFactory.THEMEGUIDE_ATTRIBUTESETTING;
		image[2] = InternalImageIconFactory.THEMEGUIDE_ADVANCEDSETTING;

		dimImage = new ImageIcon[3];
		dimImage[0] = InternalImageIconFactory.THEMEGUIDE_BASICSETTING_DIM;
		dimImage[1] = InternalImageIconFactory.THEMEGUIDE_ATTRIBUTESETTING_DIM;
		dimImage[2] = InternalImageIconFactory.THEMEGUIDE_ADVANCEDSETTING_DIM;
	}

	/**
	 * 初始化导航条
	 */
	private void initGuidePanel() {
		initImage();
		// mouserListener监听器
		MouseAdapter mouserAdapter = new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				BasicThemePanel.this.labelMouseClicked(e);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				((JLabel) e.getSource()).setCursor(Cursor.getPredefinedCursor(12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				((JLabel) e.getSource()).setCursor(Cursor.getPredefinedCursor(0));
			}
		};

		// 导航首页label
		labelStartGuidePage = new JLabel("<-");
		labelStartGuidePage.setName(ControlsProperties.getString("String_Label_Guide"));
		labelStartGuidePage.setHorizontalAlignment(SwingConstants.CENTER);
		ImageIcon imageIcon = InternalImageIconFactory.THEMEGUIDE_STARTGUIDEPAGE;
		labelStartGuidePage.setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
		labelStartGuidePage.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				JPanel parent = (JPanel) BasicThemePanel.this.getParent();
				if (null != panelMain) {
					parent.removeAll();
					parent.add(panelMain);
					parent.revalidate();
					parent.repaint();
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				((JLabel) e.getSource()).setCursor(Cursor.getPredefinedCursor(12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				((JLabel) e.getSource()).setCursor(Cursor.getPredefinedCursor(0));
			}
		});
		panelSail.add(labelStartGuidePage);
		for (int i = 0; i < panelNames.length; i++) {
			if (panelNames[i].equals(MapViewProperties.getString("String_PanelTitle_BasicSet"))) {
				// 基本面板label
				labelBasicPanel = new JLabel();
				labelBasicPanel.setName(panelNames[i]);
				labelBasicPanel.setHorizontalAlignment(SwingConstants.CENTER);
				imageIcon = InternalImageIconFactory.THEMEGUIDE_BASICSETTING_DIM;
				labelBasicPanel.setIcon(InternalImageIconFactory.THEMEGUIDE_BASICSETTING_DIM);
				labelBasicPanel.setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
				labelBasicPanel.addMouseListener(mouserAdapter);
				panelSail.add(labelBasicPanel);
				themeGuideLabels.add(labelBasicPanel);
			} else if (panelNames[i].equals(MapViewProperties.getString("String_PanelTitle_AttributeSet"))) {
				// 属性面板label
				labelAttributePanel = new JLabel();
				labelAttributePanel.setName(panelNames[i]);
				labelAttributePanel.setHorizontalAlignment(SwingConstants.CENTER);
				imageIcon = InternalImageIconFactory.THEMEGUIDE_ATTRIBUTESETTING;
				labelAttributePanel.setIcon(InternalImageIconFactory.THEMEGUIDE_ATTRIBUTESETTING);
				labelAttributePanel.setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
				labelAttributePanel.addMouseListener(mouserAdapter);
				panelSail.add(labelAttributePanel);
				themeGuideLabels.add(labelAttributePanel);
			} else if (panelNames[i].equals(MapViewProperties.getString("String_PanelTitle_AdvancedSet"))) {
				// 高级面板label
				labelAdvancedPanel = new JLabel();
				labelAdvancedPanel.setName(panelNames[i]);
				labelAdvancedPanel.setHorizontalAlignment(SwingConstants.CENTER);
				imageIcon = InternalImageIconFactory.THEMEGUIDE_ADVANCEDSETTING;
				labelAdvancedPanel.setIcon(InternalImageIconFactory.THEMEGUIDE_ADVANCEDSETTING);
				labelAdvancedPanel.setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
				labelAdvancedPanel.addMouseListener(mouserAdapter);
				panelSail.add(labelAdvancedPanel);
				themeGuideLabels.add(labelAdvancedPanel);
			}
		}
	}

	/**
	 * 当导航条上图片被点击时，mouseClicked所调用的方法
	 * 
	 * @param e
	 *            mouseClicked传进来的事件
	 */
	private void labelMouseClicked(MouseEvent e) {
		// 标题不对啊
		String name = ((JLabel) e.getSource()).getName();
		int index = 0;
		for (; index < panelNames.length; index++) {
			if (panelNames[index].equals(name)) {
				break;
			}
		}
		int panelIndex = 0;
		for (; panelIndex < panelsList.size(); panelIndex++) {
			if (panelsList.get(panelIndex).equals(panelCenter)) {
				break;
			}
		}
		if (panelIndex == index) {
			return;
		}
		JPanel parent = (JPanel) panelCenter.getParent();
		parent.remove(panelCenter);

		// 当前点击的JLabel的图片变暗
		JLabel label = themeGuideLabels.get(index);
		label.setIcon(dimImage[index]);

		// 原来选中的图片高亮
		label = themeGuideLabels.get(indexOfPanels);
		label.setIcon(image[indexOfPanels]);

		indexOfPanels = index;
		panelCenter = panelsList.get(index);
		if (themeType != ThemeType.UNIQUE) {
			panelCenter.add(panelPreview);
		}
		if (panelCenter instanceof ThemeLabelAttributePanel) {
			((ThemeLabelAttributePanel) panelCenter).setTheme(CURRENT_THEME);
		}
		parent.add(panelCenter);
		parent.revalidate();
		parent.repaint();
	}

	private JPanel getCenterPanel() {
		if (panelCenter == null) {
			try {
				List<Class<?>> panels = themeInfo.getPanels();
				JPanel panel = null;
				for (int i = 0; i < panels.size(); i++) {
					panel = (JPanel) panels.get(i).getConstructor(new Class[] { SymbolPreViewPanel.class, Theme.class })
							.newInstance(new Object[] { panelPreview, CURRENT_THEME });
					((ThemeChangePanel) panel).addThemeChangedListener(new ThemeChangedListener() {

						@Override
						public void themeHandleChanged(ThemeChangedEvent event) {
							// TODO
						}
					});
					panelsList.add(panel);
				}
				indexOfPanels = 0;
				panelCenter = panelsList.get(0);
				if (themeType != ThemeType.UNIQUE) {
					panelCenter.add(panelPreview);
				}
			} catch (Exception e) {
				Application.getActiveApplication().getOutput().output(e);
			}
			panelCenter.setBounds(3, 45, 525, 325);
		}
		return panelCenter;
	}

	private JPanel getDownPanel() {
		if (panelDown == null) {
			panelDown = new JPanel();
			panelDown.setLayout(null);
			panelDown.setBounds(3, 370, 525, 30);

			// add buttons to m_downPanel
			buttonOk = new JButton();
			buttonOk.setMargin(new Insets(0, 0, 0, 0));
			buttonOk.setText(CommonProperties.getString("String_Button_OK"));
			buttonOk.setBounds(445, 5, 70, 18);
			panelDown.add(buttonOk);
			buttonOk.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					themeGuider.setTheme(CURRENT_THEME);
					if (false == mapHasChanged) {
						refreshLayer();
						themeGuider.setResult(DialogResult.OK);
						container.dispose();
					} else {
						themeGuider.setResult(DialogResult.OK);
						container.dispose();
					}
				}
			});

			buttonCancel = new JButton();
			buttonCancel.setMargin(new Insets(0, 0, 0, 0));
			buttonCancel.setText(CommonProperties.getString("String_Button_Cancel"));
			buttonCancel.setBounds(360, 5, 70, 18);
			buttonCancel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					themeGuider.setTheme(CURRENT_THEME);
					themeGuider.setResult(DialogResult.CANCEL);
					container.dispose();
					UICommonToolkit.getLayersManager().getLayersTree().reload();
				}

			});
			panelDown.add(buttonCancel);

			buttonApply = new JButton();
			buttonApply.setMargin(new Insets(0, 0, 0, 0));
			buttonApply.setText(CommonProperties.getString("String_Button_Apply"));
			buttonApply.setBounds(275, 5, 70, 18);
			buttonApply.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					themeGuider.setTheme(CURRENT_THEME);
					refreshLayer();
				}
			});
			panelDown.add(buttonApply);

		}
		return panelDown;
	}

	protected void refreshLayer() {
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		MapControl mapControl = formMap.getMapControl();
		if (null != layer) {
			// 图层管理器刷新
			Dataset targetDataset = layer.getDataset();
			mapControl.getMap().getLayers().remove(layer);
			mapControl.getMap().getLayers().add(targetDataset, panelPreview.getPreViewMapControlLayer().getTheme(), true);
			mapControl.getMap().refresh();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
			mapHasChanged = true;
		} else if (null == layer) {
			Layer tempLayer = themeGuider.getLayer();
			Dataset targetDataset = tempLayer.getDataset();
			mapControl.getMap().getLayers().add(targetDataset, panelPreview.getPreViewMapControlLayer().getTheme(), true);
			mapControl.getMap().refresh();
			mapHasChanged = true;
		}
	}

	/**
	 * 导出专题图
	 * 
	 * @return m_outputButton
	 */
	private JButton getOutputButton() {
		if (buttonExput == null) {
			buttonExput = new JButton();
			buttonExput.setMargin(new Insets(0, 0, 0, 0));
			buttonExput.setText(ControlsProperties.getString("String_ExportThemeMap"));
			buttonExput.setBounds(444, 0, 75, 16);
			buttonExput.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO: FileNameExtensionFilter 1.6 类型不能以源码的方式使用，故先注释起来
					JFileChooser saveTheme = new JFileChooser();
					FileNameExtensionFilter filter = new FileNameExtensionFilter("XML", "XML");
					saveTheme.setFileFilter(filter);
					int result = saveTheme.showSaveDialog(themeGuider);
					if (result == JFileChooser.APPROVE_OPTION) {
						String path = saveTheme.getSelectedFile().getPath();
						FileOutputStream fileOutStream = null;
						if (!path.endsWith(".XML")) {
							path = path + ".XML";
						}
						try {
							fileOutStream = new FileOutputStream(path);
							fileOutStream.write(CURRENT_THEME.toXML().getBytes());

						} catch (Exception e1) {
							Application.getActiveApplication().getOutput().output(e1);
						} finally {
							try {
								if (fileOutStream != null) {
									fileOutStream.close();
								}
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								Application.getActiveApplication().getOutput().output(e1);
							}
						}
					}
				}

			});
		}
		return buttonExput;
	}

	/**
	 * 导入专题图
	 * 
	 * @return m_outputButton
	 */
	private JButton getInputButton() {
		if (buttonInput == null) {
			buttonInput = new JButton();
			buttonInput.setMargin(new Insets(0, 0, 0, 0));
			buttonInput.setText(ControlsProperties.getString("String_ImportThemeMap"));
			buttonInput.setBounds(363, 0, 75, 16);
			buttonInput.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					// 1.6 类型不能以源码的方式使用，故先注释起来
					JFileChooser inputTheme = new JFileChooser();
					FileNameExtensionFilter filter = new FileNameExtensionFilter("XML", "XML");
					inputTheme.setFileFilter(filter);
					int result = inputTheme.showOpenDialog(themeGuider);
					if (result == JFileChooser.APPROVE_OPTION) {
						try {
							Reader reader = new BufferedReader(new FileReader(inputTheme.getSelectedFile()));
							BufferedReader bufferReader = new BufferedReader(reader);
							StringBuilder stringBuffer = new StringBuilder("");
							String string = "";
							while ((string = bufferReader.readLine()) != null) {
								stringBuffer = stringBuffer.append(string);
							}
							CURRENT_THEME.fromXML(stringBuffer.toString());
							((ThemeChangePanel) panelCenter).setTheme(CURRENT_THEME);

						} catch (IOException e1) {
							Application.getActiveApplication().getOutput().output(e1);
						}
					}
				}

			});
		}
		return buttonInput;
	}
}
