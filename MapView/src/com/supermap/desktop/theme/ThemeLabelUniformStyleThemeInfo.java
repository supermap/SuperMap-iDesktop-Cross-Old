package com.supermap.desktop.theme;

import java.util.ArrayList;
import java.util.List;

class ThemeLabelUniformStyleThemeInfo extends ThemeInfo {
	public ThemeLabelUniformStyleThemeInfo() {
		list = new ArrayList<Class<?>>();
		list.add(ThemeLabelUniformStylePanel.class);
		list.add(ThemeLabelAttributePanel.class);
		list.add(ThemeLabelAdvancedPanel.class);
	}
}
