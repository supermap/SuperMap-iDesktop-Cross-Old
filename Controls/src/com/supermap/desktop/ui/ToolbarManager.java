package com.supermap.desktop.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import com.supermap.desktop.Application;
import com.supermap.desktop.WorkEnvironment;
import com.supermap.desktop.Interface.IToolbar;
import com.supermap.desktop.Interface.IToolbar;
import com.supermap.desktop.Interface.IToolbarManager;
import com.supermap.desktop.enums.WindowType;
import com.supermap.desktop.implement.SmMenu;
import com.supermap.desktop.implement.SmToolbar;
import com.supermap.desktop.ui.XMLButton;
import com.supermap.desktop.ui.XMLComboBox;
import com.supermap.desktop.ui.XMLCommand;
import com.supermap.desktop.ui.XMLLabel;
import com.supermap.desktop.ui.XMLMenu;
import com.supermap.desktop.ui.XMLTextbox;
import com.supermap.desktop.ui.XMLToolbar;

public class ToolbarManager implements IToolbarManager {

	private JPanel toolbarsContainer = null;
	private ArrayList<IToolbar> listToolbars = null;
	private HashMap<WindowType, ArrayList<IToolbar>> childToolbars = null;
	private Container toolbarContainer;
	
	public ToolbarManager() {
		this.listToolbars = new ArrayList<IToolbar>();
		this.childToolbars = new HashMap<WindowType, ArrayList<IToolbar>>();
		this.toolbarsContainer = new JPanel();
	}

	public JPanel getToolbarsContainer() {
		return this.toolbarsContainer;
	}

	public IToolbar get(int index) {
		return this.listToolbars.get(index);
	}

	public IToolbar get(String id) {
		IToolbar item = null;
		for (int i = 0; i < this.listToolbars.size(); i++) {
			if (this.listToolbars.get(i).getID().equalsIgnoreCase(id)) {
				item = this.listToolbars.get(i);
				break;
			}
		}
		return item;
	}

	public int getCount() {
		return this.listToolbars.size();
	}

	public boolean contains(IToolbar item) {
		return this.listToolbars.contains(item);
	}

	public boolean contains(WindowType windowType, IToolbar item) {		
		ArrayList<IToolbar> childToolbars = this.childToolbars.get(windowType);
		return childToolbars.contains(item);
	}

	public IToolbar getChildToolbar(WindowType windowType, int index) {
		ArrayList<IToolbar> childToolbars = this.childToolbars.get(windowType);
		return childToolbars.get(index);
	}

	public IToolbar getChildToolbar(WindowType windowType, String key) {
		ArrayList<IToolbar> childToolbars = this.childToolbars.get(windowType);
		IToolbar item = null;
		for (int i = 0; i < childToolbars.size(); i++) {
			if (childToolbars.get(i).getID().equalsIgnoreCase(key)) {
				item = childToolbars.get(i);
				break;
			}
		}
		return item;
	}

	public int getChildToolbarCount(WindowType windowType) {
		ArrayList<IToolbar> childToolbars = this.childToolbars.get(windowType);
		return childToolbars.size();
	}
	
	public void add(IToolbar item) {
		this.listToolbars.add(item);
	}

	public void removeAt(int index) {
		this.listToolbars.remove(index);
	}

	public void removeAll() {
		this.listToolbars.clear();
	}
	
	public void setToolbarContainer(Container container) {
		this.toolbarContainer = container;
	}

	public boolean load(WorkEnvironment workEnvironment) {
		boolean result = false;
		try {
			// 查找有哪些 Toolbar
			ArrayList<XMLToolbar> xmlToolbars = workEnvironment.getPluginInfos().getToolbars().getToolbars();
			for (int i = 0; i < xmlToolbars.size(); i++) {
				XMLToolbar xmlToolbar = xmlToolbars.get(i);
				SmToolbar toolbar = new SmToolbar(xmlToolbar);
				
//				com.supermap.desktop.ui.controls.DropDownComponent dropDownComponent = new com.supermap.desktop.ui.controls.DropDownComponent();

				// 判断一下如果关联的ControlClass为空，就添加到主菜单列表中，否则关联到子窗口菜单列表
				if (xmlToolbar.getFormClassName().equals("")) {
					this.listToolbars.add(toolbar);
					this.toolbarContainer.add(toolbar, BorderLayout.NORTH);
				} else {					
					WindowType windowType = getWindowType(xmlToolbar.getFormClassName());
					ArrayList<IToolbar> childToolbars = null;
					if (this.childToolbars.containsKey(windowType)) {
						childToolbars = this.childToolbars.get(windowType);
					} else {
						childToolbars = new ArrayList<IToolbar>();
						this.childToolbars.put(windowType, childToolbars);
					}
					childToolbars.add(toolbar);
				}
			}
			System.out.println(result);
			result = true;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}

	public void saveChange() {
		if (this.toolbarsContainer != null && this.toolbarsContainer.getComponentCount() > 0) {
			for (int i = 0; i < this.toolbarsContainer.getComponentCount(); i++) {
				SmToolbar toolbar = (SmToolbar) this.toolbarsContainer.getComponent(i);
				if (toolbar != null) {
					// 更新 workEnvironment 中 XMLToolbar 的 index
					XMLToolbar xmlToolbar = toolbar.getXMLToolbar();
					xmlToolbar.setIndex(i);
				}
			}

			// 同步更新每一个 pluginInfo 所关联的 XMLToolbar 的 index，然后对每一个 pluginInfo 进行保存
			WorkEnvironment currentWorkEnvironment = Application.getActiveApplication().getWorkEnvironmentManager().getActiveWorkEnvironment();
			XMLToolbars totalXMLToolbars = currentWorkEnvironment.getPluginInfos().getToolbars();

			for (int i = 0; i < totalXMLToolbars.getToolbars().size(); i++) {
				XMLToolbar toolbar = totalXMLToolbars.getToolbars().get(i);

				String toolbarID = toolbar.getID();
				if (toolbarID != null && !toolbarID.isEmpty()) {
					for (int j = 0; j < currentWorkEnvironment.getPluginInfos().size(); j++) {

						// 每一个 PluginInfo 中，同一个 ID 的 Toolbar 节点只有一个
						XMLToolbar updateTo = currentWorkEnvironment.getPluginInfos().get(j).getToolbars().getToolbar(toolbarID);
						if (updateTo != null) {
							updateTo.setIndex(toolbar.getIndex());
							break;
						}
					}
				}
			}
		}
	}
	
	public boolean loadChildToolbar(WindowType windowType) {
		boolean result = false;
		try {
			ArrayList<IToolbar> childToolbars = this.childToolbars.get(windowType);
			if (childToolbars != null) {
				for (int i = 0; i < childToolbars.size(); i++) {			
					SmToolbar toolbar = (SmToolbar)childToolbars.get(i);
					this.toolbarContainer.add(toolbar, BorderLayout.NORTH);
					
					int count = this.toolbarContainer.getComponentCount();
					count +=1;
					int n = count;
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}
	
	public boolean removeChildToolbar(WindowType windowType) {
		boolean result = false;
		try {
//			ArrayList<IToolbar> childToolbars = this.childToolbars.get(windowType);
//			for (int i = 0; i < childToolbars.size(); i++) {
//				SmToolbar toolbar = (SmToolbar)childToolbars.get(i);
//				toolbar.setFloatable(false);
//				toolbar.setFloatable(true);
//			}			
			// 取得总工具条的数目，子工具条的数目=总数目-主工具条的数目，即this.listToolbars.size()
			int count = this.toolbarContainer.getComponentCount();
			for (int index = count-1; index >= this.listToolbars.size(); index--) {				
				this.toolbarContainer.remove(index);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return result;
	}
	
	private WindowType getWindowType(String controlCalss) {
		WindowType windowType = WindowType.UNKNOWN;
		if (controlCalss.equalsIgnoreCase("SuperMap.Desktop.FormMap")) {
			windowType = WindowType.MAP;
		} else if (controlCalss.equalsIgnoreCase("SuperMap.Desktop.FormScene")) {
			windowType = WindowType.SCENE;
		} else if (controlCalss.equalsIgnoreCase("SuperMap.Desktop.FormLayout")) {
			windowType = WindowType.LAYOUT;
		} else if (controlCalss.equalsIgnoreCase("SuperMap.Desktop.FormTabular")) {
			windowType = WindowType.TABULAR;
		}
		
		return windowType;
	}
}
