package com.supermap.desktop.ui;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;

import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IToolbar;
import com.supermap.desktop.enums.WindowType;
import com.supermap.desktop.implement.SmButtonDropdown;

public class IdleTimerTask extends TimerTask {

	@Override
	public void run() {
		try {
			// 刷新主工具条
			ToolbarManager toolbarManager = (ToolbarManager)Application.getActiveApplication().getMainFrame().getToolbarManager();
			for (int toolbarIndex = 0; toolbarIndex < toolbarManager.getCount(); toolbarIndex++) {
				IToolbar toolbar = toolbarManager.get(toolbarIndex);
				if (toolbar.isVisible()) {
					for (int itemIndex = 0; itemIndex < toolbar.getCount(); itemIndex++) {
						IBaseItem item = toolbar.getAt(itemIndex);
						if (item.getCtrlAction() != null) {
							item.getCtrlAction().setCaller(item);
//							if (item.getCtrlAction().getClass().getName().endsWith("CtrlActionWorkspaceOpenFile")) {
//								int n = 0; 
//								int m = n;
//							}
							if (item instanceof SmButtonDropdown) {
								if (item.getCtrlAction().getClass().getName().endsWith("CtrlActionWorkspaceOpenFile")) {
									int n = 0; 
									int m = n;
									if (item.getCtrlAction().check()) {
										int nn = 0; 
										int mn = n;
									}
								}
								
							}
							item.getCtrlAction().getCaller().setEnabled(item.getCtrlAction().enable());
							item.getCtrlAction().getCaller().setChecked(item.getCtrlAction().check());
//							((JComponent)item.getCtrlAction().getCaller()).repaint();
//							((JComponent)item.getCtrlAction().getCaller()).invalidate();
						}						
					}
				}
			}
			
			// 刷新子窗口工具条
			WindowType windowType = Application.getActiveApplication().getActiveForm().getWindowType();
			for (int toolbarIndex = 0; toolbarIndex < toolbarManager.getChildToolbarCount(windowType); toolbarIndex++) {
				IToolbar toolbar = toolbarManager.getChildToolbar(windowType, toolbarIndex);
				if (toolbar.isVisible()) {
					for (int itemIndex = 0; itemIndex < toolbar.getCount(); itemIndex++) {
						IBaseItem item = toolbar.getAt(itemIndex);
						if (item.getCtrlAction() != null) {
							item.getCtrlAction().setCaller(item);
							item.getCtrlAction().getCaller().setEnabled(item.getCtrlAction().enable());
							item.getCtrlAction().getCaller().setChecked(item.getCtrlAction().check());
//							((JComponent)item.getCtrlAction().getCaller()).repaint();
//							((JComponent)item.getCtrlAction().getCaller()).invalidate();
						}	
					}
				}
			}
		} catch (Exception ex) {
			
		} finally {
			// 执行完刷新后启动定时器，准备下一次刷新
	        new Timer().schedule(new IdleTimerTask(), 1000);  
		}
	}

}
