package com.supermap.desktop.ui.controls;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

//import com.supermap.data.DatasetRelationship;

/**
 * 栅格数据集节点装饰器
 * @author huangbiao
 *
 */
class DatasetRelationshipNodeDecorator implements TreeNodeDecorator {

	public void decorate(JLabel label, TreeNodeData data) {
		if(data.getType().equals(NodeDataType.DATASET_RELATION_SHIP)){
//			DatasetRelationship datasetRelationShip = (DatasetRelationship) data.getData();
//			label.setText(datasetRelationShip.getName());
			label.setText("");
			ImageIcon icon = (ImageIcon) label.getIcon();
			BufferedImage bufferedImage = new BufferedImage(IMAGEICON_WIDTH,
					IMAGEICON_HEIGHT, BufferedImage.TYPE_INT_ARGB);
			Graphics graphics = bufferedImage.getGraphics();
			graphics.drawImage(
					InternalImageIconFactory.DT_RELATIONSHIP.getImage(), 0, 0, label);
			icon.setImage(bufferedImage);
		}
	}

}
