package com.supermap.desktop.ui.controls;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.supermap.data.EngineType;
import com.supermap.desktop.Application;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.properties.CommonProperties;

public class JDialogDatasourceOpenAndNew extends SmDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Variables declaration
	private int preSelectedIndex = -1;
	// End of variables declaration      
	
//	private static boolean connFlag = true;
	// UI Variables declaration - do not modify       
	private final JPanel contentPanel = new JPanel();
	private javax.swing.JButton cancelButton;
	private javax.swing.JButton okButton;
	private java.awt.List listDatasourceType;
	private JPanelDatasourceInfoDatabase panelDatasourceInfoDatabase;
	private JPanelDatasourceInfoWeb panelDatasourceInfoWeb;
	private GroupLayout gl_contentPanel;
	private DatasourceOperatorType datasourceOperatorType;
	// UI End of variables declaration 
	
	/**
	 * Create the dialog.
	 * @param type 数据源类型
	 */
	public JDialogDatasourceOpenAndNew(JFrame owner, DatasourceOperatorType type) {
		super(owner);
		setBounds(100, 100, 575, 301);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		listDatasourceType = new java.awt.List();		
		Font defaultFont = this.contentPanel.getFont();
		Font font = new Font(defaultFont.getFontName(), defaultFont.getStyle(),
				(int) (defaultFont.getSize() * 1.4));
		listDatasourceType.setFont(font);
		listDatasourceType.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					listWorkspaceType_ItemSelectedChanged(e);
				}
			}
		});
		okButton = new JButton();
		okButton.setPreferredSize(new Dimension(75, 23));
		if(DatasourceOperatorType.OPENDATABASE == type){
			this.setTitle(ControlsProperties.getString("String_Title_OpenDatabaseDataSourse"));
			this.okButton.setText(CommonProperties.getString("String_Button_Open"));
		}else if(DatasourceOperatorType.NEWDATABASE == type){
			this.setTitle(ControlsProperties.getString("String_Title_NewDatabaseDataSourse"));
			this.okButton.setText(ControlsProperties.getString("String_Button_Creat"));
		}else if(DatasourceOperatorType.OPENWEB == type){
			this.setTitle(ControlsProperties.getString("String_Title_OpenWebDataSourse"));
			this.okButton.setText(CommonProperties.getString("String_Button_Open"));
		}
		this.initializeDatasourceType(type);
		datasourceOperatorType = type;
		
		JPanel panel = this.getPanel(0);
		
		gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(listDatasourceType, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(78)
					.addContainerGap(156, Short.MAX_VALUE))
				.addComponent(listDatasourceType, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
				.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
		);
		
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton();
				cancelButton.setText(CommonProperties.getString("String_Button_Cancel"));
				cancelButton.setPreferredSize(new Dimension(75, 23));
				buttonPane.add(cancelButton);
			}
		}
		okButton.addActionListener(okActionListener);
		cancelButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				CencelButtonActionPerformed(e);
			}
		});
	}
	
	/**
	 * 根据类型初始化面板
	 * @param type 类型（NEWDATABASE/OPENDATABASE/OPENWEB）
	 */
	private void initializeDatasourceType(DatasourceOperatorType type) {
		try {
			listDatasourceType.removeAll();
			
			switch (type) {
			case NEWDATABASE:
				listDatasourceType.add(ControlsProperties.getString("String_SQL"));
				listDatasourceType.add(ControlsProperties.getString("String_Oracle"));
				listDatasourceType.add(ControlsProperties.getString("String_OracleSpatial"));
				listDatasourceType.add(ControlsProperties.getString("String_PostgreSQL"));
				listDatasourceType.add(ControlsProperties.getString("String_DB2"));
				listDatasourceType.add(ControlsProperties.getString("String_DM"));
				listDatasourceType.add(ControlsProperties.getString("String_KingBase"));
				listDatasourceType.add(ControlsProperties.getString("String_MySQL"));
				break;
			case OPENDATABASE:
				listDatasourceType.add(ControlsProperties.getString("String_SQL"));
				listDatasourceType.add(ControlsProperties.getString("String_Oracle"));
				listDatasourceType.add(ControlsProperties.getString("String_OracleSpatial"));
				listDatasourceType.add(ControlsProperties.getString("String_PostgreSQL"));
				listDatasourceType.add(ControlsProperties.getString("String_DB2"));
				listDatasourceType.add(ControlsProperties.getString("String_DM"));
				listDatasourceType.add(ControlsProperties.getString("String_KingBase"));
				listDatasourceType.add(ControlsProperties.getString("String_MySQL"));
				// 暂不支持
//				listDatasourceType.add(ControlsProperties.getString("String_ArcSDE"));
				break;
			case OPENWEB:
				listDatasourceType.add(ControlsProperties.getString("String_OGC"));
				listDatasourceType.add(ControlsProperties.getString("String_iServerRest"));
				listDatasourceType.add(ControlsProperties.getString("String_SuperMapCloud"));
				listDatasourceType.add(ControlsProperties.getString("String_GoogleMaps"));
				// 暂不支持
//				listDatasourceType.add(ControlsProperties.getString("String_MapWorld"));
				listDatasourceType.add(ControlsProperties.getString("String_BaiduMap"));
				listDatasourceType.add(ControlsProperties.getString("String_OpenStreetMaps"));
				break;
			}
			listDatasourceType.select(0);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void listWorkspaceType_ItemSelectedChanged(ItemEvent e) {
		try {			
			int index = this.listDatasourceType.getSelectedIndex();
			
			JPanel existingPanel = getPanel(this.preSelectedIndex);			
			JPanel newPanel = getPanel(index);
			this.gl_contentPanel.replace(existingPanel, newPanel);
			
			preSelectedIndex = index;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}
	
	private JPanel getPanel(int index) {
		JPanel result = null;
		try {
			EngineType engineType = EngineType.SQLPLUS;
			switch (datasourceOperatorType) {
			case NEWDATABASE:
				if (this.panelDatasourceInfoDatabase == null) {
					this.panelDatasourceInfoDatabase = new JPanelDatasourceInfoDatabase();
				}
				
				switch (index) {
				case 0: // SQL
					engineType = EngineType.SQLPLUS;					
					break;
				case 1: // Oracle
					engineType = EngineType.ORACLEPLUS;	
					break;
				case 2: // OracleSpatial
					engineType = EngineType.ORACLESPATIAL;	
					break;
				case 3: // PostgreSQL
					engineType = EngineType.POSTGRESQL;	
					break;
				case 4: // DB2
					engineType = EngineType.DB2;	
					break;
				case 5: // DM
					engineType = EngineType.DM;	
					break;
				case 6: // KingBase
					engineType = EngineType.KINGBASE;	
					break;
				case 7: // MySQL
					engineType = EngineType.MYSQL;	
					break;
				}
				
				this.panelDatasourceInfoDatabase.setDatasourceType(engineType);				
				result = this.panelDatasourceInfoDatabase;				
				break;
			case OPENDATABASE:
				if (this.panelDatasourceInfoDatabase == null) {
					this.panelDatasourceInfoDatabase = new JPanelDatasourceInfoDatabase();
				}
				
				switch (index) {
				case 0: // SQL
					engineType = EngineType.SQLPLUS;					
					break;
				case 1: // Oracle
					engineType = EngineType.ORACLEPLUS;	
					break;
				case 2: // OracleSpatial
					engineType = EngineType.ORACLESPATIAL;	
					break;
				case 3: // PostgreSQL
					engineType = EngineType.POSTGRESQL;	
					break;
				case 4: // DB2
					engineType = EngineType.DB2;	
					break;
				case 5: // DM
					engineType = EngineType.DM;	
					break;
				case 6: // KingBase
					engineType = EngineType.KINGBASE;	
					break;
				case 7: // MySQL
					engineType = EngineType.MYSQL;	
					break;
//				case 9: // ArcSDE
//					engineType = EngineType.ARCSDE;	
//					break;
				}
				
				this.panelDatasourceInfoDatabase.setDatasourceType(engineType);				
				result = this.panelDatasourceInfoDatabase;				
				break;
			case OPENWEB:
				if (this.panelDatasourceInfoWeb == null) {
					this.panelDatasourceInfoWeb = new JPanelDatasourceInfoWeb();
				}
				switch (index) {
				case 0: // OGC
					engineType = EngineType.OGC;					
					break;
				case 1: // iServerRest
					engineType = EngineType.ISERVERREST;	
					break;
				case 2: // SuperMapCloud
					engineType = EngineType.SUPERMAPCLOUD;	
					break;
				case 3: // GoogleMaps
					engineType = EngineType.GOOGLEMAPS;	
					break;
//				case 4: // MapWorld
//					engineType = EngineType.MAPWORLD;	
//					break;
				case 4: // BaiduMap
					engineType = EngineType.BAIDUMAPS;	
					break;
				case 5: // OpenStreetMaps
					engineType = EngineType.OPENSTREETMAPS;	
					break;
				}				
				
				this.panelDatasourceInfoWeb.setDatasourceType(engineType);				
				result = this.panelDatasourceInfoWeb;				
				break;
			}			
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return result;
	}
	
	private ActionListener okActionListener=new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			OkButtonActionPerformed(e);
		}
	};
	
	/**
	 * OK按钮点击事件，
	 * 点击时调用面板的加载数据源方法。成功加载时调用关闭函数。
	 */
	private void OkButtonActionPerformed(ActionEvent e){
		int openFlag=-1;

		// TODO 加载时进度条未完成
/*	EventQueue.invokeLater(new Runnable() {
				JDialog loadingDialog = new JDialog();
				public void run() {
					loadingDialog.setSize(100, 100);
					loadingDialog.setLayout(new GridBagLayout());
					JProgressBar loadingJProgressBar = new JProgressBar();
					loadingJProgressBar.setIndeterminate(true);
					loadingJProgressBar.setStringPainted(false);
					JLabel jLabelLoading= new JLabel("loading...");
					
					GridBagConstraints gridBagConstraints = new GridBagConstraints();
					gridBagConstraints.gridx=1;
					gridBagConstraints.gridy=1;
					gridBagConstraints.weightx=0.5;
					gridBagConstraints.weighty=0.5;
					
					loadingDialog.add(loadingJProgressBar,gridBagConstraints);
					gridBagConstraints.gridy=2;
					loadingDialog.add(jLabelLoading, gridBagConstraints);
					loadingDialog.setVisible(true);
				}
			}); */
	if(DatasourceOperatorType.OPENDATABASE == this.datasourceOperatorType){
		//打开数据库型数据源
		openFlag = this.panelDatasourceInfoDatabase.loadDatasource();
		if(JPanelDatasourceInfoDatabase.LOAD_DATASOURCE_SUCCESSFUL == openFlag){
			this.CloseDialog();
		}
	}else if(DatasourceOperatorType.NEWDATABASE == this.datasourceOperatorType){
		//新建数据库型数据源
		openFlag = this.panelDatasourceInfoDatabase.createDatasource();
		if(JPanelDatasourceInfoDatabase.CREATE_DATASOURCE_SUCCESSFUL == openFlag ||JPanelDatasourceInfoDatabase.LOAD_DATASOURCE_EXCEPTION==openFlag){
			this.CloseDialog();
		}
	}else if(DatasourceOperatorType.OPENWEB == this.datasourceOperatorType){
		//打开web型数据源
		openFlag = this.panelDatasourceInfoWeb.loadDatasourece();
		if(JPanelDatasourceInfoWeb.LOAD_DATASOURCE_SUCCESSFUL == openFlag ||JPanelDatasourceInfoWeb.LOAD_DATASOURCE_EXCEPTION==openFlag){
			this.CloseDialog();
		}
	}
		
	}
	
	/**
	 * 返回按钮点击事件，
	 * 点击时调用关闭函数。
	 * @see #CloseDialog()
	 */
	private void CencelButtonActionPerformed(ActionEvent e){
		this.CloseDialog();
	}
	
	/**
	 * 关闭对话框
	 */
	private void CloseDialog(){
		this.dispose();
	}
	
}
