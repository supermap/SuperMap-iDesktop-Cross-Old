package com.supermap.desktop.ui.controls.comboBox;

import java.util.ArrayList;

import com.supermap.data.DatasetType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;

public class ComboBoxDatasetType extends UIComboBox {

	private DatasetType[] supportedDatasetTypes;

	public ComboBoxDatasetType() {
		supportedDatasetTypes = new DatasetType[] { DatasetType.CAD, DatasetType.GRID, DatasetType.GRIDCOLLECTION, DatasetType.IMAGE,
				DatasetType.IMAGECOLLECTION, DatasetType.LINE, DatasetType.LINE3D, DatasetType.LINEM, DatasetType.LINKTABLE, DatasetType.NETWORK,
				DatasetType.PARAMETRICLINE, DatasetType.PARAMETRICREGION, DatasetType.POINT, DatasetType.POINT3D, DatasetType.REGION, DatasetType.REGION3D,
				DatasetType.TABULAR, DatasetType.TEXT, DatasetType.TOPOLOGY, DatasetType.WCS, DatasetType.WMS };

		initialize();

		// this.DrawItem += new
		// DrawItemEventHandler(ComboBoxDatasetType_DrawItem);
		// this.MeasureItem += new
		// MeasureItemEventHandler(ComboBoxDatasetType_MeasureItem);
	}

	// void ComboBoxDatasetType_MeasureItem(object sender, MeasureItemEventArgs
	// e) {
	// try {
	// SizeF itemSize = e.Graphics.MeasureString(this.Items[e.Index].ToString(),
	// this.Font);
	//
	// int vertiaclScrollBarWidth = SystemInformation.FrameBorderSize.Width;
	// if (this.MaxDropDownItems < this.Items.Count) {
	// vertiaclScrollBarWidth += SystemInformation.VerticalScrollBarWidth;
	// }
	//
	// int minWidth = (int)Math.Ceiling(itemSize.Width + vertiaclScrollBarWidth
	// + SystemInformation.SmallIconSize.Width);
	// if (this.DropDownWidth < minWidth) {
	// this.DropDownWidth = minWidth;
	// }
	// } catch (Exception ex) {
	// }
	// }
	//
	// void ComboBoxDatasetType_DrawItem(object sender, DrawItemEventArgs e) {
	// try {
	// if (e.Index >= 0 && e.Index < Items.Count) {
	// // 绘制背景色
	// e.Graphics.TextRenderingHint =
	// System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
	// e.DrawBackground();
	//
	// //Brush backBrush = new SolidBrush(e.BackColor);
	// Brush foreBrush = new SolidBrush(e.ForeColor);
	// //if (!this.Enabled) {
	// // foreBrush = new SolidBrush(Color.Gray);
	// // backBrush = new SolidBrush(SystemColors.Control);
	// //}
	//
	// //e.Graphics.FillRectangle(backBrush, e.Bounds);
	// //if (backBrush != null) {
	// // backBrush.Dispose();
	// //}
	//
	// // 绘制item
	// ComboBoxItem item = Items[e.Index] as SuperMap.Desktop.UI.ComboBoxItem;
	// Size interval = SystemInformation.FixedFrameBorderSize;
	//
	// Rectangle rectImage = new Rectangle();
	// rectImage.Location = new Point(e.Bounds.Left + interval.Width,
	// e.Bounds.Top);
	// rectImage.Width = e.Bounds.Height;
	// rectImage.Height = e.Bounds.Height;
	// if (item.Image != null) {
	// e.Graphics.DrawImageUnscaledAndClipped(item.Image, rectImage);
	// }
	//
	// StringFormat format = new StringFormat();
	// format.Alignment = StringAlignment.Near;
	// format.LineAlignment = StringAlignment.Center;
	// format.FormatFlags = StringFormatFlags.NoClip | StringFormatFlags.NoWrap
	// | StringFormatFlags.LineLimit;
	// format.Trimming = StringTrimming.None;
	//
	// Rectangle rectangle = new Rectangle();
	// rectangle.Location = new Point(rectImage.Right + interval.Width,
	// e.Bounds.Top);
	// rectangle.Height = e.Bounds.Height;
	// rectangle.Width = e.Bounds.Width - rectImage.Right - interval.Width;
	//
	// e.Graphics.DrawString(item.Name, e.Font, foreBrush, rectangle, format);
	// format.Dispose();
	// if (foreBrush != null) {
	// foreBrush.Dispose();
	// }
	//
	// if ((e.State & DrawItemState.Selected) == DrawItemState.Selected) {
	// e.DrawFocusRectangle();
	// }
	// }
	// } catch (Exception ex) {
	// }
	// }

	public DatasetType[] getSupportedDatasetTypes() {
		return this.supportedDatasetTypes;
	}

	public void setSupportedDatasetTypes(DatasetType[] value) {
		this.supportedDatasetTypes = value;
		initialize();
	}

	protected void initialize() {
		super.removeAllItems();	
		try {
			for (DatasetType datasetType : this.supportedDatasetTypes) {
				// String imageName =
				// WorkspaceManagerImageList.GetImageKeyDataset(datasetType,
				// SuperMap.UI.WorkspaceTreeNodeStatus.Normal);
				// Image image =
				// SuperMap.Desktop.UI.Properties.ControlsResources.ResourceManager.GetObject(imageName)
				// as Image;
				// ComboBoxItem item = new ComboBoxItem(datasetType,
				// SuperMap.Desktop.CommonToolkit.DatasetTypeWrap.FindName(datasetType),
				// image);
				// this.Items.Add(item);
				ComboBoxItem item = new ComboBoxItem(datasetType, CommonToolkit.DatasetTypeWrap.findName(datasetType), null);
				this.addItem(item);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	public DatasetType getSelectedDatasetType() {
		DatasetType type = DatasetType.TOPOLOGY;
		try {
			ComboBoxItem item = this.getSelectedItemObject();
			type = (DatasetType) item.getData();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return type;
	}

	public void setSelectedDatasetType(DatasetType value) {
		try {
			ComboBoxItem selectedItem = null;
			for (int i = 0; i < this.getItemCount(); i++) {
				ComboBoxItem item = (ComboBoxItem) this.getItemAt(i);
				if (value == (DatasetType) item.getData()) {
					selectedItem = item;
					break;
				}
			}
			this.setSelectedItemObject(selectedItem);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	public ComboBoxItem getSelectedItemObject() {
		ComboBoxItem item = null;
		if (this.getSelectedItem() != null) {
			item = (ComboBoxItem) this.getSelectedItem();
		}

		return item;
	}

	public void setSelectedItemObject(ComboBoxItem value) {
		this.setSelectedItem(value);
	}

	public Object getSelectedItemData() {
		Object obj = null;
		try {
			obj = this.getSelectedItemObject().getData();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return obj;
	}

	public void setSelectedItemData(Object value) {
		if (value != null) {
			for (int i = 0; i < this.getItemCount(); i++) {
				ComboBoxItem item = (ComboBoxItem) this.getItemAt(i);
				if (item.getData().toString().equals(value.toString())) {
					this.setSelectedItemObject(item);
					break;
				}
			}
		} else {
			this.setSelectedItemObject(null);
		}
	}
}
