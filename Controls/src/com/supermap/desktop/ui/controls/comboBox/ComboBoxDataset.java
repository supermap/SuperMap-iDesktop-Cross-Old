package com.supermap.desktop.ui.controls.comboBox;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.Datasource;
import com.supermap.desktop.Application;

public class ComboBoxDataset extends UIComboBox {

	private boolean isFirstItemEmpty;
	private Datasource datasource;
	private DatasetType[] datasetTypes;
	private boolean autoSelect = true;

	public ComboBoxDataset() {
		// this.DrawMode = DrawMode.OwnerDrawVariable;
		this.isFirstItemEmpty = false;
	}

	/**
	 * 获取或设置组合框第一项是否可为空，默认值为false。
	 */
	public boolean getIsFirstItemEmpty() {
		return this.isFirstItemEmpty;
	}

	public void setIsFirstItemEmpty(boolean value) {
		try {
			if (this.isFirstItemEmpty != value) {
				this.isFirstItemEmpty = value;
			}

			if (this.getItemCount() > 0) {
				ComboBoxItem item = (ComboBoxItem) this.getItemAt(0);
				Dataset dataset = itemObjectToDataset(item);
				if (this.isFirstItemEmpty) {
					if (dataset != null) {
						ComboBoxItem itemNull = buildItemObject(null);
						this.insertItemAt(itemNull, 0);
					}
				} else {
					if (dataset == null) {
						this.remove(0);
					}
				}
			} else if (this.getItemCount() == 0) {
				if (this.isFirstItemEmpty) {
					ComboBoxItem itemNull = buildItemObject(null);
					this.insertItemAt(itemNull, 0);
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	// // /// <summary>
	// // /// 获取或设置组合框的下拉是否自适应宽度
	// // /// 覆盖基类UIComboBox的属性，自适应宽度模式只能为true
	// // /// </summary>
	// // [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
	// // [Browsable(false)]
	// // internal new boolean AutoDropDownWidth
	// // {
	// // get
	// // {
	// // return base.AutoDropDownWidth;
	// // }
	// // set
	// // {
	// // base.AutoDropDownWidth = true;
	// // }
	// // }
	//
	// // /// <summary>
	// // /// 获取组合框的绘制模式
	// // /// 覆盖基类UIComboBox的属性，绘制模式只能是：OwnerDrawVariable
	// // /// </summary>
	// // /// <seealso></seealso>
	// // [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
	// // [Browsable(false)]
	// // public new DrawMode DrawMode
	// // {
	// // get
	// // {
	// // return base.DrawMode;
	// // }
	// // internal set
	// // {
	// // base.DrawMode = DrawMode.OwnerDrawVariable;
	// // }
	// // }
	// //
	// // /// <summary>
	// // /// 获取选择子项的文本
	// // /// </summary>
	// // [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
	// // [Browsable(false)]
	// // public new String SelectedText
	// // {
	// // get
	// // {
	// // String name = String.Empty;
	// // if (SelectedIndex >= 0 && this.Items.Count > 0)
	// // {
	// // ComboBoxItem item = this.Items[SelectedIndex] as ComboBoxItem;
	// // if (item != null)
	// // {
	// // name = item.Name;
	// // }
	// // }
	// // return name;
	// // }
	// // }
	//

	/**
	 * 获取或设置 ComboBox 选中的数据集
	 * 
	 * @return
	 */
	public Dataset getSelectedDataset() {
		Dataset dataset = null;

		try {
			dataset = itemObjectToDataset((ComboBoxItem) this.getSelectedItem());
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return dataset;
	}

	public void setSelectedDataset(Dataset value) {
		try {
			if (value != null) {
				this.setSelectedItem(datasetToItemObject(value));
			} else {
				this.setSelectedItem(null);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 获取或设置关联的数据源
	 */
	public Datasource getDatasource() {
		return this.datasource;
	}

	public void setDatasource(Datasource value) {
		this.datasource = value;
		updateItems();
	}

	/**
	 * 获取或设置支持展示的数据集类型
	 */
	public DatasetType[] getDatasetTypes() {
		return this.datasetTypes;
	}

	public void setDatasetTypes(DatasetType... value) {
		this.datasetTypes = value;
		updateItems();
	}

	/**
	 * 获取或设置是否自动选中一个子项。如果为true，则会自动选中工作空间中的当前激活数据集中的第一个，默认值为true。
	 */
	public boolean getAutoSelectActiveDataset() {
		return this.autoSelect;
	}

	public void setAutoSelectActiveDataset(boolean value) {
		this.autoSelect = value;
	}

	// /// <summary>
	// /// 设置下拉列表的宽度。
	// /// </summary>
	// protected override void SetDropDownWidth()
	// {
	// base.SetDropDownWidth();
	// }
	//
	// /// <summary>
	// /// 引发 System.Windows.Forms.ComboBox.DrawItem 事件。
	// /// </summary>
	// /// <param name="e">包含事件数据的 System.Windows.Forms.DrawItemEventArgs。</param>
	// protected override void OnDrawItem(DrawItemEventArgs e)
	// {
	// try
	// {
	// if (e.Index >= 0 && e.Index < Items.Count)
	// {
	// e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
	//
	// Brush backBrush = null;
	// Brush foreBrush = null;
	// if (!this.Enabled)
	// {
	// foreBrush = new SolidBrush(Color.Gray);
	// backBrush = new SolidBrush(SystemColors.Control);
	// }
	// else
	// {
	// backBrush = new SolidBrush(e.BackColor);
	// foreBrush = new SolidBrush(e.ForeColor);
	// }
	// e.Graphics.FillRectangle(backBrush, e.Bounds);
	// if (backBrush != null)
	// {
	// backBrush.Dispose();
	// }
	//
	// SuperMap.Desktop.UI.ComboBoxItem item = Items[e.Index] as SuperMap.Desktop.UI.ComboBoxItem;
	// Dataset dataset = ItemObjectToDataset(Items[e.Index] as SuperMap.Desktop.UI.ComboBoxItem);
	// Image image = item.Image;
	// String name = item.Name;
	// if (dataset != null)
	// {
	// if (image == null)
	// {
	// String imageName = WorkspaceManagerImageList.GetImageKeyDataset(dataset.Type,
	// SuperMap.UI.WorkspaceTreeNodeStatus.Normal);
	// image = SuperMap.Desktop.UI.Properties.ControlsResources.ResourceManager.GetObject(imageName) as Image;
	// }
	// if (name == null || name.Equals(String.Empty))
	// {
	// name = dataset.Name;
	// }
	// }
	//
	// Rectangle rectImage = new Rectangle();
	// rectImage.Location = new Point(e.Bounds.Location.X + 2, e.Bounds.Location.Y);
	// rectImage.Width = e.Bounds.Height;
	// rectImage.Height = e.Bounds.Height;
	// if (image != null)
	// {
	// e.Graphics.DrawImage(image, rectImage);
	// }
	//
	// StringFormat format = new StringFormat();
	// format.LineAlignment = StringAlignment.Center;
	//
	// RectangleF rectangle = new RectangleF();
	// rectangle.Location = new Point(rectImage.Right + 2, e.Bounds.Top + 1);
	// rectangle.Height = e.Bounds.Height - 2;
	// e.Graphics.DrawString(name, e.Font, foreBrush, rectangle, format);
	//
	// if (foreBrush != null)
	// {
	// foreBrush.Dispose();
	// }
	// }
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// }

	/**
	 * 将一个数据集封装成一个ComboBoxItem。
	 * 
	 * @param dataset
	 * @return
	 */
	private ComboBoxItem buildItemObject(Dataset dataset) {
		String name = "";
		if (dataset != null) {
			name = dataset.getName();
		}
		return new ComboBoxItem(dataset, name);
	}

	/**
	 * 获取ComboBox下拉列表中某个ComboBoxItem对应的数据集。
	 * 
	 * @param itemObject
	 * @return
	 */
	private Dataset itemObjectToDataset(ComboBoxItem itemObject) {
		Dataset dataset = null;

		try {
			if (itemObject != null) {// 得加个判断，因为有些数据集是在下拉项中不存在的，比如说网络数据集的子数据集
				dataset = (Dataset) itemObject.getData();
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return dataset;
	}

	/**
	 * 获取ComboBox下拉列表中某个数据集对应的ComboBoxItem。
	 * 
	 * @param dataset
	 *            待查找的数据集对象
	 * @return 数据集下拉项对应的ComboBoxItem
	 */
	public ComboBoxItem datasetToItemObject(Dataset dataset) {
		ComboBoxItem itemObject = null;
		try {
			for (int i = 0; i < this.getItemCount(); i++) {
				ComboBoxItem item = (ComboBoxItem) this.getItemAt(i);
				Dataset itemDataset = itemObjectToDataset(item);
				if (itemDataset == dataset) {
					itemObject = item;
					break;
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return itemObject;
	}

	/**
	 * 更新组合框的子项
	 */
	private void updateItems() {
		try {
			this.removeAllItems();
			int selectedIndex = 0;
			if (this.getDatasource() != null) {
				Dataset activeDataset = getActiveDataset();
				try {
					for (int i = 0; i < this.getDatasource().getDatasets().getCount(); i++) {
						Dataset dataset = this.getDatasource().getDatasets().get(i);
						DatasetType type = dataset.getType();
						if (this.getDatasetTypes() != null && this.getDatasetTypes().length > 0 && !isSupportDatasetType(type)) {
							continue;
						}

						int index = this.add(dataset);
						/*if (dataset == activeDataset) {
							selectedIndex = index;
						}*/
					}
				} catch (Exception ex) {
					return;
				}
			}

			/*if (this.getItemCount() > 0 && this.autoSelect) {
				this.setSelectedIndex(selectedIndex);
			}*/
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 根据传入的数据集类型判断此类型是否支持显示
	 * 
	 * @param type
	 *            数据集类型
	 * @return
	 */
	private boolean isSupportDatasetType(DatasetType type) {
		boolean isSupport = false;
		try {
			if (this.datasetTypes == null || (this.datasetTypes != null && this.datasetTypes.length == 0)) {
				isSupport = true;
			} else {
				for (int i = 0; i < this.datasetTypes.length; i++) {
					if (this.datasetTypes[i] == type) {
						isSupport = true;
						break;
					}
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return isSupport;
	}

	private Dataset getActiveDataset() {
		Dataset dataset = null;
		try {
			if (Application.getActiveApplication().getActiveDatasets() != null && Application.getActiveApplication().getActiveDatasets().length > 0) {
				dataset = Application.getActiveApplication().getActiveDatasets()[0];
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return dataset;
	}

	/**
	 * 从下拉列表中移除指定名称的数据集。
	 * 
	 * @param datasetName
	 *            待移除数据集的名称
	 */
	public void remove(String datasetName) {
		try {
			for (int i = 0; i < this.getItemCount(); i++) {
				ComboBoxItem itemObject = (ComboBoxItem) this.getItemAt(i);
				if (itemObject.getName().equals(datasetName)) {
					this.remove(i);
					break;
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 从下拉列表中移除指定的数据集。
	 * 
	 * @param dataset
	 *            待移除数据集
	 */
	public void remove(Dataset dataset) {
		try {
			this.remove(dataset.getName());
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 向下拉列表添加一个数据集。
	 * 
	 * @param dataset
	 *            待添加数据集
	 * @return 新增数据集的索引
	 */
	public int add(Dataset dataset) {
		int index = -1;
		try {
			this.addItem(buildItemObject(dataset));
			index = this.getItemCount() - 1;
			if (this.autoSelect && this.getSelectedDataset() == null) {
				this.setSelectedIndex(index);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return index;
	}

	public void refreshType(){
		this.removeAllItems();
		
	}
	// /**
	// * 向下拉列表添加一组数据集。
	// * @param datasets 待添加数据集集合
	// */
	// public void addRange(Dataset[] datasets) {
	// try {
	// List<SuperMap.Desktop.UI.ComboBoxItem> items = new List<SuperMap.Desktop.UI.ComboBoxItem>();
	// foreach (Dataset dataset in datasets)
	// {
	// items.Add(BuildItemObject(dataset));
	// }
	//
	// this.Items.AddRange(items.ToArray());
	// }catch (Exception ex) {
	// Application.getActiveApplication().getOutput().output(ex);
	// }
	// }
}
