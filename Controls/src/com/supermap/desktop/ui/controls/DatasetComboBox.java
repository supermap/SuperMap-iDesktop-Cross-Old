package com.supermap.desktop.ui.controls;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.Datasets;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.properties.CommonProperties;

/**
 * 带有图标的数据集下拉选择框
 * 
 * @author xie
 * 
 */
public class DatasetComboBox extends JComboBox<Object> {

	private static final long serialVersionUID = 1L;
	private DatasetType[] datasetTypes;
	private Datasets datasets;
	/**
	 * 包含各种数据集类型的下拉选择框
	 */
	public DatasetComboBox() {
		super(initDatasetComboBoxItem());
		ListCellRenderer<Object> renderer = new CommonListCellRenderer();
		setRenderer(renderer);
	}
	/**
	 * 根据给定数据集类型集合创建下拉选择框
	 * 
	 * @param datasetTypes
	 */
	public DatasetComboBox(String[] datasetTypes) {
		super(initDatasetComboBoxItem(datasetTypes));
		ListCellRenderer<Object> renderer = new CommonListCellRenderer();
		setRenderer(renderer);
	}

	/**
	 * 根据给定的数据集集合类创建下拉选择框
	 * 
	 * @param datasets
	 */
	public DatasetComboBox(Datasets datasets) {
		super(initDatasetComboBoxItem(datasets));
		this.datasets = datasets;
		ListCellRenderer<Object> renderer = new CommonListCellRenderer();
		setRenderer(renderer);
	}
	/**
	 * 根据给定的
	 */
	public DatasetComboBox(Dataset[] datasets){
		super(initDatasetComboBoxItem(datasets));
		ListCellRenderer<Object> renderer = new CommonListCellRenderer();
		setRenderer(renderer);
	} 
	/**
	 * 根据给定的数据集类型集合创建下拉选择框
	 * @param datasetTypes
	 */
	public DatasetComboBox(DatasetType[] datasetTypes) {
		super(initDatasetComboBoxItem(datasetTypes));
		this.datasetTypes = datasetTypes;
		ListCellRenderer<Object> renderer = new CommonListCellRenderer();
		setRenderer(renderer);
	}
	private static JPanel[] initDatasetComboBoxItem(DatasetType[] datasetTypes) {
		JPanel[] result = new JPanel[datasetTypes.length];
		for (int i = 0; i < datasetTypes.length; i++) {
			String filePath = CommonToolkit.DatasetImageWrap.getImageIconPath(datasetTypes[i]);
			String datasetType = CommonToolkit.DatasetTypeWrap.findName(datasetTypes[i]);
			result[i] = new DataCell(filePath, datasetType);
		}
		return result;
	}
	private static JPanel[] initDatasetComboBoxItem(Dataset[] datasets) {
		JPanel[] result = new JPanel[datasets.length];
		for (int i = 0; i < datasets.length; i++) {
			String filePath = CommonToolkit.DatasetImageWrap.getImageIconPath(datasets[i].getType());
			result[i] = new DataCell(filePath, datasets[i].getName());
		}
		return result;
	}

	/**
	 * 由于填充的是DatasetCell 返回时要得到DatasetCell中JLabel显示的字符串
	 * 
	 * @return
	 */
	public String getSelectItem() {
		DataCell temp = (DataCell) getSelectedItem();
		return temp.getDatasetName();
	}

	private static JPanel[] initDatasetComboBoxItem() {
		String[] temp = new String[] {
				CommonProperties.getString("String_DatasetType_All"),
				CommonProperties.getString("String_DatasetType_CAD"),
				CommonProperties.getString("String_DatasetType_Grid"),
				CommonProperties.getString("String_DatasetType_GridCollection"),
				CommonProperties.getString("String_DatasetType_Image"),
				CommonProperties.getString("String_DatasetType_ImageCollection"),
				CommonProperties.getString("String_DatasetType_Line"),
				CommonProperties.getString("String_DatasetType_Line3D"),
				CommonProperties.getString("String_DatasetType_LinkTable"),
				CommonProperties.getString("String_DatasetType_Network"),
				CommonProperties.getString("String_DatasetType_ParametricLine"),
				CommonProperties.getString("String_DatasetType_ParametricRegion"),
				CommonProperties.getString("String_DatasetType_Point"),
				CommonProperties.getString("String_DatasetType_Point3D"),
				CommonProperties.getString("String_DatasetType_Region"),
				CommonProperties.getString("String_DatasetType_Region3D"),
				CommonProperties.getString("String_DatasetType_Router"),
				CommonProperties.getString("String_DatasetType_Tabular"),
				CommonProperties.getString("String_DatasetType_Template"),
				CommonProperties.getString("String_DatasetType_Text"),
				CommonProperties.getString("String_DatasetType_Topology"),
				CommonProperties.getString("String_DatasetType_Unknown"),
				CommonProperties.getString("String_DatasetType_WCS"),
				CommonProperties.getString("String_DatasetType_WMS")
		};
		JPanel[] result = initDatasetComboBoxItem(temp);
		return result;
	}

	/**
	 * 根据数据集类型的中文翻译集合初始化DatasetComboBox中的单元格
	 * 
	 * @param datasetTypes：数据集类型的中文翻译集合
	 * @return
	 */
	private static JPanel[] initDatasetComboBoxItem(String[] datasetTypes) {
		JPanel[] result = new JPanel[datasetTypes.length];
		for (int i = 0; i < datasetTypes.length; i++) {
			String filePath = CommonToolkit.DatasetImageWrap.getImageIconPath(datasetTypes[i]);
			result[i] = new DataCell(filePath, datasetTypes[i]);
		}
		return result;
	}

	private static JPanel[] initDatasetComboBoxItem(Datasets datasets) {
		JPanel[] result = new JPanel[datasets.getCount()];
		for (int i = 0; i < datasets.getCount(); i++) {
			String filePath = CommonToolkit.DatasetImageWrap.getImageIconPath(datasets.get(i).getType());
			String datasetType = CommonToolkit.DatasetTypeWrap.findName(datasets.get(i).getType());
			result[i] = new DataCell(filePath, datasetType);
		}
		return result;
	}

	public DatasetType[] getDatasetTypes() {
		return datasetTypes;
	}

	public void setDatasetTypes(DatasetType[] datasetTypes) {
		this.datasetTypes = datasetTypes;
	}

	public Datasets getDatasets() {
		return datasets;
	}

	public void setDatasets(Datasets datasets) {
		this.datasets = datasets;
	}

}
