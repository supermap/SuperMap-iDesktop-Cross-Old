package com.supermap.desktop.ui.controls;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.supermap.desktop.Application;

public class SmDialog extends JDialog implements WindowListener{
	
	public SmDialog() {
		super((Frame)Application.getActiveApplication().getMainFrame(), true);
		this.addWindowListener((WindowListener) this);  
	}
	
	public SmDialog(JFrame owner) {
		super(owner, false);
		this.addWindowListener((WindowListener) this);  
	}
	
	public SmDialog(JDialog owner) {
		super(owner, false);
		this.addWindowListener((WindowListener) this);  
	}
	
	public SmDialog(JFrame owner, boolean modal) {
		super(owner, modal);
		this.addWindowListener((WindowListener) this);  
	}
	
	public SmDialog(JDialog owner, boolean modal) {
		super(owner, modal);
		this.addWindowListener((WindowListener) this);  
	}

	public DialogResult showDialog() {
		this.setVisible(true);
//		dialog.show();
        
        return this.getDialogResult();
	}
	
	protected DialogResult dialogResult = DialogResult.APPLY;
	public DialogResult getDialogResult() {
		return dialogResult;
	}
	
	public void windowOpened(WindowEvent e) {
		
	}
	
	public void windowClosing(WindowEvent e) {
		
	}
	
	public void windowClosed(WindowEvent e) {
		
	}
	
	public void windowIconified(WindowEvent e) {
		
	}
	
	public void windowDeiconified(WindowEvent e) {
		
	}
	
	public void windowActivated(WindowEvent e) {
		
	}
	
	public void windowDeactivated(WindowEvent e) {
		
	}

}
