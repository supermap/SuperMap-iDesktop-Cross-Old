package com.supermap.desktop.ui.controls;

import java.awt.Graphics;

import com.supermap.data.Geometry;
import com.supermap.data.Resources;
import com.supermap.data.Toolkit;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: SuperMap GIS Technologies Inc.</p>
 *
 * @author 孔令亮
 * @version 2.0
 */
class InternalToolkitControl extends Toolkit {
    public InternalToolkitControl() {
    }

    protected static boolean internalDraw(Geometry geometry, 
    		Resources resources, Graphics graphics){
        return Toolkit.internalDraw(geometry, resources, graphics);
    }
    
    static double DBL_MAX_VALUE = Toolkit.DBL_MAX_VALUE;
    static double DBL_MIN_VALUE = Toolkit.DBL_MIN_VALUE;
    static float FLT_MAX_VALUE = Toolkit.FLT_MAX_VALUE;
    static float FLT_MIN_VALUE = Toolkit.FLT_MIN_VALUE;
    
}
