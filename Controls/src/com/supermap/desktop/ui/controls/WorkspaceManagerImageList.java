package com.supermap.desktop.ui.controls;

public class WorkspaceManagerImageList {
	// 工作空间图像
    static String g_WorkspaceTypeDefault = "WorkspaceDefault";
    static String g_WorkspaceTypeSMW = "WorkspaceSMW";
    static String g_WorkspaceTypeSXW = "WorkspaceSXW";
    static String g_WorkspaceTypeSQL = "WorkspaceSQL";
    static String g_WorkspaceTypeOracle = "WorkspaceOracle";

    static String g_Datasources = "Datasources";

    // 数据源
    static String g_DatasourceImagePlugins = "DatasourceImagePlugins";
    static String g_DatasourceOraclePlus = "DatasourceOraclePlus";
    static String g_DatasourceOracleSpatial = "DatasourceOracleSpatial";
    static String g_DatasourceSQLPlus = "DatasourceSQLPlus";
    static String g_DatasourceOGC = "DatasourceOGC";
    static String g_DatasourceUDB = "DatasourceUDB";
    static String g_DatasourceDB2 = "DatasourceDB2";
    static String g_DatasourcePostgreSQL = "DatasourcePostgreSQL";

    static String g_DatasetTabular = "DatasetTabular";
    static String g_DatasetPoint = "DatasetPoint";
    static String g_DatasetLine = "DatasetLine";
    static String g_DatasetNetwork = "DatasetNetwork";
    static String g_DatasetNetwork3D = "DatasetNetwork3D";
    static String g_DatasetRegion = "DatasetRegion";
    static String g_DatasetText = "DatasetText";
    static String g_DatasetLineM = "DatasetLineM";
    static String g_DatasetPoint3D = "DatasetPoint3D";
    static String g_DatasetLine3D = "DatasetLine3D";
    static String g_DatasetRegion3D = "DatasetRegion3D";
    static String g_DatasetImage = "DatasetImage";
    static String g_DatasetGrid = "DatasetGrid";
    static String g_DatasetWMS = "DatasetWMS";
    static String g_DatasetWCS = "DatasetWCS";
    static String g_DatasetCAD = "DatasetCAD";
    static String g_DatasetLinkTable = "DatasetLinkTable";
    static String g_DatasetModel = "DatasetModel";

    static String g_DatasetTopology = "DatasetTopology";
    static String g_TopologyDatasetRelations = "TopologyDatasetRelations";
    static String g_TopologyErrorDatasets = "TopologyErrorDatasets";

    static String g_DatasetParametricLine = "DatasetParametricLine";
    static String g_DatasetParametricRegion = "DatasetParametricRegion";
    static String g_DatasetGridCollection = "DatasetGridCollection";
    static String g_DatasetImageCollection = "DatasetImageCollection";

    static String g_DatasetUnknown = "DatasetUnknown";

    static String g_Maps = "Maps";
    static String g_Map = "Map";

    static String g_Layouts = "Layouts";
    static String g_Layout = "Layout";

    static String g_Scenes = "Scenes";
    static String g_Scene = "Scene";

    static String g_Resources = "Resources";

    static String g_SymbolMarker = "SymbolMarker";
    static String g_SymbolLine = "SymbolLine";
    static String g_SymbolFill = "SymbolFill";

    static String g_StatusNormal = "Normal";
    static String g_StatusModified = "Modified";
    static String g_StatusNew = "New";


    static String g_imageKeyFormat = "Image_{0}_{1}";

//    static Dictionary<String, String> m_basicType;
//    static Dictionary<WorkspaceTreeNodeStatus, String> m_statusType;
//
//    ImageList m_treeIamgeList;
//
//    internal delegate String GetWorkspaceTreeNodeImageKey(WorkspaceTreeNodeBase node);
//
//    static Dictionary<WorkspaceTreeNodeDataType, GetWorkspaceTreeNodeImageKey> m_getNodeImageKeyFuncs;
//
//    static WorkspaceManagerImageList()
//    {
//        m_basicType = new Dictionary<string, string>();
//
//        // 工作空间类型
//        m_basicType.Add(WorkspaceType.Default.ToString(), g_WorkspaceTypeDefault);
//        m_basicType.Add(WorkspaceType.Oracle.ToString(), g_WorkspaceTypeOracle);
//        m_basicType.Add(WorkspaceType.SMW.ToString(), g_WorkspaceTypeSMW);
//        m_basicType.Add(WorkspaceType.SQL.ToString(), g_WorkspaceTypeSQL);
//        m_basicType.Add(WorkspaceType.SXW.ToString(), g_WorkspaceTypeSXW);
//
//        // 数据源集合
//        m_basicType.Add(g_Datasources, g_Datasources);
//
//        // 数据源类型
//        m_basicType.Add(EngineType.ImagePlugins.ToString(), g_DatasourceImagePlugins);
//        m_basicType.Add(EngineType.OraclePlus.ToString(), g_DatasourceOraclePlus);
//        m_basicType.Add(EngineType.OracleSpatial.ToString(), g_DatasourceOracleSpatial);
//        m_basicType.Add(EngineType.SQLPlus.ToString(), g_DatasourceSQLPlus);
//        m_basicType.Add(EngineType.OGC.ToString(), g_DatasourceOGC);
//        m_basicType.Add(EngineType.UDB.ToString(), g_DatasourceUDB);
//        m_basicType.Add(EngineType.DB2.ToString(), g_DatasourceDB2);
//        m_basicType.Add(EngineType.PostgreSQL.ToString(), g_DatasourcePostgreSQL);
//
//        // 数据集类型         
//        m_basicType.Add(DatasetType.Tabular.ToString(), g_DatasetTabular);
//        m_basicType.Add(DatasetType.Point.ToString(), g_DatasetPoint);
//        m_basicType.Add(DatasetType.Line.ToString(), g_DatasetLine);
//        m_basicType.Add(DatasetType.Region.ToString(), g_DatasetRegion);
//        m_basicType.Add(DatasetType.Network.ToString(), g_DatasetNetwork);
//        m_basicType.Add(DatasetType.Network3D.ToString(), g_DatasetNetwork3D);
//        m_basicType.Add(DatasetType.Text.ToString(), g_DatasetText);
//        m_basicType.Add(DatasetType.LineM.ToString(), g_DatasetLineM);
//        m_basicType.Add(DatasetType.Point3D.ToString(), g_DatasetPoint3D);
//        m_basicType.Add(DatasetType.Line3D.ToString(), g_DatasetLine3D);
//        m_basicType.Add(DatasetType.Region3D.ToString(), g_DatasetRegion3D);
//        m_basicType.Add(DatasetType.Image.ToString(), g_DatasetImage);
//        m_basicType.Add(DatasetType.Grid.ToString(), g_DatasetGrid);
//        m_basicType.Add(DatasetType.WMS.ToString(), g_DatasetWMS);
//        m_basicType.Add(DatasetType.WCS.ToString(), g_DatasetWCS);
//        m_basicType.Add(DatasetType.CAD.ToString(), g_DatasetCAD);
//        m_basicType.Add(DatasetType.LinkTable.ToString(), g_DatasetLinkTable);
//        m_basicType.Add(DatasetType.Topology.ToString(), g_DatasetTopology);
//        m_basicType.Add(DatasetType.Model.ToString(), g_DatasetModel);
//
//        m_basicType.Add(DatasetType.ParametricLine.ToString(), g_DatasetParametricLine);
//        m_basicType.Add(DatasetType.ParametricRegion.ToString(), g_DatasetParametricRegion);
//
//        m_basicType.Add(DatasetType.GridCollection.ToString(), g_DatasetGridCollection);
//        m_basicType.Add(DatasetType.ImageCollection.ToString(), g_DatasetImageCollection);
//
//        m_basicType.Add(g_DatasetUnknown, g_DatasetUnknown);
//
//        m_basicType.Add(g_TopologyErrorDatasets, g_TopologyErrorDatasets);
//        m_basicType.Add(g_TopologyDatasetRelations, g_TopologyDatasetRelations);
//
//        m_basicType.Add(g_Maps, g_Maps);
//        m_basicType.Add(g_Map, g_Map);
//
//        m_basicType.Add(g_Layouts, g_Layouts);
//        m_basicType.Add(g_Layout, g_Layout);
//
//        m_basicType.Add(g_Scenes, g_Scenes);
//        m_basicType.Add(g_Scene, g_Scene);
//
//        m_basicType.Add(g_Resources, g_Resources);
//        m_basicType.Add(g_SymbolMarker, g_SymbolMarker);
//        m_basicType.Add(g_SymbolLine, g_SymbolLine);
//        m_basicType.Add(g_SymbolFill, g_SymbolFill);
//
//        // 状态
//        m_statusType = new Dictionary<WorkspaceTreeNodeStatus, string>();
//        m_statusType.Add(WorkspaceTreeNodeStatus.Normal, g_StatusNormal);
//        m_statusType.Add(WorkspaceTreeNodeStatus.Modified, g_StatusModified);
//        m_statusType.Add(WorkspaceTreeNodeStatus.New, g_StatusNew);
//
//        m_getNodeImageKeyFuncs = new Dictionary<WorkspaceTreeNodeDataType, GetWorkspaceTreeNodeImageKey>();
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Workspace, GetNodeImageKeyWorkspace);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Datasources, GetNodeImageKeyDatasources);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Datasource, GetNodeImageKeyDatasource);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.DatasetVector, GetNodeImageKeyDatasetVector);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.DatasetImage, GetNodeImageKeyDatasetImage);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.DatasetGrid, GetNodeImageKeyDatasetGrid);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.DatasetTopology, GetNodeImageKeyDatasetTopology);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.DatasetUnknown, GetNodeImageKeyDatasetUnknown);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Maps, GetNodeImageKeyMaps);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.MapName, GetNodeImageKeyMap);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Layouts, GetNodeImageKeyLayouts);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.LayoutName, GetNodeImageKeyLayout);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Scenes, GetNodeImageKeyScenes);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.SceneName, GetNodeImageKeyScene);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.Resources, GetNodeImageKeyResources);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.SymbolMarker, GetNodeImageKeySymbolMarker);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.SymbolLine, GetNodeImageKeySymbolLine);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.SymbolFill, GetNodeImageKeySymbolFill);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.TopologyDatasetRelations, GetNodeImageKeyTopologyDatasetRelations);
//        m_getNodeImageKeyFuncs.Add(WorkspaceTreeNodeDataType.TopologyErrorDatasets, GetNodeImageKeyTopologyErrorDatasets);
//    }
//
//    public WorkspaceManagerImageList()
//    {
//        BuildImageList();
//    }
//
//    void BuildImageList()
//    {
//        try
//        {
//            m_treeIamgeList = new ImageList();
//            foreach (KeyValuePair<WorkspaceTreeNodeStatus, String> status in m_statusType)
//            {
//                foreach (KeyValuePair<String, String> basicType in m_basicType)
//                {
//                    String imageName = String.Format(g_imageKeyFormat, basicType.Value, status.Value);
//                    try
//                    {
//                        Image image = Properties.ControlsResources.ResourceManager.GetObject(imageName) as Image;
//
//                        if (image != null)
//                        {
//                            m_treeIamgeList.Images.Add(imageName, image);
//                        }
//                    }
//                    catch
//                    {
//
//                    }
//                }
//            }
//        }
//        catch
//        {
//            //   Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
//        }
//    }
//    #region  获取节点图片ID字符串的系列函数
//    // 获取工作空间的ImageKey
//    internal static String GetNodeImageKeyWorkspace(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            WorkspaceTreeNodeWorkspace nodeWorkspace = node as WorkspaceTreeNodeWorkspace;
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[nodeWorkspace.Workspace.Type.ToString()], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取数据源集合的ImageKey
//    internal static String GetNodeImageKeyDatasources(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Datasources], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取数据源的ImageKey
//    internal static String GetImageKeyDatasource(EngineType engType, WorkspaceTreeNodeStatus nodeStatus)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            String engTypeName = EngineType.ImagePlugins.ToString();
//
//            if (m_basicType.ContainsKey(engType.ToString()))
//            {
//                engTypeName = engType.ToString();
//            }
//
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[engTypeName], nodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取数据源的ImageKey
//    internal static String GetNodeImageKeyDatasource(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            WorkspaceTreeNodeDatasource nodeDatasource = node as WorkspaceTreeNodeDatasource;
//            imageKey = GetImageKeyDatasource(nodeDatasource.Datasource.EngineType, node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    //获取数据集的ImageKey
//    internal static String GetImageKeyDataset(DatasetType type)
//    {
//        String imageKey = String.Empty;
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[type.ToString()], WorkspaceTreeNodeStatus.Normal);
//        }
//        catch
//        {
//
//        }
//        return imageKey;
//    }
//
//    //获取数据集的ImageKey
//    internal static String GetImageKeyDataset(DatasetType type, WorkspaceTreeNodeStatus nodeStatus)
//    {
//        String imageKey = String.Empty;
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[type.ToString()], nodeStatus);
//        }
//        catch
//        {
//
//        }
//        return imageKey;
//    }
//
//    //获得未知数据集的ImageKey
//    internal static String GetImageKeyDatasetUnknown()
//    {
//        String imageKey = String.Empty;
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_DatasetUnknown], WorkspaceTreeNodeStatus.Normal);
//        }
//        catch
//        {
//
//        }
//        return imageKey;
//    }
//
//    //获得未知数据集的ImageKey
//    internal static String GetNodeImageKeyDatasetUnknown(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_DatasetUnknown], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//        return imageKey;
//    }
//
//    // 获取矢量数据集的ImageKey
//    internal static String GetNodeImageKeyDatasetVector(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            WorkspaceTreeNodeDatasetVector nodeDataset = node as WorkspaceTreeNodeDatasetVector;
//            imageKey = GetImageKeyDataset(nodeDataset.DatasetVector.Type, node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取Image数据集的ImageKey
//    internal static String GetNodeImageKeyDatasetImage(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            WorkspaceTreeNodeDatasetImage nodeDatasetImage = node as WorkspaceTreeNodeDatasetImage;
//            imageKey = GetImageKeyDataset(nodeDatasetImage.DatasetImage.Type, node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取Grid数据集的ImageKey
//    internal static String GetNodeImageKeyDatasetGrid(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            WorkspaceTreeNodeDatasetGrid nodeDatasetGrid = node as WorkspaceTreeNodeDatasetGrid;
//            imageKey = GetImageKeyDataset(nodeDatasetGrid.DatasetGrid.Type, node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    //获取Topology数据集的imageKey
//    internal static String GetNodeImageKeyDatasetTopology(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            WorkspaceTreeNodeDatasetTopology nodeDatasetTopology = node as WorkspaceTreeNodeDatasetTopology;
//            imageKey = GetImageKeyDataset(nodeDatasetTopology.DatasetTopology.Type, node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    //获取Topology数据集的关联子项节点的imageKey
//    internal static String GetNodeImageKeyTopologyDatasetRelations(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_TopologyDatasetRelations], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    //获取Topology数据集的错误数据集集合子节点的imageKey
//    internal static String GetNodeImageKeyTopologyErrorDatasets(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_TopologyErrorDatasets], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取地图集合的ImageKey
//    internal static String GetNodeImageKeyMaps(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Maps], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//        return imageKey;
//    }
//
//    // 获取地图的Imagekey
//    internal static String GetNodeImageKeyMap(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Map], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//        return imageKey;
//    }
//
//    // 获取布局集合的Imagekey
//    internal static String GetNodeImageKeyLayouts(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Layouts], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取布局的ImageKey
//    internal static String GetNodeImageKeyLayout(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Layout], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取场景集合的ImageKey
//    internal static String GetNodeImageKeyScenes(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Scenes], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取场景的ImageKey
//    internal static String GetNodeImageKeyScene(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Scene], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取资源集合的ImageKey
//    internal static String GetNodeImageKeyResources(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_Resources], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取点符号的ImageKey
//    internal static String GetNodeImageKeySymbolMarker(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_SymbolMarker], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取线符号的ImageKey
//    internal static String GetNodeImageKeySymbolLine(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_SymbolLine], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取填充符号的ImageKey
//    internal static String GetNodeImageKeySymbolFill(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            imageKey = String.Format(g_imageKeyFormat, m_basicType[g_SymbolFill], node.NodeStatus);
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//
//    // 获取给定节点的ImageKey
//    internal static String GetNodeImageKey(WorkspaceTreeNodeBase node)
//    {
//        String imageKey = String.Empty;
//
//        try
//        {
//            GetWorkspaceTreeNodeImageKey func = null;
//
//            if (node.UserType.Length != 0)
//            {
//                imageKey = node.UserType;
//            }
//            else
//            {
//                func = m_getNodeImageKeyFuncs[node.NodeType];
//                imageKey = func(node);
//            }
//        }
//        catch
//        {
//
//        }
//
//        return imageKey;
//    }
//    #endregion
//    internal ImageList ImageList
//    {
//        get
//        {
//            return m_treeIamgeList;
//        }
//    }
}
