package com.supermap.desktop.ui;

import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.data.Datasets;
import com.supermap.data.Datasource;
import com.supermap.data.Datasources;
import com.supermap.data.GeoStyle;
import com.supermap.data.Resources;
import com.supermap.data.SymbolFillLibrary;
import com.supermap.data.SymbolType;
import com.supermap.data.Workspace;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IContextMenuManager;
import com.supermap.desktop.Interface.IFormLayout;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.Interface.IFormScene;
import com.supermap.desktop.enums.WindowType;
import com.supermap.desktop.ui.controls.NodeDataType;
import com.supermap.desktop.ui.controls.SymbolDialog;
import com.supermap.desktop.ui.controls.SymbolLibraryDialog;
import com.supermap.desktop.ui.controls.TreeNodeData;
import com.supermap.desktop.ui.controls.WorkspaceTree;
import com.supermap.desktop.ui.controls.WorkspaceTreeTransferHandler;
import com.supermap.desktop.utilties.TabularUtilties;
import com.supermap.layout.MapLayout;
import com.supermap.mapping.Layer;
import com.supermap.mapping.Map;
import com.supermap.realspace.Scene;
import com.supermap.ui.Action;

public class WorkspaceComponentManager extends JComponent {

	private javax.swing.JScrollPane jScrollPane = null;
	private WorkspaceTree workspaceTree = null;
	// private Timer this.timer;
	// 临时的变量，现在还没有自动加载Dockbar，所以暂时用这个变量测试
	private boolean isContextMenuBuilded = false;

	private ArrayList<TreeNode> searchedList;
	private int displayIndex;
	private HashMap<TreeNode, Color> hashMapNodeDefaultColor;
	private int defaultType = -1;
	private int workspaceType = 0;
	private int datasourceType = 1;

	public WorkspaceComponentManager() {

		initializeComponent();
		initializeResources();
		initilize(Application.getActiveApplication().getWorkspace());
		// SuperMap.Desktop.CommonToolkit.ThemeStyleChangedEvent += new
		// ThemeStyleChangedEventHandler(_Toolkit_ThemeStyleChangedEvent);
		// if (SuperMap.Desktop.CommonToolkit.ThemeStyleWrap.CurrentThemeStyle
		// != null) {
		// ChangeThemeStyle(SuperMap.Desktop.CommonToolkit.ThemeStyleWrap.CurrentThemeStyle);
		// }
	}

	public WorkspaceComponentManager(Workspace workspace) {

		initializeComponent();
		initializeResources();
		initilize(workspace);
		// SuperMap.Desktop.CommonToolkit.ThemeStyleChangedEvent += new
		// ThemeStyleChangedEventHandler(_Toolkit_ThemeStyleChangedEvent);
		// if (SuperMap.Desktop.CommonToolkit.ThemeStyleWrap.CurrentThemeStyle
		// != null) {
		// ChangeThemeStyle(SuperMap.Desktop.CommonToolkit.ThemeStyleWrap.CurrentThemeStyle);
		// }
	}

	private void initializeComponent() {

		this.jScrollPane = new javax.swing.JScrollPane();
		this.workspaceTree = new WorkspaceTree();
		this.workspaceTree.setDragEnabled(true);
		this.workspaceTree.setShowsRootHandles(true);
		this.workspaceTree.setTransferHandler(new WorkspaceTreeTransferHandler());
		this.workspaceTree.setLayoutsNodeVisible(false);

		this.jScrollPane.setViewportView(this.workspaceTree);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane,
				javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane,
				javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE));
		/**
		 * 拖动实现打开文件型工作空间或者数据源
		 */
		new DropTarget(jScrollPane, new WorkspaceDropTargetAdapter());
	}

	public JScrollPane getJScrollPanel() {
		return this.jScrollPane;
	}

	public WorkspaceTree getWorkspaceTree() {
		return this.workspaceTree;
	}

	public Workspace getWorkspace() {
		return this.workspaceTree.getWorkspace();
	}

	public void setWorkspace(Workspace workspace) {
		this.workspaceTree.setWorkspace(workspace);
	}

	private JPopupMenu workspacePopupMenu = null;

	/**
	 * 获取工作空间管理器中工作空间结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getWorkspacePopupMenu() {
		return this.workspacePopupMenu;
	}

	private JPopupMenu datasourcesPopupMenu = null;

	/**
	 * 获取工作空间管理器中数据源集合的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasourcesPopupMenu() {
		return this.datasourcesPopupMenu;
	}

	private JPopupMenu datasourcePopupMenu = null;

	/**
	 * 获取工作空间管理器中数据源结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasourcePopupMenu() {
		return this.datasourcePopupMenu;
	}

	private JPopupMenu datasetPopupMenu = null;

	/**
	 * 获取工作空间管理器中数据集结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetPopupMenu() {
		return this.datasetPopupMenu;
	}

	private JPopupMenu datasetVectorPopupMenu = null;

	/**
	 * 获取工作空间管理器中矢量数据集结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetVectorPopupMenu() {
		return this.datasetVectorPopupMenu;
	}

	private JPopupMenu datasetTabularPopupMenu = null;

	/**
	 * 获取工作空间管理器中属性表数据集结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetTabularPopupMenu() {
		return this.datasetTabularPopupMenu;
	}

	private JPopupMenu datasetImagePopupMenu = null;

	/**
	 * 获取工作空间管理器中影像数据集结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetImagePopupMenu() {
		return this.datasetImagePopupMenu;
	}

	private JPopupMenu datasetGridPopupMenu = null;

	/**
	 * 获取工作空间管理器中栅格数据集结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetGridPopupMenu() {
		return this.datasetGridPopupMenu;
	}

	private JPopupMenu datasetTopologyPopupMenu = null;

	/**
	 * 获取工作空间管理器中拓扑数据集结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetTopologyPopupMenu() {
		return this.datasetTopologyPopupMenu;
	}

	private JPopupMenu mapsPopupMenu = null;

	/**
	 * 获取工作空间管理器中地图集合结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getMapsPopupMenu() {
		return this.mapsPopupMenu;
	}

	private JPopupMenu mapPopupMenu = null;

	/**
	 * 获取工作空间管理器中地图结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getMapPopupMenu() {
		return this.mapPopupMenu;
	}

	private JPopupMenu layoutsPopupMenu = null;

	/**
	 * 获取工作空间管理器中布局集合结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getLayoutPopupMenu() {
		return this.layoutsPopupMenu;
	}

	private JPopupMenu layoutPopupMenu = null;

	/**
	 * 获取工作空间管理器中布局结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getLayoutsPopupMenu() {
		return this.layoutPopupMenu;
	}

	private JPopupMenu scenesPopupMenu = null;

	/**
	 * 获取工作空间管理器中场景集合结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getScenesPopupMenu() {
		return this.scenesPopupMenu;
	}

	private JPopupMenu scenePopupMenu = null;

	/**
	 * 获取工作空间管理器中场景结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getScenePopupMenu() {
		return this.scenePopupMenu;
	}

	private JPopupMenu resourcesPopupMenu = null;

	/**
	 * 获取工作空间管理器中资源集合结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getResourcesPopupMenu() {
		return this.resourcesPopupMenu;
	}

	private JPopupMenu symbolMarkerPopupMenu = null;

	/**
	 * 获取工作空间管理器中符号库结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getSymbolMarkerPopupMenu() {
		return this.symbolMarkerPopupMenu;
	}

	private JPopupMenu symbolLinePopupMenu = null;

	/**
	 * 获取工作空间管理器中线型库结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getSymbolLinePopupMenu() {
		return this.symbolLinePopupMenu;
	}

	private JPopupMenu symbolFillPopupMenu = null;

	/**
	 * 获取工作空间管理器中填充库结点的右键菜单。
	 * 
	 * @return
	 */
	public JPopupMenu getSymbolFillPopupMenu() {
		return this.symbolFillPopupMenu;
	}

	private JPopupMenu datasetGroupPopupMenu = null;

	/**
	 * 获取组下存储的海图数据集(目前主要是海图使用)。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetGroupPopupMenu() {
		return this.datasetGroupPopupMenu;
	}

	private JPopupMenu gridCollectionItemPopupMenu = null;

	/**
	 * 获取栅格数据集集合子节点。
	 * 
	 * @return
	 */
	public JPopupMenu getGridCollectionItemPopupMenu() {
		return this.gridCollectionItemPopupMenu;
	}

	private JPopupMenu datasetImageCollectionPopupMenu = null;

	/**
	 * 获取工作空间管理器中影像数据集集合根节点。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetImageCollectionPopupMenu() {
		return this.datasetImageCollectionPopupMenu;
	}

	private JPopupMenu datasetImageCollectionItemPopupMenu = null;

	/**
	 * 获取工作空间管理器中影像数据集集合子节点。
	 * 
	 * @return
	 */
	public JPopupMenu getDatasetImageCollectionItemPopupMenu() {
		return this.datasetImageCollectionItemPopupMenu;
	}

	/**
	 *  创建右键菜单对象
	 */
	private void buildContextMenu() {
		try {

			if (Application.getActiveApplication().getMainFrame() != null) {
				IContextMenuManager manager = Application.getActiveApplication().getMainFrame().getContextMenuManager();

				this.workspacePopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuWorkspace");
				this.datasourcesPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasources");
				this.datasourcePopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasource");
				this.datasetPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDataset");
				this.datasetVectorPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetVector");
				this.datasetTabularPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetTabular");
				this.datasetImagePopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetImage");
				this.datasetImageCollectionPopupMenu = (JPopupMenu) manager
						.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetImageCollection");
				this.datasetGridPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetGrid");
				// this.datasetImageCollectionItemPopupMenu =
				// manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuWorkspace");
				// this.gridCollectionItemPopupMenu
				this.datasetTopologyPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetTopology");
				this.mapsPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuMaps");
				this.mapPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuMap");
				this.layoutsPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuLayouts");
				this.layoutPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuLayout");
				this.scenesPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuScenes");
				this.scenePopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuScene");
				this.resourcesPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuResources");
				this.symbolMarkerPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuSymbolMarker");
				this.symbolLinePopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuSymbolLine");
				this.symbolFillPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuSymbolFill");
				this.datasetGroupPopupMenu = (JPopupMenu) manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasetGroup");

				this.isContextMenuBuilded = true;
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void initilize(Workspace workspace) {
		try {
			this.workspaceTree.setWorkspace(workspace);

			initializeToolBar();
			buildContextMenu();
			// this.WorkspaceToolBar.Visible = true;
			// this.WorkspaceTreeNodeFinderBar.Visible = false;
			// this.dicNodeDefaultColor = new Dictionary<TreeNode, Color>();
			// this.searchedList = new List<TreeNode>();
			// this.timer = new Timer();
			// this.timer.Interval = 500;
			// this.timer.Tick += new EventHandler(this.timer_Tick);
			// this.timer.Start();
			// this.labelCount = new UILabel();
			// this.labelCount.Font = this.toolStripTextBoxQuery.Font;
			// this.labelCount.ForeColor = Color.Gray;
			// this.labelCount.BackColor = this.toolStripTextBoxQuery.BackColor;
			// this.toolStripTextBoxQuery.TextBox.Controls.Add(this.labelCount);
			// this.labelCount.Cursor = Cursors.Arrow;
			// this.WorkspaceTree.NodesRefreshed += new
			// EventHandler(WorkspaceTree_NodesRefreshed);
			// this.WorkspaceTree.BeforeNodeContextMenuStripShow += new
			// BeforeNodeContextMenuStripShowEventHandler(WorkspaceTree_BeforeNodeContextMenuStripShow);

			this.workspaceTree.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mousePressed(java.awt.event.MouseEvent evt) {
					workspaceTreeMousePressed(evt);
				}
			});
			this.workspaceTree.addTreeSelectionListener(new TreeSelectionListener() {

				public void valueChanged(TreeSelectionEvent e) {
					TreePath[] selectedPaths = workspaceTree.getSelectionPaths();
					if (selectedPaths != null) {
						ArrayList<Datasource> activeDatasources = null;
						ArrayList<Dataset> activeDatasets = null;

						for (TreePath selectedPath : selectedPaths) {
							DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
							TreeNodeData selectedNodeData = (TreeNodeData) selectedNode.getUserObject();
							if (selectedNodeData != null) {
								Object nodeData = selectedNodeData.getData();

								if (nodeData != null) {
									if (nodeData instanceof Datasource) {
										if (activeDatasources == null) {
											activeDatasources = new ArrayList<Datasource>();
										}
										activeDatasources.add((Datasource) nodeData);
									} else if (nodeData instanceof Dataset) {
										if (activeDatasets == null) {
											activeDatasets = new ArrayList<Dataset>();
										}
										activeDatasets.add((Dataset) nodeData);
									}
								}
							}
						}

						if (activeDatasets != null && activeDatasets.size() > 0) {
							Application.getActiveApplication().setActiveDatasets(activeDatasets.toArray(new Dataset[activeDatasets.size()]));
							Application.getActiveApplication().setActiveDatasources(new Datasource[] { activeDatasets.get(0).getDatasource() });
						} else if (activeDatasources != null && activeDatasources.size() > 0) {
							Application.getActiveApplication().setActiveDatasets(null);
							Application.getActiveApplication().setActiveDatasources(activeDatasources.toArray(new Datasource[activeDatasources.size()]));
						} else {
							Application.getActiveApplication().setActiveDatasets(null);
							Application.getActiveApplication().setActiveDatasources(null);
						}
					}
				}
			});
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void initializeToolBar() {
		try {
			// this.WorkspaceToolBar.SuspendLayout();
			// this.WorkspaceToolBar.Items.Clear();
			// this.WorkspaceToolBar.Items.AddRange(new ToolStripItem[] {
			// this.dropDownButtonHideNode, this.toolStripSeparator1,
			// this.dropDownButtonQuery, this.toolStripTextBoxQuery,
			// this.toolStripButtonPre, this.toolStripButtonNext });
			// this.WorkspaceToolBar.ResumeLayout(false);
			// this.WorkspaceToolBar.PerformLayout();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void initializeResources() {
		try {
			// this.dropDownButtonHideNode.ToolTipText =
			// Properties.ControlsResources.String_ToolBar_NodeSetting;
			// this.toolStripMenuItemHideDatasource.Text =
			// Properties.ControlsResources.String_ToolBar_HideDatasource;
			// this.toolStripMenuItemHideMap.Text =
			// Properties.ControlsResources.String_ToolBar_HideMap;
			// this.toolStripMenuItemHideLayout.Text =
			// Properties.ControlsResources.String_ToolBar_HideLayout;
			// this.toolStripMenuItemHideScene.Text =
			// Properties.ControlsResources.String_ToolBar_HideScene;
			// this.toolStripMenuItemHideResources.Text =
			// Properties.ControlsResources.String_ToolBar_HideResources;
			//
			// this.dropDownButtonQuery.ToolTipText =
			// Properties.ControlsResources.String_ToolBar_QuerySetting;
			// this.toolStripMenuItemWholeWord.Text =
			// Properties.ControlsResources.String_ToolBar_MatchWholeWord;
			// this.toolStripMenuItemCase.Text =
			// Properties.ControlsResources.String_ToolBar_MatchCase;
			//
			// this.toolStripButtonPre.ToolTipText =
			// Properties.ControlsResources.String_ToolBar_QueryPre;
			// this.toolStripButtonNext.ToolTipText =
			// Properties.ControlsResources.String_ToolBar_QueryNext;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void workspaceTreeMousePressed(java.awt.event.MouseEvent evt) {
		try {
			int buttonType = evt.getButton();
			int clickCount = evt.getClickCount();
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) this.workspaceTree.getLastSelectedPathComponent();
			if (selectedNode != null) {
				TreeNodeData selectedNodeData = (TreeNodeData) selectedNode.getUserObject();
				String nodeText = selectedNodeData.getData().toString();
				// 右键
				if (buttonType == MouseEvent.BUTTON3 && clickCount == 1 && this.workspaceTree.getLastSelectedPathComponent() != null) {
					if (!this.isContextMenuBuilded) {
						this.buildContextMenu();
					}

					JPopupMenu popupMenu = this.getPoputMenu(selectedNodeData);
					if (popupMenu != null) {
						popupMenu.show(this.workspaceTree, evt.getX(), evt.getY());
					}
					// 双击

				} else if (buttonType == MouseEvent.BUTTON1 && clickCount == 2 && this.getWorkspaceTree().getLastSelectedPathComponent() != null) {
					if (selectedNodeData.getData() instanceof Dataset) {
						Dataset dataset = (Dataset) selectedNodeData.getData();
						if (dataset.getType() == DatasetType.TABULAR) {
							TabularUtilties.openDatasetVectorFormTabular(dataset);
						} else if (dataset.getType() == DatasetType.TOPOLOGY) {
						} else {
							String name = CommonToolkit.MapWrap.getAvailableMapName(
									String.format("%s@%s", dataset.getName(), dataset.getDatasource().getAlias()), true);
							IFormMap formMap = (IFormMap) CommonToolkit.FormWrap.fireNewWindowEvent(WindowType.MAP, name);
							if (formMap != null) {
								// add by huchenpu 20150716
								// 新建的地图窗口，修改默认的Action为漫游
								formMap.getMapControl().setAction(Action.PAN);
								Map map = formMap.getMapControl().getMap();

								Layer layer = map.getLayers().add(dataset, true);
								if (layer != null) {
									map.refresh();
									UICommonToolkit.getLayersManager().setMap(map);
								}
							}
						}
					} else if (selectedNodeData.getType() == NodeDataType.MAP_NAME) {
						IFormMap formMap = (IFormMap) CommonToolkit.FormWrap.fireNewWindowEvent(WindowType.MAP, nodeText);
						if (formMap != null) {
							Map map = formMap.getMapControl().getMap();
							map.open(nodeText);
							map.refresh();
							UICommonToolkit.getLayersManager().setMap(map);
						}
					} else if (selectedNodeData.getType() == NodeDataType.SCENE_NAME) {
						IFormScene formScene = (IFormScene) CommonToolkit.FormWrap.fireNewWindowEvent(WindowType.SCENE, nodeText);
						if (formScene != null) {
							Scene scene = formScene.getSceneControl().getScene();
							// add by huchenpu 20150706
							// 这里必须要设置工作空间，否则不能显示出来。
							// 而且不能在new SceneControl的时候就设置工作空间，必须等球显示出来的时候才能设置。
							scene.setWorkspace(Application.getActiveApplication().getWorkspace());
							scene.open(nodeText);
							scene.refresh();
							UICommonToolkit.getLayersManager().setScene(scene);
						}
					} else if (selectedNodeData.getType() == NodeDataType.LAYOUT_NAME) {
						IFormLayout formLayout = (IFormLayout) CommonToolkit.FormWrap.fireNewWindowEvent(WindowType.LAYOUT, nodeText);
						if (formLayout != null) {
							MapLayout mapLayout = formLayout.getMapLayoutControl().getMapLayout();
							mapLayout.open(nodeText);
							mapLayout.refresh();
							UICommonToolkit.getLayersManager().setMap(null);
							UICommonToolkit.getLayersManager().setScene(null);
						}
					} else if (selectedNodeData.getType() == NodeDataType.SYMBOL_MARKER_LIBRARY) {
						Resources resources = Application.getActiveApplication().getWorkspace().getResources();
//						SymbolLibraryDialog.showDialog(resources, SymbolType.MARKER);
						SymbolDialog symbolDialog = new SymbolDialog();
						symbolDialog.showDialog(resources, new GeoStyle(), SymbolType.MARKER);
					} else if (selectedNodeData.getType() == NodeDataType.SYMBOL_LINE_LIBRARY) {
						Resources resources = Application.getActiveApplication().getWorkspace().getResources();
//						SymbolLibraryDialog.showDialog(resources, SymbolType.LINE);
						SymbolDialog symbolDialog = new SymbolDialog();
						symbolDialog.showDialog(resources, new GeoStyle(), SymbolType.LINE);
					} else if (selectedNodeData.getType() == NodeDataType.SYMBOL_FILL_LIBRARY) {
						Resources resources = Application.getActiveApplication().getWorkspace().getResources();
//						SymbolLibraryDialog.showDialog(resources, SymbolType.FILL);
						SymbolDialog symbolDialog = new SymbolDialog();
						symbolDialog.showDialog(resources, new GeoStyle(), SymbolType.FILL);
					}
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private JPopupMenu getPoputMenu(TreeNodeData nodeData) {

		JPopupMenu popupMenu = null;
		try {
			NodeDataType type = nodeData.getType();

			if (type == NodeDataType.WORKSPACE) {
				popupMenu = this.workspacePopupMenu;
			} else if (type == NodeDataType.DATASOURCES) {
				popupMenu = this.datasourcesPopupMenu;
			} else if (type == NodeDataType.DATASOURCE) {
				popupMenu = this.datasourcePopupMenu;
				// } else if (type == NodeDataType.DATASET) {
				// popupMenu = this.datasetPopupMenu;
			} else if (type == NodeDataType.DATASET_VECTOR) {
				Dataset dataset = (Dataset)nodeData.getData();
				if (dataset.getType() == DatasetType.TABULAR) {
					popupMenu = this.datasetTabularPopupMenu;
				} else {
					popupMenu = this.datasetVectorPopupMenu;
				}				
			} /*else if (type == NodeDataType.DATASETTABULAR) {
				popupMenu = this.datasetTabularPopupMenu;
			} */else if (type == NodeDataType.DATASET_IMAGE) {
				popupMenu = this.datasetImagePopupMenu;
			} else if (type == NodeDataType.DATASET_GRID) {
				popupMenu = this.datasetGridPopupMenu;
			} else if (type == NodeDataType.DATASET_IMAGE_COLLECTION) {
				popupMenu = this.datasetImageCollectionPopupMenu;
			} else if (type == NodeDataType.DATASET_IMAGE_COLLECTION_ITEM) {
				popupMenu = this.datasetImageCollectionItemPopupMenu;
			} else if (type == NodeDataType.DATASET_GRID_COLLECTION) {
				// popupMenu = this.gridCollectionItemPopupMenu;
			} else if (type == NodeDataType.DATASET_GRID_COLLECTION_ITEM) {
				popupMenu = this.gridCollectionItemPopupMenu;
			} else if (type == NodeDataType.DATASET_TOPOLOGY) {
				popupMenu = this.datasetTopologyPopupMenu;
			} else if (type == NodeDataType.MAPS) {
				popupMenu = this.mapsPopupMenu;
			} else if (type == NodeDataType.MAP_NAME) {
				popupMenu = this.mapPopupMenu;
			} else if (type == NodeDataType.LAYOUTS) {
				popupMenu = this.layoutsPopupMenu;
			} else if (type == NodeDataType.LAYOUT_NAME) {
				popupMenu = this.layoutPopupMenu;
			} else if (type == NodeDataType.SCENES) {
				popupMenu = this.scenesPopupMenu;
			} else if (type == NodeDataType.SCENE_NAME) {
				popupMenu = this.scenePopupMenu;
			} else if (type == NodeDataType.RESOURCES) {
				popupMenu = this.resourcesPopupMenu;
			} else if (type == NodeDataType.SYMBOL_MARKER_LIBRARY) {
				popupMenu = this.symbolMarkerPopupMenu;
			} else if (type == NodeDataType.SYMBOL_LINE_LIBRARY) {
				popupMenu = this.symbolLinePopupMenu;
			} else if (type == NodeDataType.SYMBOL_FILL_LIBRARY) {
				popupMenu = this.symbolFillPopupMenu;
				// } else if (type == NodeDataType.DATASOURCE) {
				// popupMenu = this.datasetGroupPopupMenu;
			}

			// static NodeDataType LAYER3DDATASET
			// 三维数据集图层对象。
			// static NodeDataType LAYER3DIMAGEFILE
			// 影像文件图层对象。
			// static NodeDataType LAYER3DMODEL
			// 模型缓存图层对象
			// static NodeDataType THEME3DRANGEITEM
			// 三维分段专题图对象。
			// static NodeDataType THEME3DUNIQUEITEM
			// 三维单值专题图对象。
			// static NodeDataType THEMEGRAPHITEM
			// 统计专题图对象。
			// static NodeDataType THEMEGRIDRANGEITEM
			// 栅格分段专题图对象。
			// static NodeDataType THEMEGRIDUNIQUE_TEM
			// 栅格单值专题图对象。
			// static NodeDataType THEMELABELITEM
			// 标签专题图对象。
			// static NodeDataType THEMERANGEITEM
			// 分段专题图对象。
			// static NodeDataType THEMEUNIQUEITEM
			// 单值专题图对象。
			// static NodeDataType TOPOLOGY_DATASET_RELATIONS
			// 拓扑预处理项集合类集合类对象。
			// static NodeDataType TOPOLOGY_ERROR_DATASETS
			// 拓扑数据集错误子项集合类对象。
			// static NodeDataType UNKNOWN
			// 未知节点类型。
			// static NodeDataType WMSSUBLAYER
			// WMS 子图层。

		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return popupMenu;
	}

	/**
	 * 用于提供所涉及的 DropTarget 的 DnD 操作的通知
	 * 
	 * @author xie
	 */
	private class WorkspaceDropTargetAdapter extends DropTargetAdapter {

		public void drop(DropTargetDropEvent dtde) {
			try
			{
				if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor))// 如果拖入的文件格式受支持
				{
					dtde.acceptDrop(DnDConstants.ACTION_REFERENCE);// 接收拖拽来的数据
					@SuppressWarnings("unchecked")
					List<File> list = (List<File>) (dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor));
					for (File file : list) {
						if (workspaceType == getFileType(file)) {
							// 关闭单前可能已经打开的地图和图层
							Datasources datasources = Application.getActiveApplication().getWorkspace().getDatasources();
							for (int i = 0; i < datasources.getCount(); i++) {
								Datasets datasets = datasources.get(i).getDatasets();
								CommonToolkit.DatasetWrap.CloseDataset(datasets);
							}
							// 关闭当前工作空间
							WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo(file.getAbsolutePath());
							CommonToolkit.WorkspaceWrap.openWorkspace(connectionInfo, false);
						}
						// 打开数据源类型的文件
						if (datasourceType == getFileType(file)) {
							CommonToolkit.DatasourceWrap.openFileDatasource(file.getAbsolutePath(), null, true);
						}
					}
					dtde.dropComplete(true);// 指示拖拽操作已完成
				}
			} catch (Exception e)
			{
				Application.getActiveApplication().getOutput().output(e);
			}

		}

	}

	/**
	 * 得到文件类型
	 * 
	 * @param file
	 * @return
	 */
	private int getFileType(File file) {
		int flag = defaultType;
		String fileName = file.getName();
		String fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		if (fileType.equalsIgnoreCase("smwu") || fileType.equalsIgnoreCase("sxwu")) {
			flag = workspaceType;
		}
		if (fileType.equalsIgnoreCase("udb") || fileType.equalsIgnoreCase("udd")) {
			flag = datasourceType;
		}
		return flag;
	}

}
