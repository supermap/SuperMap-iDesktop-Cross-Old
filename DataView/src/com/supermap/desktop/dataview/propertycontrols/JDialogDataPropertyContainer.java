package com.supermap.desktop.dataview.propertycontrols;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import com.supermap.desktop.Application;
import com.supermap.desktop.dataview.DataViewProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.TreeNodeData;
import com.supermap.desktop.ui.controls.WorkspaceTree;

public class JDialogDataPropertyContainer extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<AbstractPropertyControl> controls = new ArrayList<AbstractPropertyControl>();
	private transient AbstractPropertyControl currentControl; // 当前选中的属性窗口，切换节点的时候，当然是希望保持当前选中的视图（如果有的话）

	private transient WorkspaceTree workspaceTree;
	private transient TreeSelectionListener workspaceTreeSelectionListener = new TreeSelectionListener() {

		@Override
		public void valueChanged(TreeSelectionEvent e) {
			workspaceTreeSelectionChanged();
		}
	};
	private transient DataPropertyControlsFactory factory = new DataPropertyControlsFactory(this);
	private JTabbedPane tabbledPane = new JTabbedPane();

	/**
	 * Create the dialog.
	 */
	public JDialogDataPropertyContainer(Window mainFrame) {
		super(mainFrame);
		setBounds(100, 100, 750, 450);
		this.setContentPane(new JPanel(new BorderLayout()));
		this.setLocationRelativeTo(null);
		setModal(false);
		this.controls = new ArrayList<AbstractPropertyControl>();
		this.workspaceTree = UICommonToolkit.getWorkspaceManager().getWorkspaceTree();
		this.addComponentListener(new ComponentAdapter() {

			@Override
			public void componentShown(ComponentEvent e) {
				fillPropertyContainer();
				workspaceTree.addTreeSelectionListener(workspaceTreeSelectionListener);
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				workspaceTree.removeTreeSelectionListener(workspaceTreeSelectionListener);
			}
		});
	}

	private void workspaceTreeSelectionChanged() {
		fillPropertyContainer();
	}

	private void fillPropertyContainer() {
		// 设置当前选中视图
		// Tab 子项不为0，表明上一个属性集合是 Tab 页，从中取出当前选中
		if (this.tabbledPane.getTabCount() != 0) {
			this.currentControl = (AbstractPropertyControl) this.tabbledPane.getSelectedComponent();
		}

		this.setTitle(DataViewProperties.getString("String_Property_R"));
		this.controls.clear();
		this.getContentPane().removeAll();
		this.tabbledPane.removeAll();

		if (this.workspaceTree.getSelectionPath() != null) {
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) this.workspaceTree.getSelectionPath().getLastPathComponent();

			if (selectedNode != null) {
				TreeNodeData nodeData = (TreeNodeData) selectedNode.getUserObject();
				AbstractPropertyControl[] propertyControls = factory.createPropertyControl(nodeData);

				if (propertyControls != null) {
					if (propertyControls.length == 1) {
						this.currentControl = propertyControls[0];
						this.controls.add(this.currentControl);
						this.setTitle(this.currentControl.getPropertyName());
						this.getContentPane().add(propertyControls[0], BorderLayout.CENTER);
					} else if (propertyControls.length > 1) {
						for (AbstractPropertyControl abstractPropertyControl : propertyControls) {
							this.controls.add(abstractPropertyControl);
							this.tabbledPane.addTab(abstractPropertyControl.getPropertyName(), abstractPropertyControl);
							this.getContentPane().add(this.tabbledPane, BorderLayout.CENTER);

							if (this.currentControl != null) {
								for (int i = 0; i < this.tabbledPane.getTabCount(); i++) {
									AbstractPropertyControl control = (AbstractPropertyControl) this.tabbledPane.getComponentAt(i);
									if (control.getPropertyName().equals(this.currentControl.getPropertyName())) {
										this.tabbledPane.setSelectedIndex(i);
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		((JPanel) this.getContentPane()).updateUI();
	}
}
